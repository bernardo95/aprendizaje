import { Component, OnInit } from '@angular/core';
import { HeroesService } from 'src/app/services/heroes.service';
import { Heroe } from 'src/app/interfaces/heroe.interface';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styles: []
})
export class HeroesComponent implements OnInit {

heroes:Heroe;
loading:boolean=true;

  constructor(private _heroesService:HeroesService) {
    this._heroesService.getHeroes().subscribe((data)=>{
      console.log(data)
      this.heroes=data;
      this.loading=false;
    })
  }

  ngOnInit() {
  }
  borrar(key$:string){
    this._heroesService.borrarHeroe(key$).subscribe(data=>{
      if(data){
        console.error(data)
      }else{
        delete this.heroes[key$];
      }
    })
  }
}
