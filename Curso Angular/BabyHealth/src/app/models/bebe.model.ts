export class BebeModel{
  id:string;
  email:string;
  nombre:string;
  apellido:string;
  edad:string;
  altura:string;
  peso:string;
  img:string;
  cont:number;

  constructor(){
    this.cont=0;
  }
}
