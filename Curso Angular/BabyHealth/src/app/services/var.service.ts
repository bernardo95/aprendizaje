import { Injectable } from '@angular/core';
import { BebeModel } from '../models/bebe.model';

@Injectable({
  providedIn: 'root'
})
export class VarService {
  private email:string;
  private id:string;
  private bebe:BebeModel;
  private urlImg:string;
  constructor() { }

  updateEmail(email:string){
    this.email=email;
  }

  getEmail(){
    return this.email;
  }

  updateId(id:string){
    this.id=id;
  }
  getId(){
    return this.id;
  }
  updateBebeObject(bebe){
    this.bebe=bebe;
  }
  getBebeObject(){
    return this.bebe;
  }
  getUrlImg(){
   return this.urlImg;
  }
  updateUrlImg(urlImg){
    this.urlImg=urlImg;
  }
}
