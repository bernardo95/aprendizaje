import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from "angularfire2/firestore";
import { Mensaje } from '../models/mensaje.interface';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private usuario:any;
  public chats:Mensaje[]=[];
  private itemsColletion:AngularFirestoreCollection<any>;
  constructor(private afs:AngularFirestore) {
    this.usuario=JSON.parse(localStorage.getItem('bebe'));
  }

  cargarMensajes(){
    this.itemsColletion=this.afs.collection<Mensaje>('chats',ref=>ref.orderBy('fecha','desc').limit(5));
    return this.itemsColletion.valueChanges().pipe(map(mensajes=>{
      console.log(mensajes);
      this.chats=[];
      for (let mensaje of mensajes) {
          this.chats.unshift(mensaje);
      }
      return this.chats;
    }))
  }

  agregarMensaje(texto:string){
    let mensaje:Mensaje={
      nombre:this.usuario.nombre,
      mensaje: texto,
      fecha: new Date().getTime(),
      uid:this.usuario.id
    }
   return  this.itemsColletion.add(mensaje);
  }
}
