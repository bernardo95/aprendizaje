import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators';
import { BebeModel } from '../models/bebe.model';

@Injectable({
  providedIn: 'root'
})
export class BebeService {
private url='https://babyhealth-9bd4c.firebaseio.com';
private cita='citas';
private id;
vacunas={
  HepatitisB:"Hepatitis B",
  HepatitisB2dosis:"Hepatitis B 2 dosis",
  DTPa:"DTPa",
  Hib:"Hib",
  vPI:"VPI",
  vNC:"VNC",
  rV:"RV",
  Vacuna_antigripal:"Vacuna antigripal",
  TripleVírica:"Triple vírica",
  VacunaContraVaricela:"Vacuna contra la varicela",
  HepatitisA:"Hepatitis A",
  vPH:"VPH",
  VacunaCntimeningocócicaConjugada:"Vacuna antimeningocócica conjugada",
  VacunaMeningococoB:"Vacuna contra el meningococo B (MenB)"
}
  constructor(private http:HttpClient) {
    // this.id=JSON.parse(localStorage.getItem('pediatra'))['id'];
   }

  crearBebe(usuario:BebeModel=new BebeModel()){
    const Data={
      ...usuario,
      cont:0
    }
    return this.http.post(`${this.url}/bebes.json`,Data).
    pipe(map(resp=>{
      usuario.id=resp['name'];
      return usuario;
    }));

  }
  addVaunasDataBase(vacunas,id){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.put(`${this.url}/bebes/${id}/DataBaseVacunas.json`,this.vacunas,{headers:headers})
  }
  getVaunasDataBase(id){
    return this.http.get(`${this.url}/bebes/${id}/DataBaseVacunas.json`).
    pipe(map(resp =>  this.crearArregloDatos(resp)
    ))
  }
  deleteVacunaDataBase(vacuna,id){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.delete(`${this.url}/bebes/${id}/DataBaseVacunas/${vacuna}.json`,{headers:headers})
  }
  actualizarBebe(usuario,id){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.put(`${this.url}/bebes/${id}.json`,usuario,{headers:headers})
  }
  updateCont(id,cont){
    const Data={
      cont:cont+1
    }
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.put(`${this.url}/bebes/${id}.json`,Data,{headers:headers})
  }
  addRegistroCrecimiento(id,forma){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.post(`${this.url}/bebes/${id}/crecimiento.json`,forma,{headers:headers});
  }
  getRegistrosCrecimiento(id){
    return this.http.get(`${this.url}/bebes/${id}/crecimiento.json`).
    pipe(map(resp =>  this.crearArregloDatos(resp)
    ))
  }
  addRegistroAlimentacion(id,forma){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.post(`${this.url}/bebes/${id}/alimentacion.json`,forma,{headers:headers});
  }
  getRegistrosAlimentacion(id){
    return this.http.get(`${this.url}/bebes/${id}/alimentacion.json`).
    pipe(map(resp =>  this.crearArregloDatos2(resp)
    ))
  }
  addRegistroVacuna(id,forma){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.post(`${this.url}/bebes/${id}/vacunas.json`,forma,{headers:headers});
  }
  getRegistrosVacunas(id){
    return this.http.get(`${this.url}/bebes/${id}/vacunas.json`).
    pipe(map(resp =>  this.crearArregloDatos2(resp)
    ))
  }
  updateCita(calendarioModel,id,contador){
    this.updateCont(id,contador);
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.put(`${this.url}/bebes/${id}/${this.cita}/${contador}.json`,calendarioModel,{headers:headers});
  }

  updateImgBebe(bebe:BebeModel,id:string,url:string){
    //Establecemos cabeceras
       let headers = new HttpHeaders().set('Content-Type','application/json',);
       headers.set('Access-Control-Allow-Origin','*');
    const Data={
     ...bebe,
     img:url
    };
    return this.http.put(`${this.url}/bebes/${id}.json`,Data,{headers:headers});
  }
  eliminarCita(id){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.delete(`${this.url}/bebes/${id}/${this.cita}.json`,{headers:headers})
  }
  getBebe(id:string){
    return this.http.get(`${this.url}/bebes/${id}.json`);
  }

  getBebes(){
    return this.http.get(`${this.url}/bebes.json`).
    pipe(map(resp =>  this.crearArregloBebes(resp)
    ))
  }

  private crearArregloBebes(bebeObj:object){
    const bebes:BebeModel[]=[];
    if (bebeObj==null) {
        return [];
    }else{
      Object.keys(bebeObj).forEach(key =>{
        const bebe:BebeModel=bebeObj[key];
        bebe.id=key;
        bebes.push(bebe);
      })
    }
    return bebes;
  }
  private crearArregloDatos(registrosObj:object){
    const registros:any[]=[];
    if (registrosObj==null) {
        return [];
    }else{
      Object.keys(registrosObj).forEach(key=>{
        const registro:any=registrosObj[key];
        registros.push(registro);
      })
    }
    return registros;
  }
  private crearArregloDatos2(alimentosObj:object){
    const alimentos:any[]=[];
    if (alimentosObj==null) {
        return [];
    }else{
      Object.keys(alimentosObj).forEach(key=>{
        const alimento:any=alimentosObj[key];
        alimentos.push(alimento);
      })
    }
    return alimentos;
  }
}
