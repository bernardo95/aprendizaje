import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PediatraModel } from '../models/pediatra.model';
import {AngularFirestore} from "angularfire2/firestore";
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BebeModel } from '../models/bebe.model';



@Injectable({
  providedIn: 'root'
})
export class PediatraService {
 private url='https://babyhealth-9bd4c.firebaseio.com';
private cita='citas';
 public items:Observable<any[]>;
 private id;
  constructor(private http:HttpClient,
              private db:AngularFirestore) {

                this.id=localStorage.getItem('id');
              }

  crearPediatra(usuario:PediatraModel=new PediatraModel()){
    const Data={
      ...usuario,
      cont:0
    }
    return this.http.post(`${this.url}/pediatras.json`,Data).
    pipe(map(resp=>{
      usuario.id=resp['name'];
      return usuario;
    }));
  }
  actualizarPediatra(usuario,id){
    const Data={
      ...usuario,
      cont:0
    }
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.put(`${this.url}/pediatras/${id}.json`,Data,{headers:headers})
  }
  updateCont(id,cont){
    const Data={
      cont:cont+1
    }
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.put(`${this.url}/pediatras/${id}.json`,Data,{headers:headers})
  }
  updateCita(calendarioModel,id,contador){
    this.updateCont(id,contador);
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.put(`${this.url}/pediatras/${id}/${this.cita}/${contador}.json`,calendarioModel,{headers:headers});
  }
  updateImgPediatra(pediatra:PediatraModel,id:string,url:string){
    //Establecemos cabeceras
       let headers = new HttpHeaders().set('Content-Type','application/json',);
       headers.set('Access-Control-Allow-Origin','*');
    const Data={
     ...pediatra,
     img:url
    };
    return this.http.put(`${this.url}/pediatras/${id}.json`,Data,{headers:headers});
  }
  eliminarCita(id){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.delete(`${this.url}/pediatras/${id}/${this.cita}.json`,{headers:headers})
  }
  getPediatras(){
    let headers = new HttpHeaders().set('Content-Type','application/json',);
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.get(`${this.url}/pediatras.json`,{headers:headers}).
    pipe(map(resp =>  this.crearArregloPediatras(resp)
    ))
  }

  getPediatra(id:string){
    return this.http.get(`${this.url}/pediatras/${id}.json`)
  }

  private crearArregloPediatras(pediatraObj:object){
    const pediatras:PediatraModel[]=[];
    if (pediatraObj==null) {
        return [];
    }else{
      Object.keys(pediatraObj).forEach(key =>{
        const pediatra:PediatraModel=pediatraObj[key];
        pediatra.id=key;
        pediatras.push(pediatra);
      })
    }
    return pediatras;
  }

    getTypeUser(ArrayUsuarios,forma){

      for (let i = 0; i < ArrayUsuarios.length; i++) {
            if (ArrayUsuarios[i].email==forma.value.email) {
               console.log("lo encontre");
               return true;
           }else{
             return false;
           }
      }
    }
}
