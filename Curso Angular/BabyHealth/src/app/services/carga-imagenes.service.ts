import { Injectable } from '@angular/core';
import {AngularFirestore} from "angularfire2/firestore";
import * as firebase from "firebase";
import { FileItem } from '../models/file-items';
import { Observable } from 'rxjs';
import { BebeService } from './bebe.service';
import { VarService } from './var.service';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class CargaImagenesService {
private CARPETA_IMAGES='img';
progreso:Observable<number>;
id:string;
bebe:any;
  constructor(private db:AngularFirestore,
              private bebeService:BebeService,
              private varService:VarService,
              private router:Router) {
this.id=localStorage.getItem('id');
this.bebeService.getBebe(this.id).subscribe(resp=>{
  this.bebe=resp;
})
}

cargarImagenesFirebase(imagenes:FileItem[]){
  const storageRef=firebase.storage().ref();
  for(const item of imagenes){
    item.estaSubiendo=true;
    if (item.progreso>=100) {
        continue;
    }
    const uploadTask:firebase.storage.UploadTask=storageRef.child(`${this.CARPETA_IMAGES}/${item.nombreArchivo}`)
                    .put(item.archivo);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                  (snapshot:firebase.storage.UploadTaskSnapshot) =>
                  item.progreso=(snapshot.bytesTransferred/snapshot.totalBytes)*100,
                (error)=> console.error("error al subir",error),
              ()=>{

                uploadTask.snapshot.ref.getDownloadURL().then(
                  (onfullfilled:any)=>{
                    console.log('promise the download is: '+ onfullfilled);
                    item.url=onfullfilled;
                    localStorage.setItem('urlImg',item.url);
                    console.log(item.url)

                    item.estaSubiendo=false;
                    // this.guardarImagen({
                    //   nombre: item.nombreArchivo,
                    //   url:item.url
                    // });
                    this.bebeService.updateImgBebe(this.bebe,this.id,item.url).subscribe(resp=>{
                      console.log("la url se actualizo en la base de datos y es"+item.url)
                      this.router.navigateByUrl('/bebe');
                    })
                  }, (onrejected:any)=>{
                    console.log('error al descargar la url')
                  });
                console.log('imagen cargada correctamente');
              });
  }
}
  private guardarImagen(imagen:{nombre:string,url:string}){
    this.db.collection(`${this.CARPETA_IMAGES}`)
        .add(imagen);
  }
}
