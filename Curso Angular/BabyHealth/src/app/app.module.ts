import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http'
import {AngularFireModule} from "angularfire2";
import {AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireStorageModule } from '@angular/fire/storage';
import {environment} from "../environments/environment";

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { AppRoutingModule } from './app-routing.nodule';
import { SelectUserComponent } from './components/pages/select-user/select-user.component';
import { FormularioBebeComponent } from './components/pages/formulario-bebe/formulario-bebe.component';
import { FormularioPediatraComponent } from './components/pages/formulario-pediatra/formulario-pediatra.component';
import { PediatraComponent } from './components/menu/pediatra/pediatra.component';
import { BebeComponent } from './components/menu/bebe/bebe.component';
import { CargaImagenesService } from './services/carga-imagenes.service';
import { CargaImagenComponent } from './components/pages/carga-imagen/carga-imagen.component';
import { NgDropFilesDirective } from './directvies/ng-drop-files.directive';
import { PerfilBebeComponent } from './components/pages/perfil-bebe/perfil-bebe.component';
import { PerfilPediatraComponent } from './components/pages/perfil-pediatra/perfil-pediatra.component';
import { CargarImagenPediatraComponent } from './components/pages/cargar-imagen-pediatra/cargar-imagen-pediatra.component';
import { CitasBebeComponent } from './components/pages/citas-bebe/citas-bebe.component';
import { CalendarioCitaComponent } from './components/pages/calendario-cita/calendario-cita.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ChatBebeComponent } from './components/pages/chat-bebe/chat-bebe.component';
import { CitasPediatraComponent } from './components/pages/citas-pediatra/citas-pediatra.component';
import { ChatPediatraComponent } from './components/pages/chat-pediatra/chat-pediatra.component';
import { ChatService } from './services/chat.service';
import { ChatPediatraService } from './services/chat-pediatra.service';
import { AlimentacionComponent } from './components/alimentacion/alimentacion.component';
import { CrecimientoComponent } from './components/crecimiento/crecimiento.component';
import { VacunasComponent } from './components/vacunas/vacunas.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    SelectUserComponent,
    FormularioBebeComponent,
    FormularioPediatraComponent,
    PediatraComponent,
    BebeComponent,
    CargaImagenComponent,
    NgDropFilesDirective,
    PerfilBebeComponent,
    PerfilPediatraComponent,
    CargarImagenPediatraComponent,
    CitasBebeComponent,
    CalendarioCitaComponent,
    ChatBebeComponent,
    CitasPediatraComponent,
    ChatPediatraComponent,
    CrecimientoComponent,
    AlimentacionComponent,
    VacunasComponent

  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFirestoreModule
  ],
  providers: [ChatService,
              ChatPediatraService,
              CargaImagenesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
