import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { VarService } from 'src/app/services/var.service';


@Component({
selector: 'app-registro',
templateUrl: './registro.component.html',
styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
emailInput:string;
submit:boolean=false;
forma:FormGroup;
rememberMe:boolean = false;

Toast = Swal.mixin({
toast: true,
position: 'top-end',
showConfirmButton: false,
timer: 3000
});
constructor(private auth:AuthService,
            private router:Router,
            private varService:VarService) {

     }

ngOnInit() {
  this.forma=new FormGroup({
      'email': new FormControl('', [
                                    Validators.required,
                                    Validators.pattern("[a-z0-9._%+-]+@[a-z0-9._]+\.[a-z]{2,3}")
                                    ]),
      'password': new FormControl('', [Validators.required,Validators.minLength(6)]),
      'password2': new FormControl('',[Validators.required,Validators.minLength(6),
                                      this.match('password')])
    });

    if (localStorage.getItem('email')) {
    this.forma.value.email=localStorage.getItem('email');
    this.rememberMe=true;
    }
}

   guardarCambios(){
    this.submit=true;
    console.log(this.forma.value);
    console.log(this.forma);
      if (this.forma.valid) {
    this.emailInput=this.forma.value.email;
    this.varService.updateEmail(this.emailInput);
          Swal.fire({
            allowOutsideClick:false,
            type: 'info',
            text: 'Espere por favor...'
          });
        Swal.showLoading();
        this.auth.nuevoUsuario(this.forma.value).
        subscribe(resp=>{
          Swal.close();
          if (this.rememberMe) {
            localStorage.setItem('email',this.forma.value.email);
          }
  this.router.navigateByUrl('/selectUser');
  this.Toast.fire({
   type: 'success',
   title: 'Sesion iniciada'
  })
    },err=>{
      Swal.fire({
        allowOutsideClick:false,
        type: 'error',
        title: 'Error al autenticar',
        text: err.error.error.message
      });
  });
  }
  }

  match(controlKey: string) {
  return (control: AbstractControl): { [s: string]: boolean } => {
      // control.parent es el FormGroup
      if (control.parent) { // en las primeras llamadas control.parent es undefined
        const checkValue  = control.parent.controls[controlKey].value;
        if (control.value !== checkValue) {
          return {
            match: false
          };
        }
      }
      return null;
  };
}
}
