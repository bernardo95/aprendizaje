import { Component, OnInit } from '@angular/core';
import { FileItem } from 'src/app/models/file-items';
import { CargaImagenesService } from 'src/app/services/carga-imagenes.service';

declare var $:any;
@Component({
  selector: 'app-carga-imagen',
  templateUrl: './carga-imagen.component.html',
  styleUrls: ['./carga-imagen.component.css']
})
export class CargaImagenComponent implements OnInit {
archivos:FileItem[]=[];
estaSobreElemento:boolean=false;

  constructor(public cargaImagenesService:CargaImagenesService) {
    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
   }

  ngOnInit() {
  }

cargarImagenes(){
  this.cargaImagenesService.cargarImagenesFirebase(this.archivos);
}

public toggleMenu(){
$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});
}

limpiarArchivos(){
  this.archivos=[];
}
}
