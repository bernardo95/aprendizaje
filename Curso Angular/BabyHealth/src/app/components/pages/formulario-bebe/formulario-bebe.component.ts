import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BebeService } from 'src/app/services/bebe.service';
import { VarService } from 'src/app/services/var.service';

@Component({
  selector: 'app-formulario-bebe',
  templateUrl: './formulario-bebe.component.html',
  styleUrls: ['./formulario-bebe.component.css']
})
export class FormularioBebeComponent implements OnInit {
  submit:boolean=false;
  forma:FormGroup;
  constructor(private router:Router,
              private bebeService:BebeService,
              private varService:VarService) { }

  ngOnInit() {
    this.forma=new FormGroup({
        'id': new FormControl(),
        'email': new FormControl(this.varService.getEmail()),
        'nombre': new FormControl('', [Validators.required,Validators.minLength(4)]),
        'apellido': new FormControl('',[Validators.required,Validators.minLength(4)]),
        'edad': new FormControl('',[Validators.required,Validators.minLength(1)]),
        'altura': new FormControl('',[Validators.required]),
        'peso': new FormControl('',[Validators.required]),
        'sexo': new FormControl('',Validators.required)
      });
  }
enviar(){
  this.submit=true;
  if (this.forma.valid) {
    this.bebeService.crearBebe(this.forma.value).subscribe(resp=>{
      console.log(resp);
      localStorage.setItem('id',resp.id)
      this.bebeService.actualizarBebe(resp,localStorage.getItem('id')).subscribe(resp=>{
        console.log('id add');
        this.bebeService.addVaunasDataBase(this.forma.value,localStorage.getItem('id')).subscribe(resp=>{
          console.log('vacunas add');
          localStorage.clear();
        })
      })
    });
    this.router.navigateByUrl('/login');

  }
}
}
