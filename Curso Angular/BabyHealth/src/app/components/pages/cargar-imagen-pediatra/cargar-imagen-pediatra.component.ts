import { Component, OnInit } from '@angular/core';
import { FileItem } from 'src/app/models/file-items';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { CargaImagenesPediatraService } from 'src/app/services/carga-imagenes-pediatra.service';

declare var $:any;
@Component({
  selector: 'app-cargar-imagen-pediatra',
  templateUrl: './cargar-imagen-pediatra.component.html',
  styleUrls: ['./cargar-imagen-pediatra.component.css']
})
export class CargarImagenPediatraComponent implements OnInit {
  archivos:FileItem[]=[];
  estaSobreElemento:boolean=false;
  constructor(public cargaImagenesService:CargaImagenesPediatraService,
              private auth:AuthService,
              private route:Router
              ) {
    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
   }

  ngOnInit() {
  }

  cargarImagenes(){
    this.cargaImagenesService.cargarImagenesFirebase(this.archivos);
  }

  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
  limpiarArchivos(){
    this.archivos=[];
  }
  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }
}
