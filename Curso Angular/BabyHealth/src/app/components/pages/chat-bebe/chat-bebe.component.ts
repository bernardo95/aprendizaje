import { Component, OnInit,AfterViewChecked } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { BebeService } from 'src/app/services/bebe.service';
import { PediatraService } from 'src/app/services/pediatra.service';

declare var $:any;
@Component({
  selector: 'app-chat-bebe',
  templateUrl: './chat-bebe.component.html',
  styleUrls: ['./chat-bebe.component.css']
})
export class ChatBebeComponent implements OnInit {
mensaje:string="";
elemento:any;
bebe:any;
pediatraId:any;
pediatraInfo:any;
dataDisponible:boolean=false;
  constructor(private auth:AuthService,
              private route:Router,
              public chatService:ChatService,
              private bebeService:BebeService,
              private pediatraService:PediatraService) {
                this.initData();

    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
    this.chatService.cargarMensajes().subscribe((resp)=>{
      this.ngAfterViewChecked();
      console.log(resp)
    });
      this.bebe=JSON.parse(localStorage.getItem('bebe'));
      console.log(this.bebe)
  }
  ngOnInit() {

    this.initData().then(()=>{
      this.dataDisponible=true;
    })
    this.bebe=JSON.parse(localStorage.getItem('bebe'));
    this.elemento=document.getElementById('app-mensajes');
  }
  async initData(){
  await  this.bebeService.getBebe(this.bebe.id).subscribe(resp=>{
      this.bebe=resp;
      this.pediatraService.getPediatra(this.bebe['citas'].undefined.id).subscribe(resp=>{
        this.pediatraInfo=resp;
        localStorage.setItem('pediatra',JSON.stringify(this.pediatraInfo));
        console.log(resp);
      })
    })
  }
  ngAfterViewChecked() {
    this.elemento.scrollTop = this.elemento.scrollHeight
  }
  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
enviar_mensaje(){
  if (this.mensaje.length==0) {
      return
  }else{
    this.chatService.agregarMensaje(this.mensaje).then(()=>{
      this.mensaje="";
      console.log('mensaje enviado');
    }).catch(()=>{
      this.mensaje="";
      console.log('error al enviar el mensaje');
    })
  }
  console.log(this.mensaje)
}
    salir(){
      localStorage.clear();
      this.auth.logout();
      this.route.navigateByUrl('/login');
    }
}
