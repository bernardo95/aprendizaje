import { Component, OnInit ,NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { BebeService } from 'src/app/services/bebe.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-perfil-bebe',
  templateUrl: './perfil-bebe.component.html',
  styleUrls: ['./perfil-bebe.component.css']
})
export class PerfilBebeComponent implements OnInit {
editar:boolean=false;
submit:boolean=false;
bebeInfo:any;
forma:FormGroup;
id:string;
urlImg:any;
dataDisponible:boolean=false;
  constructor(private route:Router,
              private bebeService:BebeService,
              private zone: NgZone
              ) {

                this.bebeData();
                this.urlImg=localStorage.getItem('urlImg');
                this.reloadLocalStorage();
   }
  ngOnInit() {
    this.bebeData().then(resp=>{
      this.dataDisponible=true;
      this.reloadLocalStorage();
      // if (localStorage.getItem('urlImg')==undefined) {
      //     this.bebeData();
      // }
    })
  }
 async bebeData(){
  setTimeout(()=>{
    this.id=localStorage.getItem('id');
     this.bebeService.getBebe(this.id).subscribe(resp=>{
        this.bebeInfo=resp;
         this.urlImg=resp['img'];
         console.log(this.urlImg)
         this.crearForma();
         this.reloadLocalStorage();
       })
  },500)

 }
 crearForma(){
   this.forma=new FormGroup({
         'id': new FormControl(localStorage.getItem('id')),
       'email': new FormControl(localStorage.getItem('email')),
       'img': new FormControl(localStorage.getItem('urlImg')),
       'nombre': new FormControl(localStorage.getItem('nombre'), [Validators.required,Validators.minLength(4)]),
       'apellido': new FormControl(localStorage.getItem('apellido'),[Validators.required,Validators.minLength(4)]),
       'edad': new FormControl(localStorage.getItem('edad'),[Validators.required,Validators.minLength(1)]),
       'altura': new FormControl(localStorage.getItem('altura'),[Validators.required]),
       'peso': new FormControl(localStorage.getItem('peso'),[Validators.required]),
       'sexo': new FormControl(localStorage.getItem('sexo'),)
     });
 }
editarForm(){
  this.editar=true;
  this.bebeData().then(res=>{
    this.reloadLocalStorage();
  })
}
 enviar(){
   this.submit=true;
   if (this.forma.valid) {
     this.bebeService.actualizarBebe(this.forma.value,localStorage.getItem('id')).subscribe(resp=>{
       console.log(resp);
       this.bebeData();
       this.reloadLocalStorage();
     });
     this.reloadPage();
   }
 }
 reloadPage() {
        this.route.navigateByUrl('/bebe').then(resp=>{
          this.bebeData();
        })
    }
async  reloadLocalStorage(){
  await  localStorage.setItem('altura',this.bebeInfo.altura);
  await  localStorage.setItem('peso',this.bebeInfo.peso);
  await  localStorage.setItem('apellido',this.bebeInfo.apellido);
  await  localStorage.setItem('edad',this.bebeInfo.edad);
  await  localStorage.setItem('email',this.bebeInfo.email);
  await  localStorage.setItem('nombre',this.bebeInfo.nombre);
  await  localStorage.setItem('urlImg',this.bebeInfo.img);
  await  localStorage.setItem('sexo',this.bebeInfo.sexo);
  await  localStorage.setItem('contador',this.bebeInfo.cont);
  await  localStorage.setItem('bebe',JSON.stringify(this.bebeInfo));
  }
cambiarImagen(){
  this.route.navigateByUrl('/cargaImg')
}
}
