import { Component, OnInit,AfterViewChecked ,NgZone} from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ChatPediatraService } from 'src/app/services/chat-pediatra.service';
import { PediatraService } from 'src/app/services/pediatra.service';
import { BebeService } from 'src/app/services/bebe.service';

declare var $:any;
@Component({
  selector: 'app-chat-pediatra',
  templateUrl: './chat-pediatra.component.html',
  styleUrls: ['./chat-pediatra.component.css']
})
export class ChatPediatraComponent implements OnInit {
  mensaje:string="";
  elemento:any;
  pediatra:any;
  bebeInfo:any;
  constructor(private auth:AuthService,
              private route:Router,
               public chatService:ChatPediatraService,
                private pediatraService:PediatraService,
                private bebeService:BebeService,
                private zone:NgZone) {
    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
    this.chatService.cargarMensajes().subscribe((resp)=>{
      this.ngAfterViewChecked();
      console.log(resp)
    });
    this.pediatra=JSON.parse(localStorage.getItem('pediatra'));
    this.bebeInfo=JSON.parse(localStorage.getItem('bebe'));
  }

  ngOnInit() {
      this.elemento=document.getElementById('app-mensajes');
  }
  ngAfterViewChecked() {
    this.elemento.scrollTop = this.elemento.scrollHeight
  }
  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
  enviar_mensaje(){
    if (this.mensaje.length==0) {
        return
    }else{
      this.chatService.agregarMensaje(this.mensaje).then(()=>{
        this.mensaje="";
        console.log('mensaje enviado');
      }).catch(()=>{
        this.mensaje="";
        console.log('error al enviar el mensaje');
      })
    }
    console.log(this.mensaje)
  }
terminarCita(){
  this.bebeService.eliminarCita(this.bebeInfo.id).subscribe(resp=>{
    console.log('cita eliminada bebe')
  })
  this.pediatraService.eliminarCita(this.pediatra.id).subscribe(resp=>{
    console.log('cita eliminada pediatra');
    console.log(resp)
    this.route.navigateByUrl('/citasPediatra');
  })
}
reloadPage() { //click handler or similar
       this.zone.runOutsideAngular(() => {
           location.reload();
       });


   }
    salir(){
      localStorage.clear();
      this.auth.logout();
      this.route.navigateByUrl('/login');
    }
}
