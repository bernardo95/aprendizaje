import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PediatraService } from 'src/app/services/pediatra.service';
import { VarService } from 'src/app/services/var.service';


@Component({
  selector: 'app-formulario-pediatra',
  templateUrl: './formulario-pediatra.component.html',
  styleUrls: ['./formulario-pediatra.component.css']
})


export class FormularioPediatraComponent implements OnInit {

  submit:boolean=false;
  forma:FormGroup;

  constructor(private router:Router,
              private pediatraService:PediatraService,
              private varService:VarService) { }

  ngOnInit() {
    this.forma=new FormGroup({
        'id': new FormControl(),
        'email': new FormControl(this.varService.getEmail()),
        'nombre': new FormControl('', [Validators.required,Validators.minLength(4)]),
        'apellido': new FormControl('',[Validators.required,Validators.minLength(4)]),
        'edad': new FormControl('',[Validators.required,Validators.minLength(2)]),
        'universidad': new FormControl('',[Validators.required,Validators.minLength(3)]),
        'ciudad': new FormControl('',[Validators.required,Validators.minLength(3)]),
        'sexo': new FormControl('',Validators.required)
      });
  }
  enviar(){
    console.log(this.forma)
    this.submit=true;
    if (this.forma.valid) {
      this.pediatraService.crearPediatra(this.forma.value).subscribe(resp=>{
        console.log(resp);
        localStorage.setItem('id',resp.id)
        this.pediatraService.actualizarPediatra(resp,localStorage.getItem('id')).subscribe(resp=>{
          console.log('id add');
          localStorage.clear();
        })
      });
      this.router.navigateByUrl('/login');
    }
  }
}
