import { Component, OnInit,NgZone } from '@angular/core';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { BebeService } from 'src/app/services/bebe.service';
import { PediatraService } from 'src/app/services/pediatra.service';

declare var $:any;

@Component({
  selector: 'app-calendario-cita',
  templateUrl: './calendario-cita.component.html',
  styleUrls: ['./calendario-cita.component.css']
})
export class CalendarioCitaComponent implements OnInit {

  model: NgbDateStruct;
  date: {year: number, month: number};
  time = {hour: 0, minute: 0};
  citaDataBase:any;
  calendarioModel:any;
  bebeId={
    id:""
  };
  pediatraId={
    id:""
  };
  citaDataBasePediatra: any;
  pediatraInfo:any;
  contPediatra:any;
  contBebe:any;
   constructor(private calendar: NgbCalendar,
              private auth:AuthService,
              private route:Router,
              private bebeService:BebeService,
              private pediatraService:PediatraService,
              private zone: NgZone) {

    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
    this.contPediatra=JSON.parse(localStorage.getItem('pediatra'))['cont'];
    this.pediatraInfo=JSON.parse(localStorage.getItem('pediatra'));
    this.pediatraId.id=JSON.parse(localStorage.getItem('pediatra'))['id'];
    this.contBebe=localStorage.getItem('contador');
    this.bebeId.id=localStorage.getItem('id');
    console.log(this.pediatraId)
    // this.getContadores();

  }

  ngOnInit() {
    this.selectToday();
  }

  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
  selectToday() {
  this.model = this.calendar.getToday();
}
// getContadores(){
//   this.contPediatra=this.pediatraService.getCont(this.pediatraId);
//   console.log(this.contPediatra)
//   this.contBebe=this.bebeService.getCont(this.bebeId);
//     console.log(this.contBebe)
// }
  getDateCalendatFull(){

    this.calendarioModel={
      ...this.model,
      ...this.time
    }
    localStorage.setItem('calendario',JSON.stringify(this.calendarioModel));

    this.citaDataBase={
      ...this.calendarioModel,
      ...this.pediatraId
    }
    this.citaDataBasePediatra={
      ...this.calendarioModel,
      ...this.bebeId
    }
   // this.getContadores();
    this.pediatraService.updateCita(this.citaDataBasePediatra,this.pediatraId.id,this.contPediatra).subscribe(resp=>{
      console.log('la cita se agendo en la base de datos de pediatra'+resp)
    })
    this.bebeService.updateCita(this.citaDataBase,localStorage.getItem('id'),this.contBebe).subscribe(resp=>{
      console.log("la cita se agendo en la base de datos"+resp)
        this.route.navigateByUrl('/citasBebe')
    })

  }
  reloadPage() { //click handler or similar
         this.zone.runOutsideAngular(() => {

             location.reload();


         });
     }
  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }
}
