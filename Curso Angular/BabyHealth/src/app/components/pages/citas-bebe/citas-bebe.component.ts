import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { PediatraService } from 'src/app/services/pediatra.service';
import { BebeService } from 'src/app/services/bebe.service';

declare var $:any;
@Component({
  selector: 'app-citas-bebe',
  templateUrl: './citas-bebe.component.html',
  styleUrls: ['./citas-bebe.component.css']
})
export class CitasBebeComponent implements OnInit {
listaPediatras:any;
  constructor(private auth:AuthService,
              private route:Router,
              private pediatraService:PediatraService,
              private bebeService:BebeService) {
                this.recargarDatosBebe();
                if (this.listaPediatras==undefined) {
                    this.getPediatras();
                }
    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
   }

  ngOnInit() {
  }
  async recargarDatosBebe(){
    await this.bebeService.getBebe(localStorage.getItem('id')).subscribe(resp=>{
      if (resp['citas']==undefined) {

      }else{
        this.route.navigateByUrl('/chatBebe');
      }
    })
  }
  async getPediatras(){
    await this.pediatraService.getPediatras().subscribe(resp=>{
      this.listaPediatras=resp;
      for (let i = 0; i < this.listaPediatras.length; i++) {
          if (this.listaPediatras[i].id==null) {
              this.listaPediatras[i]=undefined;
          }
      }
      console.log(this.listaPediatras);
    });
  }
  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
  calendarioCita(item){
    localStorage.setItem('pediatra',JSON.stringify(item) );
    this.route.navigateByUrl('/calendarioCita');
  }
  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }
}
