import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { PediatraService } from 'src/app/services/pediatra.service';
import { BebeService } from 'src/app/services/bebe.service';
declare var $:any;
@Component({
  selector: 'app-citas-pediatra',
  templateUrl: './citas-pediatra.component.html',
  styleUrls: ['./citas-pediatra.component.css']
})
export class CitasPediatraComponent implements OnInit {

pediatraCitas:any;
pediatraInfo:any;
bebeInfo:any;
bebeId:any;
imgBebe:any;
  constructor(private auth:AuthService,
              private route:Router,
              private pediatraService:PediatraService,
              private bebeService:BebeService) {

                this.updatePediatraInfo();
    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
  }

  ngOnInit() {
    if (this.bebeInfo.img==undefined) {
        this.updateBebeInfo();
    }
  }

async updatePediatraInfo(){
  await this.pediatraService.getPediatra(localStorage.getItem('id')).subscribe(resp=>{
      console.log(resp)
      this.pediatraCitas=resp['citas'];
      this.pediatraInfo=resp;
      this.bebeId=resp['citas'][0].id;
      console.log(this.pediatraCitas)
      this.updateBebeInfo();
    })
  }
  async updateBebeInfo(){
    await this.bebeService.getBebe(this.bebeId).subscribe(resp=>{
      this.bebeInfo=resp;
      localStorage.setItem('bebe',JSON.stringify(this.bebeInfo));
      this.imgBebe=localStorage.setItem('imgBebe',this.bebeInfo.img)
     console.log(this.bebeInfo)
    })
  }
  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
  botonChat(){
    this.route.navigateByUrl('/chatPediatra');
  }

  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }
}
