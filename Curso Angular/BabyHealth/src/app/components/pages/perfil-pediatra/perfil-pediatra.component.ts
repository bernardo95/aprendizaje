import { Component, OnInit,NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { PediatraService } from 'src/app/services/pediatra.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-perfil-pediatra',
  templateUrl: './perfil-pediatra.component.html',
  styleUrls: ['./perfil-pediatra.component.css']
})
export class PerfilPediatraComponent implements OnInit {
  editar:boolean=false;
  submit:boolean=false;
  id:string;
  urlImg:any;
  pediatraInfo:any;
  forma:FormGroup;
  dataDisponible:boolean=false;

    constructor(private route:Router,
                private pediatraService:PediatraService,
                private zone: NgZone) {
                  this.pediatraData();
                  this.urlImg=localStorage.getItem('urlImg');
                  this.reloadLocalStorage();
     }

  ngOnInit() {
    this.pediatraData().then(resp=>{
      this.dataDisponible=true;
      this.reloadLocalStorage();
      if (localStorage.getItem('urlImg')==undefined) {
          this.pediatraData();
      }
    })
  }
  async pediatraData(){
   setTimeout(()=>{
     this.id=localStorage.getItem('id');
      this.pediatraService.getPediatra(this.id).subscribe(resp=>{
         this.pediatraInfo=resp;
          this.urlImg=resp['img'];
          console.log(this.urlImg)
          this.crearForma();
          this.reloadLocalStorage();
        })
   },500)

  }

    async  reloadLocalStorage(){
      await  localStorage.setItem('apellido',this.pediatraInfo.apellido);
      await  localStorage.setItem('edad',this.pediatraInfo.edad);
      await  localStorage.setItem('email',this.pediatraInfo.email);
      await  localStorage.setItem('nombre',this.pediatraInfo.nombre);
      await  localStorage.setItem('urlImg',this.pediatraInfo.img);
      await  localStorage.setItem('sexo',this.pediatraInfo.sexo);
      await  localStorage.setItem('contador',this.pediatraInfo.cont);
      await  localStorage.setItem('ciudad',this.pediatraInfo.ciudad);
      await  localStorage.setItem('contador',this.pediatraInfo.cont);
      await  localStorage.setItem('pediatra',JSON.stringify(this.pediatraInfo));
      }

      crearForma(){
        this.forma=new FormGroup({
            'id': new FormControl(localStorage.getItem('id')),
            'email': new FormControl(localStorage.getItem('email')),
            'img': new FormControl(localStorage.getItem('urlImg')),
            'nombre': new FormControl(localStorage.getItem('nombre'), [Validators.required,Validators.minLength(4)]),
            'apellido': new FormControl(localStorage.getItem('apellido'),[Validators.required,Validators.minLength(4)]),
            'edad': new FormControl(localStorage.getItem('edad'),[Validators.required,Validators.minLength(1)]),
            'ciudad': new FormControl(localStorage.getItem('ciudad'),[Validators.required]),
            'sexo': new FormControl(localStorage.getItem('sexo'),),
            'cont': new FormControl(localStorage.getItem('contador'),),
            'universidad': new FormControl(localStorage.getItem('universidad'),)
          });
      }
     editarForm(){
       this.editar=true;
       this.pediatraData().then(res=>{
         this.reloadLocalStorage();
       })

     }
      enviar(){
        this.submit=true;
        if (this.forma.valid) {
          this.pediatraService.actualizarPediatra(this.forma.value,localStorage.getItem('id')).subscribe(resp=>{
            console.log(resp);
            this.pediatraData();
            this.reloadLocalStorage();
          });
          // this.reloadPage();

        }
      }
      reloadPage() { //click handler or similar

             this.route.navigateByUrl('/pediatra').then(resp=>{
               this.pediatraData();
             })
         }


 cambiarImagen(){
   this.route.navigateByUrl('/cargaImgPediatra')
 }
}
