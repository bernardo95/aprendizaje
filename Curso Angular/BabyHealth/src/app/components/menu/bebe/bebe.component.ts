import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { VarService } from 'src/app/services/var.service';
import { BebeService } from 'src/app/services/bebe.service';
import { BebeModel } from 'src/app/models/bebe.model';


declare var $:any;
@Component({
  selector: 'app-bebe',
  templateUrl: './bebe.component.html',
  styleUrls: ['./bebe.component.css']
})
export class BebeComponent implements OnInit {
// @ViewChild('imageUser') inputImageUser:ElementRef;
email:String;
idBebe:String;
bebeObject:BebeModel;
urlImg:string;


  constructor(private auth:AuthService,
              private route:Router,
              private varService:VarService,
              private bebeService:BebeService) {
this.email=varService.getEmail();

    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
   }

  ngOnInit() {
// console.log(this.email);
this.bebeService.getBebes().subscribe(resp=>{
  // console.log(resp)
  this.getInfoBebe(resp);
});
  }
  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }

  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }
  getInfoBebe(resp){
    for (let i = 0; i < resp.length; i++) {
          if (resp[i].email==this.email) {
            localStorage.setItem('altura',resp[i].altura);
            localStorage.setItem('peso',resp[i].peso);
            localStorage.setItem('apellido',resp[i].apellido);
            localStorage.setItem('edad',resp[i].edad);
            localStorage.setItem('email',resp[i].email);
            localStorage.setItem('nombre',resp[i].nombre);
            localStorage.setItem('id',resp[i].id);
            localStorage.setItem('urlImg',resp[i].img);
            localStorage.setItem('sexo',resp[i].sexo);
            localStorage.setItem('contador',resp[i].cont);
            localStorage.setItem('bebe',JSON.stringify(resp[i]));
             console.log(resp[i]);
             this.idBebe=resp[i].id;
             this.bebeObject=resp[i];
             this.urlImg=resp[i].img;
             this.varService.updateId(resp[i].id);
             this.varService.updateBebeObject(resp[i]);
             this.varService.updateUrlImg(resp[i].img)

    }
  }
}

}
