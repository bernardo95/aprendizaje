import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { PediatraService } from 'src/app/services/pediatra.service';
import { VarService } from 'src/app/services/var.service';
import { PediatraModel } from 'src/app/models/pediatra.model';
declare var $:any;
@Component({
  selector: 'app-pediatra',
  templateUrl: './pediatra.component.html',
  styleUrls: ['./pediatra.component.css']
})
export class PediatraComponent implements OnInit {
  email:String;
  idPediatra:String;
  pediatraObject:PediatraModel;
  urlImg:string;

  constructor(private auth:AuthService,
              private route:Router,
              private pediatraService:PediatraService,
              private varService:VarService) {
    this.email=varService.getEmail();
    $(document).ready(function () {
      $('#sidebar').toggleClass('active');
    });
   }

  ngOnInit() {
    // console.log(this.email);
    this.pediatraService.getPediatras().subscribe(resp=>{
      // console.log(resp)
      this.getInfoPediatra(resp);
    });
  }

  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }

  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }

  getInfoPediatra(resp){
    for (let i = 0; i < resp.length; i++) {
          if (resp[i].email==this.email) {
            localStorage.setItem('apellido',resp[i].apellido);
            localStorage.setItem('edad',resp[i].edad);
            localStorage.setItem('email',resp[i].email);
            localStorage.setItem('nombre',resp[i].nombre);
            localStorage.setItem('id',resp[i].id);
            localStorage.setItem('urlImg',resp[i].img);
            localStorage.setItem('sexo',resp[i].sexo);
            localStorage.setItem('ciudad',resp[i].ciudad);
            localStorage.setItem('universidad',resp[i].universidad);
            localStorage.setItem('pediatra',JSON.stringify(resp[i]));
             console.log(resp[i]);
             this.idPediatra=resp[i].id;
             this.pediatraObject=resp[i];
             this.urlImg=resp[i].img;
             this.varService.updateId(resp[i].id);
             this.varService.updateBebeObject(resp[i]);
             this.varService.updateUrlImg(resp[i].img)

    }
  }
}
}
