import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BebeService } from 'src/app/services/bebe.service';
declare var $:any;
@Component({
  selector: 'app-alimentacion',
  templateUrl: './alimentacion.component.html',
  styleUrls: ['./alimentacion.component.css']
})
export class AlimentacionComponent implements OnInit {
  forma:FormGroup;
  registrosAlimentacion:any;
  constructor(private auth:AuthService,
              private route:Router,
              private bebeService:BebeService) {

        this.bebeService.getRegistrosAlimentacion(localStorage.getItem('id')).subscribe(resp=>{
          this.registrosAlimentacion=resp;
          console.log(this.registrosAlimentacion)
        })
        $(document).ready(function () {
          $('#sidebar').toggleClass('active');
        });
               }

  ngOnInit() {
    this.crearForma();
  }

  crearForma(){
    this.forma=new FormGroup({
        'alimentacion': new FormControl('',[Validators.required]),
        'fecha': new FormControl('')
      });
  }
  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
  enviarRegistro(){
    if (this.forma.valid) {
      this.forma.value.fecha= new Date().getTime();
        this.bebeService.addRegistroAlimentacion(localStorage.getItem('id'),this.forma.value).subscribe(resp=>{
          console.log('registro enviado',resp);
          this.bebeService.getRegistrosAlimentacion(localStorage.getItem('id')).subscribe(resp=>{
            this.registrosAlimentacion=resp;
            console.log(this.registrosAlimentacion);
            this.forma.reset();
          })
        })
    }
  }
  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }
}
