import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BebeService } from 'src/app/services/bebe.service';
declare var $:any;
@Component({
  selector: 'app-crecimiento',
  templateUrl: './crecimiento.component.html',
  styleUrls: ['./crecimiento.component.css']
})
export class CrecimientoComponent implements OnInit {
forma:FormGroup;
registrosCrecimiento:any;
  constructor(private auth:AuthService,
              private route:Router,
              private bebeService:BebeService) {

        this.bebeService.getRegistrosCrecimiento(localStorage.getItem('id')).subscribe(resp=>{
          this.registrosCrecimiento=resp;
          console.log(resp)
        })
        $(document).ready(function () {
          $('#sidebar').toggleClass('active');
        });
              }

  ngOnInit() {
    this.crearForma();
  }
  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
  crearForma(){
    this.forma=new FormGroup({
        'altura': new FormControl('',[Validators.required]),
        'peso': new FormControl('',[Validators.required]),
        'imc': new FormControl(''),
        'estado': new FormControl('')
      });
  }
  enviarRegistro(){
    if (this.forma.valid) {
      this.forma.value.imc=  this.forma.value.peso/(this.forma.value.altura*this.forma.value.altura)*10000;
      if (this.forma.value.imc<18.5) {
          this.forma.value.estado="Peso inferior al normal";
      }if (this.forma.value.imc>=18.5 && this.forma.value.imc<25) {
          this.forma.value.estado="Peso normal";
      }if (this.forma.value.imc>=25 && this.forma.value.imc<30) {
          this.forma.value.estado="Peso superior al normal";
      }if (this.forma.value.imc>=30) {
          this.forma.value.estado="Estado de obesidad";
      }
        this.bebeService.addRegistroCrecimiento(localStorage.getItem('id'),this.forma.value).subscribe(resp=>{
          console.log('registro enviado',resp);
          this.bebeService.getRegistrosCrecimiento(localStorage.getItem('id')).subscribe(resp=>{
            this.registrosCrecimiento=resp;
            console.log(resp);
            this.forma.reset();
          })
        })
    }
  }
  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }
}
