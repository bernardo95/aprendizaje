import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BebeService } from 'src/app/services/bebe.service';
declare var $:any;
@Component({
  selector: 'app-vacunas',
  templateUrl: './vacunas.component.html',
  styleUrls: ['./vacunas.component.css']
})
export class VacunasComponent implements OnInit {
  forma:FormGroup;
  registrosVacunas:any;
  DataBaseVacunas:any;
  constructor(private auth:AuthService,
              private route:Router,
              private bebeService:BebeService)   {
        this.bebeService.getVaunasDataBase(localStorage.getItem('id')).subscribe(resp=>{
          this.DataBaseVacunas=resp;
          console.log(resp)
        })
        this.bebeService.getRegistrosVacunas(localStorage.getItem('id')).subscribe(resp=>{
          this.registrosVacunas=resp;
          console.log(this.registrosVacunas)
        })
        $(document).ready(function () {
          $('#sidebar').toggleClass('active');
        });
              }
  ngOnInit() {
    this.crearForma();
  }
  public toggleMenu(){
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }
  crearForma(){
    this.forma=new FormGroup({
        'vacuna': new FormControl('',[Validators.required]),
        'fecha': new FormControl('')
      })
    }

    enviarRegistro(){
      if (this.forma.valid) {
        this.forma.value.fecha= new Date().getTime();
          this.bebeService.addRegistroVacuna(localStorage.getItem('id'),this.forma.value).subscribe(resp=>{
            console.log('registro enviado',resp);
            this.bebeService.getRegistrosVacunas(localStorage.getItem('id')).subscribe(resp=>{
              this.registrosVacunas=resp;
              console.log(this.registrosVacunas);
              // this.bebeService.getVaunasDataBase(localStorage.getItem('id')).subscribe(resp=>{
              //   for (let i = 0; i < resp.length; i++) {
              //       if (resp.i==this.forma.value.vacuna) {
              //           this.bebeService.deleteVacunaDataBase(i,localStorage.getItem('id')).subscribe(resp=>{
              //             console.log('vacuna eliminada');
              //             this.bebeService.getVaunasDataBase(localStorage.getItem('id')).subscribe(resp=>{
              //               this.DataBaseVacunas=resp;
              //             })
              //           })
              //       }
              //   }
              // })
              this.forma.reset();
            })
          })
      }
    }
  salir(){
    localStorage.clear();
    this.auth.logout();
    this.route.navigateByUrl('/login');
  }
}
