import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { PediatraService } from 'src/app/services/pediatra.service';
import { VarService } from 'src/app/services/var.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submit:boolean=false;
  forma:FormGroup;
  rememberMe:boolean = false;
  ArrayUsuarios;


  Toast = Swal.mixin({
   toast: true,
   position: 'top-end',
   showConfirmButton: false,
   timer: 3000
 });

    constructor(private auth:AuthService,
                private router:Router,
                private pediatraService:PediatraService,
                private varService:VarService
                ) {

                    this.pediatraService.getPediatras().subscribe(resp=>{
                       this.ArrayUsuarios=resp;
                       // console.log(this.ArrayUsuarios);
                     })
                 }

    ngOnInit() {
      this.forma=new FormGroup({
          'email': new FormControl('', [
                                        Validators.required,
                                        Validators.pattern("[a-z0-9._%+-]+@[a-z0-9._]+\.[a-z]{2,3}")
                                        ]),
          'password': new FormControl('', [Validators.required,Validators.minLength(6)])
        });
      if (localStorage.getItem('email')) {
          this.forma.value.email=localStorage.getItem('email');
          this.rememberMe=true;
      }

     }
     onSubmit(){
       console.log(this.forma)
      this.submit=true;
       if (this.forma.valid) {
         Swal.fire({
           allowOutsideClick:false,
           type: 'info',
           text: 'Espere por favor...'
         });
         Swal.showLoading();
        this.auth.login(this.forma.value).subscribe(resp=>{
          // console.log(resp);

          if (this.rememberMe) {
              localStorage.setItem('email',this.forma.value.email);
          }
            if (this.pediatraService.getTypeUser(this.ArrayUsuarios,this.forma)) {
                this.router.navigateByUrl('/pediatra');
                // console.log("fue true")
                this.varService.updateEmail(this.forma.value.email);
            }else{
              this.router.navigateByUrl('/bebe');
              // console.log("fue false")
              this.varService.updateEmail(this.forma.value.email);
            }
          Swal.close();
          this.Toast.fire({
            type: 'success',
            title: 'Sesion iniciada'
          })
        },err=>{
          Swal.fire({
            allowOutsideClick:false,
            type: 'error',
            title: 'Error al autenticar',
            text: err.error.error.message
          });
          // console.log(err.error.error.message);
        })
       }

     }
}
