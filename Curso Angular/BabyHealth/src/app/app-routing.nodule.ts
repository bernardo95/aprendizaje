import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import { RegistroComponent } from './components/registro/registro.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { SelectUserComponent } from './components/pages/select-user/select-user.component';
import { FormularioPediatraComponent } from './components/pages/formulario-pediatra/formulario-pediatra.component';
import { FormularioBebeComponent } from './components/pages/formulario-bebe/formulario-bebe.component';
import { PediatraComponent } from './components/menu/pediatra/pediatra.component';
import { BebeComponent } from './components/menu/bebe/bebe.component';
import { CargaImagenComponent } from './components/pages/carga-imagen/carga-imagen.component';
import { CargarImagenPediatraComponent } from './components/pages/cargar-imagen-pediatra/cargar-imagen-pediatra.component';
import { CitasBebeComponent } from './components/pages/citas-bebe/citas-bebe.component';
import { CalendarioCitaComponent } from './components/pages/calendario-cita/calendario-cita.component';
import { ChatBebeComponent } from './components/pages/chat-bebe/chat-bebe.component';
import { CitasPediatraComponent } from './components/pages/citas-pediatra/citas-pediatra.component';
import { ChatPediatraComponent } from './components/pages/chat-pediatra/chat-pediatra.component';
import { CrecimientoComponent } from './components/crecimiento/crecimiento.component';
import { AlimentacionComponent } from './components/alimentacion/alimentacion.component';
import { VacunasComponent } from './components/vacunas/vacunas.component';


const ROUTES:Routes = [
  {path: 'registro' , component:RegistroComponent},
  {path: 'login' , component:LoginComponent},
  {path: 'selectUser' , component:SelectUserComponent,canActivate:[AuthGuard]},
  {path: 'formularioPediatra' , component:FormularioPediatraComponent,canActivate:[AuthGuard]},
  {path: 'formularioBebe' , component:FormularioBebeComponent,canActivate:[AuthGuard]},
  {path: 'pediatra' , component:PediatraComponent,canActivate:[AuthGuard]},
  {path: 'bebe' , component:BebeComponent,canActivate:[AuthGuard]},
  {path: 'cargaImg', component:CargaImagenComponent,canActivate:[AuthGuard]},
  {path: 'cargaImgPediatra', component:CargarImagenPediatraComponent,canActivate:[AuthGuard]},
  {path: 'citasBebe', component:CitasBebeComponent,canActivate:[AuthGuard]},
  {path: 'citasPediatra', component:CitasPediatraComponent,canActivate:[AuthGuard]},
  {path: 'calendarioCita', component:CalendarioCitaComponent,canActivate:[AuthGuard]},
  {path: 'chatBebe', component:ChatBebeComponent,canActivate:[AuthGuard]},
  {path: 'chatPediatra', component:ChatPediatraComponent,canActivate:[AuthGuard]},
  {path: 'crecimiento', component:CrecimientoComponent,canActivate:[AuthGuard]},
  {path: 'alimentacion', component:AlimentacionComponent,canActivate:[AuthGuard]},
    {path: 'vacunas', component:VacunasComponent,canActivate:[AuthGuard]},
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports:[RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule{}
