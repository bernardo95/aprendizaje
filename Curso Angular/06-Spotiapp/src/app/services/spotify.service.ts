import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http:HttpClient) { }
getQuery(query:string){
  const url=`https://api.spotify.com/v1/${query}`;
  const headers= new HttpHeaders({
    'Authorization':'BQDikfo5n_8ZLWiNbYeqXThFW6O8tOb2VXg4j7WzwySbXM9JkbnbB5T-c_hW70L8JVgL93mgOSNbl25UNiOhfphhpvZpD_JBW_D7cDIpRmC0EsDyCUpMMhSw5xntFQl-rvlvWNH8pnEAuf5SjeC8xe4kdg_6nAfNLQ'
  });

  return this.http.get(url,{headers});
}
  getNewReleases(){
    return this.getQuery('browse/new-releases').pipe(map(data=>{
      return data['albums'].items;
    }))
  }

  getArtistas(termino:string){
    return this.getQuery(`search?q=${termino}&type=artist&limit=10`).pipe(map(data=>{
      return data['artists'].items;
    }))
  }

  getArtista(id:string){
    return this.getQuery(`artists/${id}`);
    // .pipe(map(data=>{
    //   return data['artists'].items;
    // }))
  }

  getTopTracks(id:string){
    return this.getQuery(`artists/${id}/top-tracks?country=us`)
    .pipe(map(data=>{
      return data['tracks'];
    }))
  }
}
