let mensaje = "Hola";
if (true) {
  let mensaje = "Adios";
}

console.log(mensaje);

const OPCIONES = "Todas";
if (true) {
    const OPCIONES = "Ninguna";
}
console.log(OPCIONES);

let nombre:string = "bernardo";
let numero:number = 123;
let booleano:boolean = true;
let hoy:Date = new Date();


hoy = new Date('2020-10-21');

console.log(hoy);

let cualquiera:any;
cualquiera = nombre;
cualquiera = numero;

let apellido:string = "vega";
let edad:number = 23;
let texto = `Hola, ${nombre} ${apellido} (${edad})`;

console.log(texto);

function getNonbre(){
  return "Bernardo";
}

let funcion:string = `${getNonbre()}`;
console.log(funcion);

function activar(quien:string, momento?:string){
  let mensaje:string;
  if (momento) {
      mensaje = `${quien} activo la batisenal en la ${momento}`;
  } else{
    mensaje = `${quien} activo la batisenal`;
  }
  console.log(mensaje);
}

activar("Gordon","tarde");
