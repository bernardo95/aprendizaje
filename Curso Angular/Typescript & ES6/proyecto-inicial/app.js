"use strict";
var mensaje = "Hola";
if (true) {
    var mensaje_1 = "Adios";
}
console.log(mensaje);
var OPCIONES = "Todas";
if (true) {
    var OPCIONES_1 = "Ninguna";
}
console.log(OPCIONES);
var nombre = "bernardo";
var numero = 123;
var booleano = true;
var hoy = new Date();
hoy = new Date('2020-10-21');
console.log(hoy);
var cualquiera;
cualquiera = nombre;
cualquiera = numero;
var apellido = "vega";
var edad = 23;
var texto = "Hola, " + nombre + " " + apellido + " (" + edad + ")";
console.log(texto);
function getNonbre() {
    return "Bernardo";
}
var funcion = "" + getNonbre();
console.log(funcion);
function activar(quien, momento) {
    var mensaje;
    if (momento) {
        mensaje = quien + " activo la batisenal en la " + momento;
    }
    else {
        mensaje = quien + " activo la batisenal";
    }
    console.log(mensaje);
}
activar("Gordon", "tarde");
