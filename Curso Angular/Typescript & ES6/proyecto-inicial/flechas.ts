let miFunction = function(a){
  return a;
}

let miFunctionF = a => a;

let miFunction2 = function(a:number, b:number){
  return a+b;
}

let miFuncion2F = ( a:number, b:number) => a + b;

let miFunction3 = function(nombre:string){
  nombre = nombre.toUpperCase();
  return nombre;
}

let miFuncion3F = (nombre:string) =>{
  nombre = nombre.toUpperCase();
  return nombre;
}
