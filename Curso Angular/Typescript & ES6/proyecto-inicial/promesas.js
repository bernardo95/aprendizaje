"use strict";
var prom1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        console.log("Promesa temrinada");
        resolve();
        // reject();
    }, 1500);
});
prom1.then(function () {
    console.log("Ejecutarme si sale bien");
}, function () {
    console.log("Ejecutarme si sale mal");
});
