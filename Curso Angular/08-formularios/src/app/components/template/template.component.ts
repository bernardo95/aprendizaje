import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styles: [`
    // .ng-invalid.ng-touched:not(form) {
    //   border: 1px solid red;
    }
    `]
})
export class TemplateComponent{
usuario:Object={
  nombre: null,
  apellido: null,
  correo: null,
  pais:"",
  sexo:null,
  acepta:false
}
sexos:string[]=["Hombre","Mujer","Otros"]
paises= [
  {
  codigo:"COL",
  nombre: "Colombia"},
  {
    codigo:"MX",
    nombre:"Mexico"
  }
]
  constructor() { }
  guardar(forma:NgForm){
    console.log(forma);
    console.log(forma.value);
    console.log("usuario", this.usuario)
  }

}
