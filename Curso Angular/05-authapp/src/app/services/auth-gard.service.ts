import { Injectable } from '@angular/core';
import {Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate} from "@angular/router";
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGardService implements CanActivate{

  constructor(private auth:AuthService) {}
  canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot){
    if(this.auth.isAuthenticated()){
      console.log("Paso el gard");
      return true;
    }else{
      console.log("Bloqueado por el gard");
      return false;
    }

  }
}
