import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Mensaje } from '../interface/mensaje.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private itemsCollection: AngularFirestoreCollection<Mensaje>;
  public chats:Mensaje[]=[];
  public usuario:any={ };

  constructor(private afs: AngularFirestore,
              public afAuth: AngularFireAuth) {

      this.afAuth.authState.subscribe(user=>{
        console.log(user)
        if(!user){
          return;
        }else{
          this.usuario.nombre=user.displayName;
          this.usuario.uid=user.uid;
        }
      })

   }

   // login(proveedor:string) {
   //   if(proveedor=='google'){
   //     this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
   //   }else{
   //     this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
   //   }
     login(proveedor: string) {
        if (proveedor === 'google') {
          this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
        } else {
          console.log('FacebookAuthProvider');
          this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider())
            .catch((error) => {
              if (error.code === 'auth/account-exists-with-different-credential') {
                let pendingCred = error.credential;
                let email = error.email;
                this.afAuth.auth.fetchSignInMethodsForEmail(email)
                  .then((methods) => {
                    if (methods[0] === 'google.com') {
                      this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
                        .then((result) => {
                          result.user.linkAndRetrieveDataWithCredential(pendingCred)
                            .then((usercred) => {
                            });
                        })
                      return;
                    }
                  })
              }
            });
        }
     }
     logout() {
       this.usuario={};
       localStorage.clear();
       this.afAuth.auth.signOut();
     }

   cargarMensaje(){
     this.itemsCollection = this.afs.collection<Mensaje>('chats',ref=>ref.orderBy('fecha','desc').limit(5));
     return this.itemsCollection.valueChanges().pipe(map((mensajes:Mensaje[])=>{
       console.log(mensajes);
       this.chats=[];
       for (let mensaje of mensajes) {
           this.chats.unshift(mensaje);
       }
       return this.chats;
     }))
   }

   agregarMensaje(texto:string){
     let mensaje:Mensaje={
       nombre:this.usuario.nombre,
       mensaje:texto,
       fecha: new Date().getTime(),
       uid:this.usuario.uid
     }
     return this.itemsCollection.add(mensaje);
   }
}
