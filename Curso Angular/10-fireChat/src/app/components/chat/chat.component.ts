import { Component,OnInit} from '@angular/core';
import { ChatService } from 'src/app/provider/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styles: []
})
export class ChatComponent implements OnInit  {

mensaje:string="";
elemento:any;

  constructor(private _cs:ChatService) {
    this._cs.cargarMensaje().subscribe( ()=>{
      setTimeout( ()=>{
        this.elemento.scrollTop=this.elemento.scrollHeight;
      },200);

    });
  }

  ngOnInit(){
    this.elemento=document.getElementById('app-mensajes');
  }

enviar_mensaje(){
console.log(this.mensaje);

if (this.mensaje.length==0) {

}else{
  this._cs.agregarMensaje(this.mensaje).then(()=>{
    console.log('mensaje enviado')
    this.mensaje="";
  }).catch((err)=>{
    console.error('Error al enviar', err);
  })
}
}

}
