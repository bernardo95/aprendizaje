let employee = [{
  id:1,
  nombre: 'Bernardo'
},{
  id:2,
  nombre: 'Jose'
},{
  id:3,
  nombre: 'Andres'
}]

let salarios = [{
  id:1,
  salario: 1000
},{
  id:2,
  salario: 2000
}]

let getEmployee = (id,callback) =>{

  let empleadoDB = employee.find(employee =>{
    return employee.id == id;
  })
  if (!empleadoDB) {
    callback(`no existe un empleado con el id ${id}`)
  }else {
    callback(null, empleadoDB);
  }
}

let getSalario = (employee , callback)=>{
  let salarioDB = salarios.find(salarios =>{
    return salarios.id==employee.id;
  })
  if (!salarioDB) {
    callback(`no existe un salario para ${employee.nombre}`)
  }else {
    callback(null, {
      id: employee.id,
      nombre: employee.nombre,
      salario: salarioDB.salario
    });
  }
}
getEmployee(3, (err , employee)=>{
  if (err) {
    console.log(err);
  }else {
    getSalario(employee, (err, resp)=>{
      if (err) {
        return console.log(err);
      }else {
        console.log(`el salario de ${resp.nombre} es de ${resp.salario}`);
      }
    })
  }
});
