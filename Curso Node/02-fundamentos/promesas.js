let employee = [{
  id:1,
  nombre: 'Bernardo'
},{
  id:2,
  nombre: 'Jose'
},{
  id:3,
  nombre: 'Andres'
}]

let salarios = [{
  id:1,
  salario: 1000
},{
  id:2,
  salario: 2000
}]

let getEmployee = (id) => {

  return new Promise( (resolve,reject)=>{

    let empleadoDB = employee.find(employee =>{
      return employee.id == id;
    })

    if (!empleadoDB) {
      reject(`no existe un empleado con el id ${id}`);
    }else {
      resolve(empleadoDB);
    }
  } );

}
let getSalario = (employee)=>{
  return new Promise( (resolve,reject)=>{
    let salarioDB = salarios.find(salarios =>{
      return salarios.id==employee.id;
    })
    if (!salarioDB) {
      reject(`no existe un salario para ${employee.nombre}`)
    }else {
      resolve({
        id: employee.id,
        nombre: employee.nombre,
        salario: salarioDB.salario
      });
    }
  });

}

getEmployee(4).then(employee=>{
    return getSalario(employee);
}).then(resp =>{
  console.log(`el salario de ${resp.nombre} es de ${resp.salario}`);
}).catch(err =>{
  console.log(err);
})
