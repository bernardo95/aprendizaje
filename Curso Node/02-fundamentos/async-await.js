// let getNombre = async ()=>{
//  throw new Error('No existe un nombre')
//    return 'Bernardo';
// };
//
// getNombre().then(resp =>{
//   console.log(resp);
// }).catch( err =>{
//   console.log("Error async "+ err);
// })

let getNombre = ()=>{

   return new Promise( (resolve,reject)=>{
     setTimeout( ()=> {
        resolve('Bernardo')
     }, 2000);
   } )
};

let saludo = async ()=>{
   let nombre = await getNombre();
  return `hola ${nombre}`;
}

saludo().then(resp =>{
  console.log(resp);
})
