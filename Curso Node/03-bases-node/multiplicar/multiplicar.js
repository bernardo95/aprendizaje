const fs = require('fs');

let crearArchivo = (base) =>{
  return new Promise ( (resolve,reject)=>{

    if( !Number(base)){
      reject('No es un numero');
      return;
    }
    let data="";
    for (var i = 0; i <=10; i++) {
      data += base*i;
    }

    fs.writeFile('tabla-2.txt',data,(err)=>{
      if (err){
        reject(err);
      }
      else {
        resolve('El archivo ha sido guardado el archivo');
      }
    });
  } )
}

module.exports = {
  crearArchivo
}
