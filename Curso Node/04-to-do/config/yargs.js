const argv=require('yargs')
                  .command('crear','Crear un elemento por hacer',{
                    descripcion:{
                      demand: true,
                      alias: 'd',
                      desc: 'descripcion de la tarea por hacer'
                    }
                  })
                  .command('actualizar','Actualiza el estado de una tarea',{
                    descripcion:{
                      demand: true,
                      alias: 'd',
                      desc: 'descripcion de la tarea por hacer'
                    },
                    completado:{
                      default: true,
                      alias: 'c',
                      desc: 'Marca como completado la tarea por hacer'
                    }
                    })
                    .command('borrar','Borra una tarea',{
                      descripcion:{
                        demand: true,
                        alias: 'd',
                        desc: 'descripcion de la tarea por hacer'
                      }
                      })
                    .help()
                    .argv;

                    module.exports = {
                      argv
                    }
