let socket = io();
let searchParams = new URLSearchParams(window.location.search);
let label = $('small');


socket.on('connect', () => {
    console.log('Conectado al servidor');
});

socket.on('disconnect', () => {
    console.log('Conectado al servidor');

});

if (!searchParams.has('escritorio')) {
    window.location = 'index.html';
    throw new Error('El escritorio es necesario');
}

let escritorio = searchParams.get('escritorio');
$('h1').text('Escritorio ' + escritorio);

$('button').on('click', () => {
    socket.emit('atenderTicket', { escritorio }, (resp) => {
        if (resp === 'no hay tickets para atender') {
            alert(resp);
            return;
        }
        label.text('Ticket ' + resp.numero);

    });
});