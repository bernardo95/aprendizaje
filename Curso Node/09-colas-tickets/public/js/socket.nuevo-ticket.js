//comando apra establecer la comunicacion

let socket = io();

let label = $('#lblNuevoTicket');

socket.on('connect', () => {
    console.log('Conectado al servidor');
    // socket.emit('estadoActual', null, (ultimoTicket) => {
    //     label.text(ultimoTicket);
    // });

});

socket.on('disconnect', () => {
    console.log('Conectado al servidor');

});

socket.on('estadoActual', (data) => {
    label.text(data.actual);
});

$('button').on('click', () => {
    socket.emit('siguienteTicket', null, (siguienteTicket) => {
        label.text(siguienteTicket);
    });

});