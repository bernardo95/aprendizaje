const express = require('express');
const app = express();
const hbs = require('hbs');
app.use(express.static(__dirname+'/public'));

//Expres hbs engine
hbs.registerPartials(__dirname+'/views/partials');
app.set('view engine','hbs');

//helpers
hbs.registerHelper('getYear',()=>{
  return new Date().getFullYear();
})

app.get('/',(req,res)=>{

  res.render('home',{
    nombre: 'Bernardo'
  });
});
app.get('/home',(req,res)=>{

  res.render('home',{
    nombre: 'Bernardo'
  });
})
app.get('/about',(req,res)=>{

  res.render('about',{
    nombre: 'Bernardo',
    year: new Date().getFullYear()
  });
});

app.listen(3800);
