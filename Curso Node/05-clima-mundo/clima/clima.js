const axios = require('axios');

const getClima = async(lat,lon)=>{
  const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=3eb6b9a36f39b194edd207fcb2cc6389`);
  return resp.data.main.temp;
}

module.exports = {
  getClima
}
