// path: 'api/login'


const { Router } = require('express');
const { check } = require('express-validator');
const { createUser, loginUser, renewToken } = require('../controllers/auth');
const { validateToken } = require('../middlewares/validate_jwt');
const { validateFields } = require('../middlewares/validator_fields');
const router = Router();

router.post('/create', [
    check('name', 'name is required').notEmpty(),
    check('email', 'email is required').isEmail(),
    check('password', 'password is required').notEmpty(),
    validateFields
], createUser);

router.post('/', [
    check('email', 'email is required').isEmail(),
    check('password', 'password is required').notEmpty(),
    validateFields,
], loginUser);

router.get('/renew', [
    validateToken
], renewToken);


module.exports = router;