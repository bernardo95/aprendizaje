//path : api/users

const { Router } = require('express');
const { validateToken } = require('../middlewares/validate_jwt');
const { getUsers } = require('../controllers/users');

const router = Router();

router.get('/', validateToken, getUsers);

module.exports = router;