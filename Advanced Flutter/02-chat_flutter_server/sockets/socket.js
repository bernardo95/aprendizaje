const {
  userConnected,
  userDisconned,
  saveMessage,
} = require("../controllers/socket");
const { verifyJWT } = require("../helpers/jwt");
const { io } = require("../index");

// Mensajes de Sockets
io.on("connection", (client) => {
  console.log("Cliente conectado");
  const [valid, uid] = verifyJWT(client.handshake.headers.authentication);
  if (!valid) {
    return client.disconnect();
  }
  console.log("Cliente autenticado");
  console.log(uid);
  userConnected(uid);

  //join to user a channel
  //channel global, channel private client.id, channel custom clien.join

  client.join(uid);

  client.on("mensaje-personal", async (payload) => {
    console.log(payload);
    await saveMessage();
    io.to(payload.para).emit("mensaje-personal", payload);
  });

  client.on("disconnect", () => {
    console.log("Cliente desconectado");
    userDisconned(uid);
  });
});
