const { response } = require('express');
const jwt = require('jsonwebtoken');

const validateToken = (req, res = response, next) => {

    const token = req.header('Authorization');
    if (!token) {
        return res.status(401).json({
            ok: false,
            msg: 'token is required'
        });
    }
    try {
        const { uid } = jwt.verify(token, process.env.JWT_KEY);
        req.uid = uid;
        next();
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Token not valid'
        });
    }

}

module.exports = {
    validateToken
}