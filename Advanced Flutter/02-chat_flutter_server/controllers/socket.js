const User = require("../models/user");
const Message = require("../models/message");

const userConnected = async (uid) => {
  const user = await User.findOneAndUpdate(uid, { online: true });

  return user;
};

const userDisconned = async (uid) => {
  const user = await User.findOneAndUpdate(uid, { online: false });

  return user;
};

const saveMessage = async (payload) => {
  try {
    const message = new Message(payload);
    await message.save();
    return true;
  } catch (error) {
    return false;
  }
};

module.exports = {
  userConnected,
  userDisconned,
  saveMessage,
};
