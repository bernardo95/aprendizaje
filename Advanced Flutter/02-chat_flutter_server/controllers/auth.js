const { response } = require("express");
const bcryptjs = require("bcryptjs");
const User = require("../models/user");
const { generateJWT } = require("../helpers/jwt");

const createUser = async(req, res = response) => {
    const { email, name, password } = req.body;
    try {
        const existUser = await User.findOne({ email });
        if (existUser) {
            return res.status(400).json({
                ok: false,
                msg: "User already exist",
            });
        }
        const _user = new User({
            email,
            name,
            password,
        });

        //Encrypt password
        const salt = bcryptjs.genSaltSync();
        _user.password = bcryptjs.hashSync(password, salt);

        let user = await _user.save();

        //Generate JWT

        const token = await generateJWT(_user.id);

        return res.status(200).json({
            ok: true,
            msg: "User created",
            user,
            token,
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: "Error al realizar la operacion crear usuario",
            error,
        });
    }
};

const loginUser = async(req, res = response) => {
    try {
        const { email, password } = req.body;
        let user = await User.findOne({ email });

        if (!user) {
            return res.status(400).json({
                ok: false,
                msg: "Email not exist",
            });
        }

        //password compare
        const validPassword = bcryptjs.compareSync(password, user.password);
        if (!validPassword) {
            return res.status(400).json({
                ok: false,
                msg: "Password invalid",
            });
        }
        const token = await generateJWT(user.id);

        return res.status(200).json({
            ok: true,
            msg: "User logged",
            user,
            token,
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: "error al intentar logear el usuario",
            error,
        });
    }
};

const renewToken = async(req, res = response) => {
    try {
        const uid = req.uid;

        let user = await User.findById(uid);

        const token = await generateJWT(uid);

        return res.json({
            ok: true,
            user,
            token,
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: "error al realizar la operacion",
            error,
        });
    }
};
module.exports = {
    createUser,
    loginUser,
    renewToken,
};