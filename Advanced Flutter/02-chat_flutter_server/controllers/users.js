const { response } = require("express");
const UserSchema = require('../models/user');

const getUsers = async(req, res = response) => {
    try {

        const since = Number(req.query.since);
        const from = Number(req.query.from);

        let users = await UserSchema.find({ _id: { $ne: req.uid } }).sort('-online').skip(since).limit(from);

        return res.status(200).json({
            ok: true,
            users,
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'something inexpected happened',
            error
        });
    }
}

module.exports = {
    getUsers
}