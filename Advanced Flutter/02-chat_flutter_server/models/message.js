const { Schema, model } = require("mongoose");

const MessageSchema = new Schema(
  {
    de: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
      trim: true,
    },
    para: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
      trim: true,
    },
    mensaje: {
      type: String,
      required: true,
      trim: true,
    },
  },
  {
    timestamps: true,
  }
);
MessageSchema.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  return object;
});
module.exports = model("Message", MessageSchema);
