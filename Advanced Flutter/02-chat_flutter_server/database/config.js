const mongoose = require('mongoose');

const dbConnection = async() => {
    try {
        let db = await mongoose.connect(process.env.DB_CONNECTION, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,

        });
        if (db.STATES.connected) {
            console.log('DB online');
        } else {
            console.log('Erro al conectar la DB');
        }
    } catch (error) {
        console.log(error);
        throw new Error('Error en la base de datos');
    }
}

module.exports = {
    dbConnection
}