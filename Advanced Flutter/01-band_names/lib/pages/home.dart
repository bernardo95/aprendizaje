import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

import 'package:provider/provider.dart';

import 'package:band_names/models/bad.dart';
import 'package:band_names/services/socket_service.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Band> bands = [];

  @override
  void initState() { 
    final socketService = Provider.of<SocketService>(context,listen: false);
    socketService.socket.on('active-bands', (payload){
      this.bands = (payload as List).map((band) => Band.fromMap(band)).toList();
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() { 
    final socketService = Provider.of<SocketService>(context,listen: false);
    socketService.socket.off('active-bands');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final serverStatus = Provider.of<SocketService>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Band Names',
          style: TextStyle(color: Colors.black87),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 1,
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10),
            child: (serverStatus.serverStatus == ServerStatus.Online) ? Icon(Icons.check_circle,color: Colors.blue[300],):
            Icon(Icons.offline_bolt,color: Colors.red[300],)
          )
        ],
      ),
      body: Column(
        children: [
          if(this.bands.length>0)
          ShowGraph(bands: this.bands),
          Expanded(
            child: ListView.builder(
                itemCount: bands.length,
                itemBuilder: (BuildContext context, int index) {
                  return _bandTitle(bands[index]);
                }),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addNewBand,
        elevation: 1,
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _bandTitle(Band band) {
    final socketService = Provider.of<SocketService>(context,listen: false).socket;
    return Dismissible(
      key: Key(band.id),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection diretion){
        //llamar el borrado al server
        socketService.emit('delete-band', {'id': band.id});
      },
      background: Container(
        padding: EdgeInsets.only(left: 10),
        color: Colors.red,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text('Delete Band',style: TextStyle(color: Colors.white),)),
      ),
          child: ListTile(
        leading: CircleAvatar(
          child: Text(band.name.substring(0, 2)),
          backgroundColor: Colors.blue[100],
        ),
        title: Text(band.name),
        trailing: Text(
          '${band.votes}',
          style: TextStyle(fontSize: 19),
        ),
        onTap: () {
          print(band.id);
           socketService.emit('vote-band', {'id':band.id});
        },
      ),
    );
  }

  _addNewBand() {
    final _textController = new TextEditingController();
    if (Platform.isAndroid) {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('New band name'),
            content: TextField(
              controller: _textController,
            ),
            actions: [
              MaterialButton(
                  child: Text('Add'),
                  elevation: 4,
                  textColor: Colors.blue,
                  onPressed: () => _addBandToList(_textController.text))
            ],
          );
        },
      );
    }

    return showCupertinoDialog(context: context, builder: (BuildContext context){
      return CupertinoAlertDialog(
        title: Text('New band name'),
        content: CupertinoTextField(
          controller: _textController,
        ),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Add'),
            onPressed: ()=> _addBandToList(_textController.text),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: Text('Dismiss'),
            onPressed: ()=> Navigator.pop(context),
          )
        ],
      );
    });
  }

  void _addBandToList(String name) {
    if (name.length > 3) {
    final socketService = Provider.of<SocketService>(context,listen: false).socket;
      socketService.emit('add-band',{'nombre':name});
    }
    Navigator.pop(context);
  }
}

class ShowGraph extends StatelessWidget {
  final List<Band> bands;
  const ShowGraph({Key key, this.bands}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, double> dataMap = new Map();

    bands.forEach((band) {
      dataMap.putIfAbsent(band.name, () => band.votes.toDouble());
    });
  
    return PieChart(
      animationDuration: Duration(seconds: 1),
      chartType: ChartType.disc,
      dataMap: dataMap);
  }
}