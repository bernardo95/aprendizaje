import 'package:flutter/material.dart';


import 'package:provider/provider.dart';


import 'package:band_names/services/socket_service.dart';


class StatusPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  final socketService = Provider.of<SocketService>(context);
  // socketService.socket.emit(event)
    return Scaffold(
      body: Center(
        child: Text('ServerStatus : ${socketService.serverStatus}'),
     ),
     floatingActionButton: FloatingActionButton(
       elevation: 1,
       child: Icon(Icons.message),
       onPressed: (){
         String channel = 'mensaje';
         var payload = {
           'nombre' : 'bernardo',
           'mensaje': 'hola desde flutter'
         };
         socketService.emit(channel, payload);
     }),
   );
  }
}