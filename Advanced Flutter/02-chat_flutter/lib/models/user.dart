// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
    User({
        this.online,
        this.email,
        this.name,
        this.createdAt,
        this.updatedAt,
        this.uid,
    });

    bool online;
    String email;
    String name;
    DateTime createdAt;
    DateTime updatedAt;
    String uid;

    factory User.fromJson(Map<String, dynamic> json) => User(
        online: json["online"],
        email: json["email"],
        name: json["name"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        uid: json["uid"],
    );

    Map<String, dynamic> toJson() => {
        "online": online,
        "email": email,
        "name": name,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "uid": uid,
    };
}
