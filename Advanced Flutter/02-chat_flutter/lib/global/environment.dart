
import 'dart:io';

class PlatformEnv {
  String url;

  PlatformEnv() {
    if (Platform.isAndroid) {
      this.url = 'http://10.0.2.2:3000';
    } else if (Platform.isIOS) {
      this.url = 'http://localhost:3000';
    }
  }
}

class Environment {
  
  static String apiUrl = PlatformEnv().url + '/api';
  static String socketUrl = PlatformEnv().url;
}