

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

Future showAlert({@required BuildContext context,@required  String title,@required  String subtitle}){
  if (Platform.isAndroid) {
    return showDialog(context: context,
    builder: (_){
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20)
        ),
        title: Text(title),
        content: Text(subtitle),
        actions: [
          MaterialButton(
            child: Text('ok'),
            elevation: 1,
            textColor: Colors.blue,
            onPressed: (){
            Navigator.pop(context);
          })
        ],
      );
    }
    );
  }
    return showCupertinoDialog(context: context, builder: (context){
      return CupertinoAlertDialog(
        title: Text(title),
        content: Text(subtitle),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('ok'),
            onPressed: (){
            Navigator.pop(context);
            },
          )
        ],
      );
    });
  
}