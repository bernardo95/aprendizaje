import 'package:chat_flutter/services/socket_service.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:chat_flutter/pages/login_page.dart';
import 'package:chat_flutter/pages/users_page.dart';
import 'package:chat_flutter/services/auth_service.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: checkLoginState(context),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            return Center(
              child: Text('Autenticando...'),
            );
          } else {
            return Center(
              child: Text('Autenticando...'),
            );
          }
        },
      ),
    );
  }

  Future<void> checkLoginState(BuildContext context) async {
    final authService = Provider.of<AuthService>(context, listen: false);
    final authenticated = await authService.isLoggedIn();
    if (authenticated.ok) {
      Provider.of<SocketService>(context, listen: false).connect();
      Navigator.pushReplacement(
          context,
          PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              pageBuilder: (BuildContext context, Animation<double> initial,
                  Animation<double> end) {
                return UsersPage();
              }));
    } else {
      Navigator.pushReplacement(
          context,
          PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              pageBuilder: (BuildContext context, Animation<double> initial,
                  Animation<double> end) {
                return LoginPage();
              }));
    }
  }
}
