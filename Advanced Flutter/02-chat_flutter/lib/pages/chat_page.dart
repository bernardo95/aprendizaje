import 'dart:io';

import 'package:chat_flutter/services/auth_service.dart';
import 'package:chat_flutter/services/chat_service.dart';
import 'package:chat_flutter/services/socket_service.dart';
import 'package:chat_flutter/widgets/chat_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with TickerProviderStateMixin {
  List<ChatMessage> _messages = [];
  ChatService chatService;
  SocketService socketService;
  AuthService authService;
  @override
  void initState() {
    super.initState();
    this.chatService = Provider.of<ChatService>(context, listen: false);
    this.socketService = Provider.of<SocketService>(context, listen: false);
    this.authService = Provider.of<AuthService>(context, listen: false);
    this.socketService.socket.on('mensaje-personal', _listenMesajeController);
  }

  _listenMesajeController(dynamic paylaod) {
    ChatMessage message = new ChatMessage(
        text: paylaod['mensaje'],
        uid: paylaod['de'],
        animationController: new AnimationController(
            vsync: this, duration: Duration(milliseconds: 300)));
    setState(() {
      _messages.insert(0, message);
    });
    message.animationController.forward();
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    for (ChatMessage message in _messages) {
      message.animationController.dispose();
    }
    this.socketService.socket.off('mensaje-personal');
    super.dispose();
  }

  final _textController = TextEditingController();
  final _focusNode = FocusNode();
  bool isWriting = false;

  @override
  Widget build(BuildContext context) {
    final chatService = this.chatService;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 1,
          title: Column(
            children: [
              CircleAvatar(
                maxRadius: 14,
                child: Text(
                  '${chatService.userFrom.name.substring(0, 2)}',
                  style: TextStyle(fontSize: 12),
                ),
                backgroundColor: Colors.blueAccent[100],
              ),
              SizedBox(),
              Text(
                '${chatService.userFrom.name}',
                style: TextStyle(color: Colors.black87, fontSize: 13),
              )
            ],
          ),
        ),
        body: Container(
          child: Column(
            children: [
              Flexible(
                child: ListView.builder(
                  reverse: true,
                  itemCount: _messages.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _messages[index];
                  },
                ),
              ),
              Divider(),
              Container(
                color: Colors.white,
                child: _inputChat(),
              ),
              Divider(),
            ],
          ),
        ));
  }

  Widget _inputChat() {
    return SafeArea(
      bottom: true,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          children: [
            Flexible(
              child: TextField(
                controller: _textController,
                onSubmitted: _handleSubmit,
                onChanged: (String text) {
                  setState(() {
                    if (text.trim().length > 0) {
                      isWriting = true;
                    } else {
                      isWriting = false;
                    }
                  });
                },
                decoration:
                    InputDecoration.collapsed(hintText: 'Enviar mensaje'),
                focusNode: _focusNode,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 4),
              child: Platform.isIOS
                  ? CupertinoButton(
                      child: Text('Enviar'),
                      onPressed: isWriting
                          ? () => _handleSubmit(_textController.text.trim())
                          : null)
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 4.0),
                      child: IconTheme(
                        data: IconThemeData(color: Colors.blue[400]),
                        child: IconButton(
                            highlightColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            icon: Icon(Icons.send),
                            onPressed: isWriting
                                ? () =>
                                    _handleSubmit(_textController.text.trim())
                                : null),
                      )),
            )
          ],
        ),
      ),
    );
  }

  void _handleSubmit(String text) {
    if (text.length == 0) return;

    final newMessage = new ChatMessage(
      uid: '1',
      text: text,
      animationController: AnimationController(
          vsync: this, duration: Duration(milliseconds: 500)),
    );

    setState(() {
      _messages.insert(0, newMessage);
      newMessage.animationController.forward();
      isWriting = false;
      _textController.clear();
      _focusNode.requestFocus();
    });
    final payload = {
      'de': this.authService.user.uid,
      'para': this.chatService.userFrom.uid,
      'mensaje': text,
    };
    this.socketService.emit('mensaje-personal', payload);
  }
}
