import 'package:chat_flutter/services/chat_service.dart';
import 'package:chat_flutter/services/users_service.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:chat_flutter/services/socket_service.dart';
import 'package:chat_flutter/services/auth_service.dart';

import 'package:chat_flutter/models/user.dart';

class UsersPage extends StatefulWidget {
  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  final usersServices = UsersService();
  List<User> users = [];

  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    _onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketService>(context);

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            authService.user.name,
            style: TextStyle(color: Colors.black54),
          ),
          elevation: 1,
          backgroundColor: Colors.white,
          leading: IconButton(
              icon: Icon(Icons.exit_to_app, color: Colors.black54),
              onPressed: () {
                AuthService.deleteToken();
                socketService.disconnect();
                Navigator.pushReplacementNamed(context, 'login');
              }),
          actions: [
            Container(
                margin: EdgeInsets.only(right: 10),
                child: (socketService.serverStatus == ServerStatus.Online)
                    ? Icon(
                        Icons.check_circle,
                        color: Colors.blue[400],
                      )
                    : Icon(
                        Icons.offline_bolt,
                        color: Colors.red,
                      ))
          ],
        ),
        body: SmartRefresher(
          controller: _refreshController,
          child: _ListViewUsers(usuarios: users),
          enablePullDown: true,
          header: WaterDropHeader(
            complete: Icon(
              Icons.check,
              color: Colors.blue[400],
            ),
            waterDropColor: Colors.blue[400],
          ),
          onRefresh: _onRefresh,
        ));
  }

  void _onRefresh() async {
    this.users = await usersServices.getUsers(since: 1, from: 10);
    setState(() {});
    // monitor network fetch
    // await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
}

class _ListViewUsers extends StatelessWidget {
  final List<User> usuarios;
  const _ListViewUsers({Key key, @required this.usuarios}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return UserListTile(user: usuarios[index]);
        },
        separatorBuilder: (BuildContext context, int index) {
          return Divider();
        },
        itemCount: usuarios.length);
  }
}

class UserListTile extends StatelessWidget {
  final User user;

  const UserListTile({
    Key key,
    @required this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        final chatService = Provider.of<ChatService>(context, listen: false);
        chatService.userFrom = user;
        Navigator.pushNamed(context, 'chat');
      },
      title: Text(user.name),
      subtitle: Text(user.email),
      leading: CircleAvatar(
        backgroundColor: Colors.blue[200],
        child: Text(user.name.substring(0, 2)),
      ),
      trailing: Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: user.online ? Colors.green : Colors.red),
      ),
    );
  }
}
