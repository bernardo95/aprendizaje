import 'package:flutter/material.dart';

import 'package:chat_flutter/services/socket_service.dart';
import 'package:provider/provider.dart';


import 'package:chat_flutter/services/auth_service.dart';


import 'package:chat_flutter/helpers/show_alert.dart';
import 'package:chat_flutter/widgets/btn_login.dart';
import 'package:chat_flutter/widgets/custom_input.dart';
import 'package:chat_flutter/widgets/labels_login.dart';
import 'package:chat_flutter/widgets/logo_login.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
            child: Container(
              height: MediaQuery.of(context).size.height*0.9,
              child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Logo(titulo:'Messenger'),
                _Form(),
                Labels(ruta: 'register',titulo: '¿No tienes cuenta?',subtitulo: 'Crea una ahora!',),
                Text(
                  'Terminos y condiciones de uso',
                  style:
                      TextStyle(color: Colors.black87, fontWeight: FontWeight.w400),
                ),
              ],
          ),
            ),
        ),
      ),
    );
  }
}



class _Form extends StatefulWidget {
  _Form({Key key}) : super(key: key);

  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
    final emailController = TextEditingController();
    final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: true);
    final socketService = Provider.of<SocketService>(context);

    return Container(
      margin: EdgeInsets.only(top: 40),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: [
          CustomInput(icon: Icons.mail_outline,placeHolder: 'Correo',textEditingController: emailController,textInputType: TextInputType.emailAddress,),
          CustomInput(icon: Icons.lock_outline,placeHolder: 'Contraseña',textEditingController: passwordController,obscureText: true),
          BotonLogin(text: 'Ingrese',onPress: (authService.authenticating) ? null : () async {
            FocusScope.of(context).unfocus();
            final auth = await authService.login(emailController.text.trim(), passwordController.text.trim());
            if (auth.ok) {
              socketService.connect();
              Navigator.pushReplacementNamed(context, 'users');
            }else{
              showAlert(context: context,title: 'Credenciales incorrectas',subtitle: auth.msg);
            }
          },)
        ],
      ),
    );
  }
}

