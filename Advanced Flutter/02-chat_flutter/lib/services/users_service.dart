import 'package:chat_flutter/global/environment.dart';
import 'package:chat_flutter/models/user.dart';
import 'package:chat_flutter/models/users_response.dart';
import 'package:chat_flutter/services/auth_service.dart';
import 'package:http/http.dart' as http;

class UsersService {
  Future<List<User>> getUsers({int since, int from}) async {
    List<User> users = [];
    final headers = {
      'Content-Type': 'application/json',
      'Authorization': await AuthService.getToken()
    };
    try {
      http.Response response =
          await http.get('${Environment.apiUrl}/users', headers: headers);
      if (response.statusCode == 200) {
        final usersResponse = usersResponseFromJson(response.body);
        return usersResponse.users;
      } else if (response.statusCode == 500) {
        return users;
      }
    } catch (e) {
      return users;
    }
  }
}
