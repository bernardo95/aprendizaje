import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:http/http.dart' as http;

import 'package:chat_flutter/models/login_response.dart';
import 'package:chat_flutter/global/environment.dart';
import 'package:chat_flutter/models/user.dart';

class AuthService with ChangeNotifier {
  User user = User();
  bool _authenticating = false;

  bool get authenticating => this._authenticating;

  set authenticating(bool value) {
    this._authenticating = value;
    notifyListeners();
  }

  // Create storage
  final _storage = new FlutterSecureStorage();

  static Future<String> getToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    return token;
  }

  static Future<void> deleteToken() async {
    final _storage = new FlutterSecureStorage();
    await _storage.delete(key: 'token');
  }

  Future<LoginResponse> login(String email, String password) async {
    this.authenticating = true;

    final data = {'email': email, 'password': password};
    final headers = {'Content-Type': 'application/json'};
    final http.Response response = await http.post(
        '${Environment.apiUrl}/login',
        body: jsonEncode(data),
        headers: headers);

    LoginResponse login;
    if (response.statusCode == 200) {
      login = loginResponseFromJson(response.body);
      this.user = login.user;
      await this._saveToken(login.token);
    } else if (response.statusCode == 400) {
      login = LoginResponse();
      login.msg = jsonDecode(response.body)['msg'];
      login.ok = false;
    }

    this.authenticating = false;
    return login;
  }

  Future<LoginResponse> register(String name, String email, String password) async {
     this.authenticating = true;

    final data = { 'name': name , 'email': email, 'password': password};
    final headers = {'Content-Type': 'application/json'};
    final http.Response response = await http.post(
        '${Environment.apiUrl}/login/create',
        body: jsonEncode(data),
        headers: headers);

    LoginResponse register;
    if (response.statusCode == 200) {
      register = loginResponseFromJson(response.body);
      this.user = register.user;
      await this._saveToken(register.token);
    } else if (response.statusCode == 400) {
      register = LoginResponse();
      register.msg = jsonDecode(response.body)['msg'];
      register.ok = false;
    }

    this.authenticating = false;
    return register;
  }
  Future<void> logout() async {
    await _deleteToken();
  }

  Future _saveToken(String token) async {
    return await _storage.write(key: 'token', value: token);
  }

  Future _deleteToken() async {
    return await _storage.delete(key: 'token');
  }

  Future<LoginResponse> isLoggedIn() async{
    final token = await this._storage.read(key: 'token');
    final headers = {'Content-Type': 'application/json','Authorization': token};

    final http.Response response = await http.get('${Environment.apiUrl}/login/renew',headers: headers);
    LoginResponse login;
    if (response.statusCode == 200) {
      login = loginResponseFromJson(response.body);
      this.user = login.user;
      await this._saveToken(login.token);
    } else if (response.statusCode == 500) {
      await this.logout();
      login = LoginResponse();
      login.msg = jsonDecode(response.body)['msg'];
      login.ok = false;
    }
    return login;
  }
}
