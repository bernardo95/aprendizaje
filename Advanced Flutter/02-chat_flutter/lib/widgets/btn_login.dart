import 'package:flutter/material.dart';

class BotonLogin extends StatelessWidget {
  final String text;
  final Function onPress;
  const BotonLogin({Key key,@required this.text,@required this.onPress}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        elevation: 2,
        highlightElevation: 5,
        color: Colors.blue,
        shape: StadiumBorder(),
        child: Container(
          width: double.infinity,
          height: 45,
          child: Center(
            child: Text(
              this.text,
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
          ),
        ),
        onPressed: this.onPress
    );
  }
}
