import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  final String titulo;

  const Logo({Key key,@required this.titulo}) : super(key: key); 
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 170,
        margin: EdgeInsets.all(50),
        child: Column(
          children: [
            Image(
              image: AssetImage('assets/images/tag-logo.png'),
            ),
            SizedBox(
              height: 25,
            ),
            Text(
              this.titulo,
              style: TextStyle(fontSize: 30),
            )
          ],
        ),
      ),
    );
  }
}