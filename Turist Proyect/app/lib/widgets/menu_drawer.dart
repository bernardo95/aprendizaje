import 'package:app/providers/login_state.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class MenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UsuarioInstancia usuario = Provider.of<UsuarioInstancia>(context);
    LoginState loginProviderUser = Provider.of<LoginState>(context);
    // String imagen;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  child: Consumer<LoginState>(
                    builder:
                        (BuildContext context, LoginState value, Widget child) {
                      
                      if (value.isNormalUser()) {
                        
                        return FutureBuilder(
                          future: usuario.getPhotoUrl(),
                          builder: (BuildContext context,
                              AsyncSnapshot<dynamic> snapshot) {
                            print(snapshot.data);
                            return CircleAvatar(
                                radius: 30,
                                backgroundImage: (snapshot.data == null)
                                    ? AssetImage('assets/img/no-profile.jpg')
                                    : NetworkImage(snapshot.data));
                          },
                        );
                      }
                      return FutureBuilder(
                        future: usuario.getPhotoUrl(),
                        builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) {
                          return CircleAvatar(
                            radius: 30,
                            backgroundImage: (snapshot.data == null)
                                ? AssetImage('assets/img/no-profile.jpg')
                                : NetworkImage(snapshot.data),
                          );
                        },
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                    padding: EdgeInsets.only(bottom: 5.0),
                    child: FutureBuilder(
                        future: usuario.getNombre(),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          return Text(
                            snapshot.data ?? '',
                            style:
                                TextStyle(fontSize: 17.0, color: Colors.white),
                          );
                        })),
              ],
            ),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/img/menu-img.jpg'),
                    fit: BoxFit.cover)),
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.blue),
            title: Text(
              'Inicio',
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            onTap: () {},
          ),
          ListTile(
              leading: Icon(Icons.account_circle, color: Colors.blue),
              title:
                  Text('Cuenta', style: TextStyle(fontWeight: FontWeight.w600)),
              onTap: () {
                Navigator.popAndPushNamed(context, 'feedPage');
              }),
          ListTile(
            leading: Icon(Icons.notifications_active, color: Colors.blue),
            title: Text('Notificaciones',
                style: TextStyle(fontWeight: FontWeight.w600)),
            onTap: () {},
          ),
          ListTile(
              leading: Icon(Icons.settings, color: Colors.blue),
              title: Text('Configuracion',
                  style: TextStyle(fontWeight: FontWeight.w600)),
              onTap: () {}),
          ListTile(
            leading: Icon(Icons.exit_to_app, color: Colors.blue),
            title: FutureBuilder(
              future: usuario.getId(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                return (snapshot.data == null)
                    ? Text('Ingresar',
                        style: TextStyle(fontWeight: FontWeight.w600))
                    : Text('Salir',
                        style: TextStyle(fontWeight: FontWeight.w600));
              },
            ),
            onTap: () async {
              var logged = await usuario.isLoggedFunction();
              if (logged) {
                Navigator.pushNamed(context, 'app');
                // usuario.clear();
                loginProviderUser.logout(LoginProvider.FACEBBOK);
                loginProviderUser.logout(LoginProvider.GOOGLE);
                loginProviderUser.logout(LoginProvider.NORMAL);
              }
              if (!logged) {
                Navigator.pushNamed(context, 'loginPage');
              }
            }, //
          ),
        ],
      ),
    );
  }
}
