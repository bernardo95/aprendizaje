import 'package:app/pages/tab/tab_page.dart';
import 'package:app/providers/enviroment.dart';
import 'package:app/providers/login_state.dart';
import 'package:app/providers/sitios_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:app/route_generator.dart';
import 'package:app/theme/tema.dart';
import 'package:flutter/material.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_)=>Enviroment()),
        ChangeNotifierProvider(create: (_)=>SitioProvider(),),
        ChangeNotifierProvider(create: (_)=>UsuarioInstancia()),
        ChangeNotifierProvider(create: (_)=>LoginState())
      ],
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'), // English
          const Locale('es'), //Spanish
        ],
        initialRoute: 'feedPage',
        debugShowCheckedModeBanner: false,
        // home: MyBottomNavigationBar(),
        onGenerateRoute: RouteGenerator.generateRoute,
        theme: miTema,
        // initialRoute: 'registroPage',
      ),
    );
  }
}
