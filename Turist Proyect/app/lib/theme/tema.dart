import 'package:flutter/material.dart';

final miTema = ThemeData(
  fontFamily: 'BalooChettan',
  appBarTheme: AppBarTheme(
    color: Color.fromRGBO(0, 140, 80, 0.75),
  )
);