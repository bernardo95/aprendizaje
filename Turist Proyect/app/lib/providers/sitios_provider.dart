import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:app/models/sitio_model.dart';

class SitioProvider with ChangeNotifier {
  SitioProvider(){
    this.getSitios();
  }
  final String urlLocal = 'http://10.0.2.2:3000';
  final String urlHeroku = 'https://turist-app.herokuapp.com';

  bool _cargando = false;

  List<Sitio> sitios = new List();

  Future<List<Sitio>> _procesarRespuesta(String url) async {
    http.Response resp = await http.get('$url/sitios');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['sitiosDB'];
    final sitios = new Sitios.fromJsonList(decodedData);
    return sitios.items;
  }

  Future<List<Sitio>> getSitios() async {
    if (_cargando) return [];
    _cargando = true;
    final resp = await _procesarRespuesta(urlLocal);
    sitios.addAll(resp);
    _cargando = false;
    notifyListeners();
    return resp;
  }
}
