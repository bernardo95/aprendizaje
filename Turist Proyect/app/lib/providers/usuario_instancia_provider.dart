import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
class UsuarioInstancia with ChangeNotifier {

  Future<bool> isLoggedFunction()async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return (prefs.getBool('logged')==null) ? false : prefs.getBool('logged');
    } catch (e) {
      print(e);
      return false;
    }
  }
  Future<dynamic> getId() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('id');
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<dynamic> getNombre() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('nombre');
    } catch (e) {
      print(e);

      return null;
    }
  }

  Future<dynamic> getEmail() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('email');
    } catch (e) {
      print(e);

      return null;
    }
    
  }

  Future<dynamic> getPhotoUrl() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('photoUrl');
    } catch (e) {
      print(e);

      return null;
    }
    
  }

  Future<void> clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    notifyListeners();
  }

}
