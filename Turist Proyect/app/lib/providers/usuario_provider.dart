import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:app/models/usuario_model.dart';

class UsuarioProvider {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final String url = 'http://10.0.2.2:3000';
  Future<bool> _procesarRespuesta(String url, String email) async {
    http.Response resp = await http.get('$url/usuario/exist/$email');
    final decode = json.decode(resp.body);
    bool data = decode['callback'];
    // print(data);
    return data;
  }

  Future<bool> getEmailExist(String email) async {
    final resp = await _procesarRespuesta(url, email);
    return resp;
  }

  Future<Usuario> postUsuario(Usuario user) async {
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final map = user.toMap();
    final userJson = json.encode(map);
    // print(userJson);
    http.Response resp = await http.post('$url/usuario', body: userJson, headers: header);
    Map<String, dynamic> decodedResp = json.decode(resp.body);
    final deco = decodedResp['usuario'];
    final respMapeadatoModel = Usuario.fromMap(deco);
    // print(respMapeadatoModel);
    if (resp.statusCode== 500 || resp.statusCode==400) {
      return new Usuario();
    } else {
    return respMapeadatoModel;
    }
  }

  Future<dynamic> postImageUsuario(File file,String id) async {
    if(id==null) return null;
    var stream = new http.ByteStream((file.openRead()));
    // string to uri
    var uri = Uri.parse("$url/usuario/upload/image/$id");
    // get length for http post
    var length = await file.length();
    // new multipart request
    var request = new http.MultipartRequest("POST", uri);
    var multipartFile = new http.MultipartFile('photo',stream,length,filename: file.path,);
    // add multipart form to request
    request.files.add(multipartFile);
    // send request
    var response = await request.send();
    if (response.statusCode == 200) {
      return response;
    } else {
      return null;
    }
  }
  Future<Usuario> crearCuenta(Usuario user,File file)async{

    Usuario respId;
    dynamic resp2;
    if (file == null) {
     respId = await postUsuario(user);
     return respId;
    } else {
     respId = await postUsuario(user);
     resp2 = await postImageUsuario(file, respId.id);
     if (resp2==null) {
       return new Usuario();
     }else{
       respId.imgProfile = file;
      await _auth.createUserWithEmailAndPassword(email: user.email, password: user.password);
       return respId;
     }
    }

  }
}

