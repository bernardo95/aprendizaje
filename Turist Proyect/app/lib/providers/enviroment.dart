import 'package:flutter/cupertino.dart';

class Enviroment with ChangeNotifier {
  String _urlLocal = "http://localhost:3000:";
  String _urlRemota = "https://turist-app.herokuapp.com";

  String get urlLocal => this._urlLocal;
  String get urlRemota => this._urlRemota;
}
