// import 'dart:math';

bool isString(String s) {
  if(s.contains(new RegExp(r'[0-9]'))) return false;
  if (s.isEmpty) return false;
  if (s is String) {
    return true;
  } else {
    return false;
  }
}

bool isNumeric(String s) {
  if (s.isEmpty) return false;
  final n = num.tryParse(s);
  return (n == null) ? false : true;
}

bool dateVerify(DateTime date){
  final hoy = DateTime.now();
  if (date == null) return false;
  if (date==hoy) return false;
  if(date.isAfter(hoy)){
    return false;
  }
  return true;
}

bool emailValid(String email){
  Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp= new RegExp(pattern);
  if (regExp.hasMatch(email)) {
    return true;
  }else{
    return false;
  }
}
bool passwordMatch(String password1,String password2){
  if (password1==password2) {
    return true;
  } else {
    return false;
  }
}
