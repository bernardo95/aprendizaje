import 'dart:convert';

class Sitios {
  List<Sitio> items = new List();
  Sitios();

  Sitios.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final sitio = new Sitio.fromJson(item);
      items.add(sitio);
    }
  }
}

Sitio sitioFromJson(String str) => Sitio.fromJson(json.decode(str));

String sitioToJson(Sitio data) => json.encode(data.toJson());

class Sitio {
  String id;
  String nombre;
  List<dynamic> imagenes;
  String descripcion;
  Map<String, dynamic> horario;
  Map<String, dynamic> ubicacion;
  String categoria;
  String facilidadAcceso;
  List<dynamic> imagesFiles;

  Sitio({
    this.id,
    this.nombre,
    this.imagenes,
    this.descripcion,
    this.horario,
    this.ubicacion,
    this.categoria,
    this.facilidadAcceso,
  });

  factory Sitio.fromJson(Map<String, dynamic> json) => Sitio(
        id: json["_id"],
        nombre: json["nombre"],
        imagenes: json["imagenes"],
        descripcion: json["descripcion"],
        horario: json["horario"],
        ubicacion: json["ubicacion"],
        categoria: json["categoria"],
        facilidadAcceso: json["facilidad_acceso"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "nombre": nombre,
        "imagenes": imagenes,
        "descripcion": descripcion,
        "horario": horario,
        "ubicacion": ubicacion,
        "categoria": categoria,
        "facilidad_acceso": facilidadAcceso,
      };
}
