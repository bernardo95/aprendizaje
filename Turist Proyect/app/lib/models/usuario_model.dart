

import 'dart:convert';

import 'dart:io';

Usuario usuarioFromJson(String str) => Usuario.fromMap(json.decode(str));

String usuarioToJson(Usuario data) => json.encode(data.toMap());

class Usuario {
    String id;
    String nombre;
    String apellido;
    String email;
    String password;
    String photoUrl;
    File imgProfile;
    String role;
    String fechaNacimiento;
    String genero;
    bool google;
    bool facebook;
    bool estado;

    Usuario({
        this.id,
        this.nombre,
        this.apellido,
        this.email,
        this.password,
        this.photoUrl,
        this.imgProfile,
        this.role,
        this.fechaNacimiento,
        this.genero,
        this.google,
        this.facebook,
        this.estado,
    });

    factory Usuario.fromMap(Map<String, dynamic> json) => Usuario(
        id: json["_id"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        email: json["email"],
        password: json["password"],
        photoUrl: json["photoUrl"],
        imgProfile: json["img_profile"],
        role: json["role"],
        fechaNacimiento: json["fecha_nacimiento"],
        genero: json["genero"],
        google: json["google"],
        facebook: json["facebook"],
        estado: json["estado"],
    );

    Map<String, dynamic> toMap() => {
        "_id": id,
        "nombre": nombre,
        "apellido": apellido,
        "email": email,
        "password": password,
        "photoUrl": photoUrl,
        "img_profile": imgProfile,
        "role": role,
        "fecha_nacimiento": fechaNacimiento,
        "genero": genero,
        "google": google,
        "facebook": facebook,
        "estado": estado,
    };
}
