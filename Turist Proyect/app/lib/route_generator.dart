import 'package:app/main.dart';
import 'package:app/pages/login/login.dart';
import 'package:app/pages/menu/botton_navigation_menu.dart';
import 'package:app/pages/registro/registro.dart';
import 'package:app/pages/registro/registro_page2.dart';
import 'package:app/pages/registro/registro_page3.dart';
import 'package:app/pages/registro/registro_page4.dart';
import 'package:app/pages/registro/registro_page5.dart';
import 'package:app/pages/registro/registro_page6.dart';
import 'package:app/pages/sitios/feed_page.dart';
import 'package:app/pages/sitios/sitio_turistico.dart';
import 'package:app/test/test.dart';
import 'package:flutter/material.dart';


class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    //Gettings arguments passed in while callung navigator.pushNamed()
    final args = settings.arguments;
    switch (settings.name) {
      case 'feedPage':
        return MaterialPageRoute(builder: (_) => FeedPage());
      case 'sitioTuristico':
        return MaterialPageRoute(
            builder: (_) => SitioTuristicoPage(
                  sitio: args,
                ));
      case 'loginPage':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case 'menuPage':
        return MaterialPageRoute(builder: (_)=>MenuBottonPage());
      case 'registroPage':
        return MaterialPageRoute(builder: (_) => RegistroPage(data: args,));
      case 'registroPage2':
        return MaterialPageRoute(builder: (_) => RegistroPage2(data: args,));
      case 'registroPage3':
        return MaterialPageRoute(builder: (_) => RegistroPage3(data: args,));
      case 'registroPage4':
        return MaterialPageRoute(builder: (_) => RegistroPage4(data: args,));
      case 'registroPage5':
        return MaterialPageRoute(builder: (_) => RegistroPage5(data: args,));
      case 'registroPage6':
        return MaterialPageRoute(builder: (_) => RegistroPage6(data: args,));
      case 'app':
        return MaterialPageRoute(builder: (_)=> MyApp());
      case 'test':
        return MaterialPageRoute(builder: (_)=> TestPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
