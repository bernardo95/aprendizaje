import 'package:app/models/usuario_model.dart';
import 'package:app/providers/login_state.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:app/widgets/customShapeClipper.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:app/utils/validators.dart' as utils;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController _controllerCorreo = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          _fondo(context),
          _botonAtras(context),
          // _LoginRedesSociales(),
          _loginFormulario(context),
        ],
      ),
    );
  }

  Widget _botonRegistro() {
    return FlatButton(
        onPressed: () {
          Navigator.pushNamed(context, 'registroPage');
        },
        child: Text(
          '¿No tienes cuenta aún, registrate aquí?',
          style: TextStyle(fontSize: 18.0),
        ));
  }

  _loginFormulario(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        // width: size.width*0.6,
        // height: size.height * 0.5,
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SafeArea(
                  child: Container(
                height: size.height * 0.17,
              )),
              Text(
                'Logo Aqui',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              SizedBox(
                height: size.height * 0.1,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: TextFormField(
                  controller: _controllerCorreo,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Correo',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {},
                  validator: (value) {
                    if (utils.emailValid(value)) {
                      return null;
                    } else {
                      return 'Ingrese un correo valido';
                    }
                  },
                ),
              ),
              SizedBox(
                width: size.width * 0.4,
                child: TextFormField(
                  controller: _controllerPassword,
                  obscureText: true,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Contraseña',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {},
                  validator: (value) {
                    if (value.length < 5) {
                      return 'Contraseña muy corta';
                    } else {
                      return null;
                    }
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: RaisedButton(
                  onPressed: () {
                    _submit();
                  },
                  color: Color.fromRGBO(0, 140, 80, 0.75),
                  textColor: Colors.white,
                  child: Text(
                    'Ingresar',
                    style: TextStyle(fontSize: 19),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              _LoginRedesSociales(),
              SizedBox(
                height: 50,
              ),
              _botonRegistro()
            ],
          ),
        ),
      ),
    );
  }

  void _submit() {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      logear();
    }
  }

  Future<void> logear() async {
    Usuario usuario = Usuario(email: _controllerCorreo.text, password: _controllerPassword.text);
    LoginState _loginState = Provider.of<LoginState>(context,listen: false);
    await Provider.of<LoginState>(context, listen: false).loginNormal(loginProvider: LoginProvider.GOOGLE,usuario: usuario);
    Usuario currentUserNodeJS =await Provider.of<LoginState>(context, listen: false).currentUserNodeJS();
    if (currentUserNodeJS != null) {
      Navigator.pushNamed(context, 'app');
    }
    if (_loginState.existMessageError() != '') {
      mensajeErrorLogin(context, _loginState);
    }
  }

  Future<void> mensajeErrorLogin(BuildContext context, LoginState loginState) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('Ocurrio un error'),
            content: Text(loginState.existMessageError()),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'))
            ],
          );
        });
    loginState.setMessageError = '';
  }
}

Widget _fondo(BuildContext context) {
  final size = MediaQuery.of(context).size;
  return Stack(
    children: <Widget>[
      ClipPath(
        clipper: CustomShapeClipper(),
        child: Container(
          height: size.height * 0.4,
          color: Color.fromRGBO(0, 140, 80, 0.75),
        ),
      )
    ],
  );
}

Widget _botonAtras(BuildContext context) {
  final size = MediaQuery.of(context).size;

  return Container(
    // alignment: Alignment.topCenter,
    child: IconButton(
        padding: EdgeInsets.only(left: 10.0, top: size.height * 0.07),
        alignment: Alignment.topLeft,
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
          size: 30.0,
        ),
        onPressed: () {
          Navigator.pushNamed(context, 'app');
        }),
  );
}

class _LoginRedesSociales extends StatelessWidget {
  const _LoginRedesSociales({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LoginState _loginState = Provider.of<LoginState>(context);

    return Center(
      child: Consumer<LoginState>(
        builder: (BuildContext context, LoginState value, Widget child) {
          if (value.isLoading()) {
            return CircularProgressIndicator();
          } else {
            return child;
          }
        },
        child: Container(
          width: 200,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FloatingActionButton(
                  heroTag: 'googleBoton',
                  backgroundColor: Color(0xff4285F4),
                  child: FaIcon(FontAwesomeIcons.google),
                  onPressed: () async {
                    await Provider.of<LoginState>(context, listen: false)
                        .login(loginProvider: LoginProvider.GOOGLE);
                    FirebaseUser user =
                        await Provider.of<LoginState>(context, listen: false)
                            .currentUser();
                    if (user != null) {
                      Navigator.pushNamed(context, 'app');
                    }
                    if (_loginState.existMessageError() != '') {
                      mensajeErrorLogin(context, _loginState);
                    }
                  }),
              FloatingActionButton(
                  heroTag: 'facebookBoton',
                  backgroundColor: Color(0xff3b5998),
                  child: FaIcon(FontAwesomeIcons.facebook),
                  onPressed: () async {
                    await Provider.of<LoginState>(context, listen: false)
                        .login(loginProvider: LoginProvider.FACEBBOK);
                    FirebaseUser user =
                        await Provider.of<LoginState>(context, listen: false)
                            .currentUser();
                    if (user != null) {
                      Navigator.pushNamed(context, 'app');
                    }
                    if (_loginState.existMessageError() != '') {
                      mensajeErrorLogin(context, _loginState);
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> mensajeErrorLogin(
      BuildContext context, LoginState loginState) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('Ocurrio un error'),
            content: Text(loginState.existMessageError()),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'))
            ],
          );
        });
    loginState.setMessageError = '';
  }
}

//   _loginFormulario(BuildContext context) {
//     final size = MediaQuery.of(context).size;
//     return Center(
//       child: Container(
//         // width: size.width*0.6,
//         // height: size.height * 0.5,
//         child: Form(
//           key: formKey,
//           child: Column(
//             children: <Widget>[
//               SafeArea(
//                   child: Container(
//                 height: size.height * 0.17,
//               )),
//               Text(
//                 'Logo Aqui',
//                 style: TextStyle(color: Colors.white, fontSize: 30),
//               ),
//               SizedBox(
//                 height: size.height * 0.1,
//               ),
//               SizedBox(
//                 width: size.width * 0.4,
//                 child: TextFormField(
//                   controller: _controllerCorreo,
//                   style: TextStyle(
//                     fontSize: 22.0,
//                   ),
//                   keyboardType: TextInputType.emailAddress,
//                   decoration: InputDecoration(
//                     focusedBorder: UnderlineInputBorder(
//                       borderSide:
//                           BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
//                     ),
//                     hintText: 'Correo',
//                     focusColor: Color.fromRGBO(0, 140, 80, 0.75),
//                     hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
//                     hintStyle: TextStyle(fontWeight: FontWeight.w500),
//                   ),
//                   onSaved: (value) {},
//                   validator: (value) {
//                     if (utils.emailValid(value)) {
//                       return null;
//                     } else {
//                       return 'Ingrese un correo valido';
//                     }
//                   },
//                 ),
//               ),
//               SizedBox(
//                 width: size.width * 0.4,
//                 child: TextFormField(
//                   controller: _controllerPassword,
//                   obscureText: true,
//                   style: TextStyle(
//                     fontSize: 22.0,
//                   ),
//                   keyboardType: TextInputType.text,
//                   decoration: InputDecoration(
//                     focusedBorder: UnderlineInputBorder(
//                       borderSide:
//                           BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
//                     ),
//                     hintText: 'Contraseña',
//                     focusColor: Color.fromRGBO(0, 140, 80, 0.75),
//                     hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
//                     hintStyle: TextStyle(fontWeight: FontWeight.w500),
//                   ),
//                   onSaved: (value) {},
//                   validator: (value) {
//                     if (value.length < 5) {
//                       return 'Contraseña muy corta';
//                     } else {
//                       return null;
//                     }
//                   },
//                 ),
//               ),
//               SizedBox(
//                 height: 15,
//               ),
//               SizedBox(
//                 width: size.width * 0.4,
//                 child: RaisedButton(
//                   onPressed: () {
//                     _submit();
//                   },
//                   color: Color.fromRGBO(0, 140, 80, 0.75),
//                   textColor: Colors.white,
//                   child: Text(
//                     'Ingresar',
//                     style: TextStyle(fontSize: 19),
//                   ),
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(8.0)),
//                 ),
//               ),
//               SizedBox(
//                 height: 50,
//               ),
//               _LoginRedesSociales(),
//               SizedBox(height: 50,),
//               _botonRegistro()
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   // void _submit() {
//   //   if (!formKey.currentState.validate()) return;
//   //   if (formKey.currentState.validate()) {
//   //     logear();
//   //   }
//   // }

//   // void logear() async {
//   //   Usuario usuario = Usuario(email: _controllerCorreo.text , password: _controllerPassword.text);

//   //   // LoginState _loginState = Provider.of<LoginState>(context);
//   //   await Provider.of<LoginState>(context, listen: false).login(loginProvider: LoginProvider.NORMAL,usuario: usuario);
//   //   Usuario user = await Provider.of<LoginState>(context, listen: false).currentUserNodeJS();
//   //   LoginState _loginState = Provider.of<LoginState>(context);
//   //   if (user != null) {
//   //     Navigator.pushNamed(context, 'app');
//   //   }
//   //   if (_loginState.existMessageError() != '') {
//   //     mensajeErrorLogin(context, _loginState);
//   //   }
//   // }

//   void mostrarAlerta() {
//     showDialog(
//         barrierDismissible: false,
//         context: context,
//         builder: (BuildContext contex) {
//           return AlertDialog(
//             shape:
//                 RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//             title: Text('Información incorrecta'),
//             actions: <Widget>[
//               FlatButton(
//                   onPressed: () => Navigator.of(context).pop(),
//                   child: Text(
//                     'ok',
//                     style: TextStyle(
//                         fontSize: 20, color: Color.fromRGBO(0, 140, 80, 0.75)),
//                   ))
//             ],
//           );
//         });
//   }

//   Future<void> mensajeErrorLogin(
//       BuildContext context, LoginState loginState) async {
//     await showDialog(
//         barrierDismissible: true,
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             shape:
//                 RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//             title: Text('Ocurrio un error'),
//             content: Text(loginState.existMessageError()),
//             actions: <Widget>[
//               FlatButton(
//                   onPressed: () {
//                     Navigator.of(context).pop();
//                   },
//                   child: Text('Ok'))
//             ],
//           );
//         });
//     loginState.setMessageError = '';
//   }

// }
