import 'dart:io';

import 'package:app/models/usuario_model.dart';
import 'package:app/providers/login_state.dart';
import 'package:app/providers/usuario_provider.dart';
import 'package:app/widgets/customShapeClipper.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegistroPage6 extends StatefulWidget {
  final Usuario data;

  RegistroPage6({
    Key key,
    @required this.data,
  }) : super(key: key);
  @override
  _RegistroPage6State createState() => _RegistroPage6State();
}

class _RegistroPage6State extends State<RegistroPage6> {
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
  }

  File _image;
  final formKey = GlobalKey<FormState>();
  UsuarioProvider usuarioProvider = UsuarioProvider();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: <Widget>[
            _fondo(context),
            _botonAtras(context),
            _registroFormulario(context),
            Container(
              margin: EdgeInsets.only(
                  bottom: size.height * 0.1, left: size.width * 0.2),
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                  width: size.width,
                  child: Text(
                    'Puedes elegir una imagen después',
                    style: TextStyle(fontSize: 16.0),
                  )),
            ),
          ],
        ));
  }

  Widget _fondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: size.height * 0.4,
            color: Color.fromRGBO(0, 140, 80, 0.75),
          ),
        )
      ],
    );
  }

  Widget _botonAtras(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      // alignment: Alignment.topCenter,
      child: IconButton(
          padding: EdgeInsets.only(left: 10.0, top: size.height * 0.07),
          alignment: Alignment.topLeft,
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30.0,
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'registroPage5',
                arguments: widget.data);
          }),
    );
  }

  _registroFormulario(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        // width: size.width*0.6,
        // height: size.height * 0.5,
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SafeArea(
                  child: Container(
                height: size.height * 0.17,
              )),
              Text(
                'Selecciona una imagen',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              SizedBox(
                height: size.height * 0.15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: GestureDetector(
                  child: _widgetDynamic(),
                  onTap: () {
                    _selecionarFoto();
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: RaisedButton(
                  onPressed: () {
                    _submit();
                  },
                  color: Color.fromRGBO(0, 140, 80, 0.75),
                  textColor: Colors.white,
                  child: Text(
                    'Siguiente',
                    style: TextStyle(fontSize: 19),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submit() {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      _enviarFormulario();
      // Navigator.pushNamed(context, 'app', arguments: widget.data);
    }
  }

  Widget _mostrarFoto() {
    return GestureDetector(
      onTap: () {
        _mostrarAlerta();
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Image(
          image: AssetImage(_image.path),
          height: 150,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  _selecionarFoto() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (_image != null) {
      //limpieza

    }
    setState(() {
      _image = image;
    });
  }

  Widget _widgetDynamic() {
    if (_image == null) {
      return IconButton(
        iconSize: 100.0,
        color: Colors.grey[800],
        icon: Icon(Icons.image),
        onPressed: () {
          _selecionarFoto();
          // setState(() {});
        },
      );
    } else {
      return _mostrarFoto();
    }
  }

  void _mostrarAlerta() {
    // final size = MediaQuery.of(context).size;
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext contex) {
          return AlertDialog(
              title: Text('¿Deseas cambiar la imagen?',
                  style: TextStyle(fontSize: 20.0)),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      setState(() {
                        _image = null;
                      });
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Eliminar imagen',
                      style: TextStyle(
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          fontSize: 16.0),
                    )),
                // SizedBox(width: size.height*0.05,),
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'No',
                      style: TextStyle(
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          fontSize: 16.0),
                    )),
                FlatButton(
                    onPressed: () {
                      _selecionarFoto();
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Si',
                      style: TextStyle(
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          fontSize: 16.0),
                    )),
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)));
        });
  }

  Future<void> _enviarFormulario() async {


    prefs = await SharedPreferences.getInstance();


    final size = MediaQuery.of(context).size;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: FutureBuilder(
              future: usuarioProvider.crearCuenta(widget.data, _image),
              builder: (BuildContext context, AsyncSnapshot<Usuario> snapshot) {
                if (snapshot.hasData) {

                  return Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Cuenta creada, ya puedes iniciar sesion',
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        RaisedButton(
                          onPressed: ()  {
                            Navigator.popAndPushNamed(context, 'app');
                          },
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          textColor: Colors.white,
                          child: Text(
                            'Terminar',
                            style: TextStyle(fontSize: 19),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0)),
                        ),
                      ],
                    ),
                  );
                }
                if (snapshot.hasError) {
                  return Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'No se pudo crear la cuenta',
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        RaisedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, 'app');
                          },
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          textColor: Colors.white,
                          child: Text(
                            'Intentar después',
                            style: TextStyle(fontSize: 19),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0)),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Container(
                      height: size.height * 0.6,
                      child: Center(child: CircularProgressIndicator()));
                }
              },
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
          );
        });
  }

  Future<void> mensajeErrorLogin(
      BuildContext context, LoginState loginState) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('Ocurrio un error'),
            content: Text(loginState.existMessageError()),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'))
            ],
          );
        });
    loginState.setMessageError = '';
  }
}
