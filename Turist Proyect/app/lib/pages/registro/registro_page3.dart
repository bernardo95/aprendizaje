import 'package:app/models/usuario_model.dart';
import 'package:flutter/material.dart';
import 'package:app/widgets/customShapeClipper.dart';

class RegistroPage3 extends StatefulWidget {
  final Usuario data;

  RegistroPage3({
    Key key,
    @required this.data,
  }) : super(key: key);
  @override
  _RegistroPage3State createState() => _RegistroPage3State();
}

class _RegistroPage3State extends State<RegistroPage3> {
  int selectedRadioTile;
  @override
  void initState() {
    super.initState();
    selectedRadioTile = 0;
  }

  final formKey = GlobalKey<FormState>();

  setSelectedRadioTile(int value) {
    setState(() {
      selectedRadioTile = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
        body: Stack(
      children: <Widget>[
        _fondo(context),
        _botonAtras(context),
        _registroFormulario(context)
      ],
    ));
  }

  Widget _fondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: size.height * 0.4,
            color: Color.fromRGBO(0, 140, 80, 0.75),
          ),
        )
      ],
    );
  }

  Widget _botonAtras(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      // alignment: Alignment.topCenter,
      child: IconButton(
          padding: EdgeInsets.only(left: 10.0, top: size.height * 0.07),
          alignment: Alignment.topLeft,
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30.0,
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'registroPage2',arguments: widget.data);
          }),
    );
  }

  _registroFormulario(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        // width: size.width*0.6,
        // height: size.height * 0.5,
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SafeArea(
                  child: Container(
                height: size.height * 0.17,
              )),
              Text(
                '¿Cuál es tu genero?',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              SizedBox(
                height: size.height * 0.1,
              ),
              SizedBox(
                  width: size.width * 0.6,
                  child: Column(
                    children: <Widget>[
                      RadioListTile(
                          value: 1,
                          groupValue: selectedRadioTile,
                          title: Text(
                            'Masculino',
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w600),
                          ),
                          activeColor: Color.fromRGBO(0, 140, 80, 0.75),
                          onChanged: (value) {
                            setState(() {
                              selectedRadioTile = value;
                              widget.data.genero = 'Masculino';
                            });
                          }),
                      RadioListTile(
                          value: 2,
                          groupValue: selectedRadioTile,
                          title: Text(
                            'Femenino',
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w600),
                          ),
                          activeColor: Color.fromRGBO(0, 140, 80, 0.75),
                          onChanged: (value) {
                            setState(() {
                              selectedRadioTile = value;
                              widget.data.genero = 'Femenino';
                            });
                          }),
                      RadioListTile(
                          value: 3,
                          groupValue: selectedRadioTile,
                          title: Text(
                            'Otro',
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w600),
                          ),
                          activeColor: Color.fromRGBO(0, 140, 80, 0.75),
                          onChanged: (value) {
                            setState(() {
                              selectedRadioTile = value;
                              
                            });
                          }),
                    ],
                  )),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: RaisedButton(
                  onPressed: () {
                    _submit();
                  },
                  color: Color.fromRGBO(0, 140, 80, 0.75),
                  textColor: Colors.white,
                  child: Text(
                    'Siguiente',
                    style: TextStyle(fontSize: 19),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _submit() {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      // formKey.currentState.save();

      print(widget.data.toMap());
      Navigator.pushNamed(context, 'registroPage4',arguments: widget.data);
    }
  }
}
