import 'package:app/models/usuario_model.dart';
import 'package:app/providers/usuario_provider.dart';
import 'package:app/utils/validators.dart' as utils;
import 'package:app/widgets/customShapeClipper.dart';
import 'package:flutter/material.dart';

class RegistroPage4 extends StatefulWidget {
  final Usuario data;

  RegistroPage4({
    Key key,
    @required this.data,
  }) : super(key: key);
  @override
  _RegistroPage4State createState() => _RegistroPage4State();
}

class _RegistroPage4State extends State<RegistroPage4> {
  UsuarioProvider provider = UsuarioProvider();
  final formKey = GlobalKey<FormState>();
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: <Widget>[
            _fondo(context),
            _botonAtras(context),
            _registroFormulario(context)
          ],
        ));
  }

  Widget _fondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: size.height * 0.4,
            color: Color.fromRGBO(0, 140, 80, 0.75),
          ),
        )
      ],
    );
  }

  Widget _botonAtras(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      // alignment: Alignment.topCenter,
      child: IconButton(
          padding: EdgeInsets.only(left: 10.0, top: size.height * 0.07),
          alignment: Alignment.topLeft,
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30.0,
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'registroPage3',
                arguments: widget.data);
          }),
    );
  }

  _registroFormulario(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        // width: size.width*0.6,
        // height: size.height * 0.5,
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SafeArea(
                  child: Container(
                height: size.height * 0.17,
              )),
              Text(
                'Ingrese un correo',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              SizedBox(
                height: size.height * 0.1,
              ),
              SizedBox(
                width: size.width * 0.5,
                child: TextFormField(
                  controller: _controller,
                  // obscureText: true,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Correo electronico',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {
                    widget.data.email = value;
                  },
                  validator: (value) {
                    if (utils.emailValid(value)) {
                      return null;
                    } else {
                      return 'Ingrese un correo valido';
                    }
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: RaisedButton(
                  onPressed: () {
                    _submit();
                  },
                  color: Color.fromRGBO(0, 140, 80, 0.75),
                  textColor: Colors.white,
                  child: Text(
                    'Siguiente',
                    style: TextStyle(fontSize: 19),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      final resp = await provider.getEmailExist(_controller.text);
      if (resp == true) {
        _mostrarAlerta();
      } else {
        formKey.currentState.save();
        print(widget.data.toMap());
        Navigator.pushNamed(context, 'registroPage5', arguments: widget.data);
      }
    }
  }

  void _mostrarAlerta() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext contex) {
          return AlertDialog(
              title: Text('Upss!', style: TextStyle(fontSize: 20.0)),
              content: Text(
                'El correo ya esta en uso',
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w300),
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Entendido',
                      style: TextStyle(
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          fontSize: 16.0),
                    ))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)));
        });
  }
}
