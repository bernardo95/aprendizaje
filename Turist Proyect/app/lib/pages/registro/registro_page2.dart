import 'package:app/models/usuario_model.dart';
import 'package:flutter/material.dart';
import 'package:app/widgets/customShapeClipper.dart';
import 'package:app/utils/validators.dart' as utils;

class RegistroPage2 extends StatefulWidget {
  final Usuario data;

  RegistroPage2({
    Key key,
    @required this.data,
  }) : super(key: key);

  @override
  _RegistroPage2State createState() => _RegistroPage2State();
}

class _RegistroPage2State extends State<RegistroPage2> {
  final formKey = GlobalKey<FormState>();
  String _fecha = '';
  TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: <Widget>[
            _fondo(context),
            _botonAtras(context),
            _registroFormulario(context)
          ],
        ));
  }

  Widget _fondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: size.height * 0.4,
            color: Color.fromRGBO(0, 140, 80, 0.75),
          ),
        )
      ],
    );
  }

  Widget _botonAtras(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      // alignment: Alignment.topCenter,
      child: IconButton(
          padding: EdgeInsets.only(left: 10.0, top: size.height * 0.07),
          alignment: Alignment.topLeft,
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30.0,
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'registroPage',
                arguments: widget.data);
          }),
    );
  }

  _registroFormulario(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        // width: size.width*0.6,
        // height: size.height * 0.5,
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SafeArea(
                  child: Container(
                height: size.height * 0.17,
              )),
              Text(
                '¿Cuándo es tu cumpleaños?',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              SizedBox(
                height: size.height * 0.1,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: TextFormField(
                  enableInteractiveSelection: false,
                  controller: _controller,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'aaaa/mm/dd',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    selectDate(context);
                  },
                  onSaved: (value) {
                    widget.data.fechaNacimiento = value;
                  },
                  validator: (value) {
                    final date = DateTime.parse(value);
                    if (utils.dateVerify(date)) {
                      return null;
                    } else {
                      return 'fecha no valida';
                    }
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: RaisedButton(
                  onPressed: () {
                    _submit();
                  },
                  color: Color.fromRGBO(0, 140, 80, 0.75),
                  textColor: Colors.white,
                  child: Text(
                    'Siguiente',
                    style: TextStyle(fontSize: 19),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1940),
      lastDate: DateTime(2021),
      locale: Locale('es'),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(),
          child: child,
        );
      },
    );

    if (picked != null) {
      setState(() {
        _fecha = picked.toString().split(' ')[0];
        _controller.text = _fecha;
      });
    }
  }

  void _submit() {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      formKey.currentState.save();

      print(widget.data.toMap());
      Navigator.pushNamed(context, 'registroPage3', arguments: widget.data);
    }
  }
}
