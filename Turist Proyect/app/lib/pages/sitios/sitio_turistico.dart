import 'package:app/models/sitio_model.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SitioTuristicoPage extends StatelessWidget {
  final Sitio sitio;
  SitioTuristicoPage({
    Key key,
    @required this.sitio,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ChangeNotifierProvider(
            create: (_) => _ModelPage(),
            child: _MainScroll(imagenes: sitio.imagenes, sitio: sitio)));
  }
}

class _MainScroll extends StatelessWidget {
  final List imagenes;
  final Sitio sitio;
  const _MainScroll({Key key, this.imagenes, this.sitio}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      shrinkWrap: true,
      // physics: BouncingScrollPhysics(),
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Colors.green,
          title: Text('Turist App'),
          centerTitle: true,
          elevation: 0,
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          _Carousel(
            imagenes: imagenes,
          ),
          _MainView(sitio: sitio)
        ]))
      ],
    );
  }
}

class _Carousel extends StatelessWidget {
  final List imagenes;
  const _Carousel({Key key, this.imagenes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int indexProvider = Provider.of<_ModelPage>(context).index;
    final size = MediaQuery.of(context).size;
    return CarouselSlider(
      options: CarouselOptions(
          height: 200,
          initialPage: 0,
          scrollPhysics: BouncingScrollPhysics(),
          enlargeCenterPage: true,
          onPageChanged: (int index, CarouselPageChangedReason params) {
            indexProvider = index;
          }),
      items: imagenes.map((imgUrl) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
              width: size.width,
              margin: EdgeInsets.only(top: 5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image(
                  image:
                      NetworkImage("http://10.0.2.2:3000/sitio/imagen/$imgUrl"),
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }
}

class _MainView extends StatefulWidget {
  final Sitio sitio;

  const _MainView({Key key, this.sitio}) : super(key: key);
  @override
  __MainViewState createState() => __MainViewState();
}

class __MainViewState extends State<_MainView>
    with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(
      child: Text(
        'Informacion',
      ),
    ),
    Tab(
      child: Text(
        'Ubicacion',
      ),
    ),
    Tab(
      child: Text(
        'Calificacion',
      ),
    )
  ];

  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
    _tabController.addListener(() {
      Provider.of<_ModelPage>(context, listen: false).indexScrollTabBar =
          _tabController.index;
      // print(_tabController.index);
      // print(Provider.of<_ModelPage>(context,listen: false).indexScrollTabBar );
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    int index = Provider.of<_ModelPage>(context).indexScrollTabBar;
    // final size = MediaQuery.of(context).size;
    return Column(children: <Widget>[
      TabBar(
        indicatorColor: Colors.green,
        indicatorWeight: 2,
        labelColor: Colors.black,
        labelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
        unselectedLabelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.normal, fontSize: 13),
        controller: _tabController,
        tabs: myTabs,
      ),
      ListView(
        addAutomaticKeepAlives: true,
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          if (index == 0) _Informacion(sitio: widget.sitio),
          if (index == 1) _Ubicacion(),
          if (index == 2) _Calificacion()
        ],
      )
    ]);
  }
}

class _Informacion extends StatelessWidget {
  final Sitio sitio;
  const _Informacion({Key key, this.sitio}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      // height: size.height,
      // color: Colors.lime,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              sitio.nombre,
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
            Row(
              children: <Widget>[
                Text(
                  '3',
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Colors.green),
                ),
                Icon(
                  Icons.star,
                  color: Colors.green,
                ),
                SizedBox(
                  width: 30,
                ),
                Icon(
                  Icons.place,
                  color: Colors.grey[800],
                ),
                Text(
                  'Ubicacion',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey[800]),
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  'Acerca de este sitio',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
                Spacer(),
                RaisedButton(
                    color: Colors.white,
                    elevation: 0,
                    child: Text(
                      'Ver más',
                      style: TextStyle(fontSize: 20, color: Colors.green),
                    ),
                    onPressed: () {
                      bool extend = Provider.of<_ModelPage>(context,listen: false).extend;
                      if (extend) {
                        Provider.of<_ModelPage>(context,listen: false).extend = false;
                      }else{
                        Provider.of<_ModelPage>(context,listen: false).extend = true;
                      }
                    }),
              ],
            ),
            Consumer<_ModelPage>(
              builder: (BuildContext context, _ModelPage value, Widget child) {
                if (value.extend) {
                  return Text(
                    sitio.descripcion,
                    style: TextStyle(
                        fontSize: 19,
                        fontWeight: FontWeight.normal,
                        color: Colors.grey[800]),
                  );
                }
                return Text(
                sitio.descripcion.substring(0, 170) + ' ...',
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.normal,
                    color: Colors.grey[800]),
              );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _Ubicacion extends StatelessWidget {
  const _Ubicacion({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: size.height,
      color: Colors.purple,
    );
  }
}

class _Calificacion extends StatelessWidget {
  const _Calificacion({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: size.height,
      color: Colors.orange,
    );
  }
}

class _ModelPage with ChangeNotifier {
  int _index = 0;
  int _indexScrollTabBar = 0;
  bool _extend = false;

  int get index => this._index;

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  int get indexScrollTabBar => this._indexScrollTabBar;

  set indexScrollTabBar(int value) {
    _indexScrollTabBar = value;
    notifyListeners();
  }

  bool get extend => this._extend;

  set extend(bool value) {
    this._extend = value;
    notifyListeners();
  }
}
