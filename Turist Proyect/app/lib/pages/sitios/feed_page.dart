import 'package:app/models/sitio_model.dart';
import 'package:app/pages/menu/botton_navigation_menu.dart';
import 'package:app/providers/sitios_provider.dart';
import 'package:app/search/search_delegate.dart';
import 'package:app/widgets/menu_drawer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    final sitioProvider = Provider.of<SitioProvider>(context);
    final size = MediaQuery.of(context).size;
    return Scaffold(
        drawer: MenuWidget(),
        body: ChangeNotifierProvider(
          create: (_) => _MenuModel(),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              (sitioProvider.sitios.length != 0)
                  ? _MainScroll(sitioProvider: sitioProvider)
                  : _Loading(),
              _PositionMenuBoton(size: size)
            ],
          ),
        ));
  }

  @override
  bool get wantKeepAlive => true;
}

class _PositionMenuBoton extends StatelessWidget {
  const _PositionMenuBoton({
    Key key,
    @required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    _MenuModel provider = Provider.of<_MenuModel>(context);
    return Positioned(
        bottom: size.height * 0.04,
        child: MenuBottonPage(
          mostrar: provider.mostrar,
        ));
  }
}

class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.6,
        child: Center(child: CircularProgressIndicator()));
  }
}

class _MainScroll extends StatefulWidget {
  final SitioProvider sitioProvider;
  const _MainScroll({Key key, this.sitioProvider}) : super(key: key);

  @override
  __MainScrollState createState() => __MainScrollState();
}

class __MainScrollState extends State<_MainScroll> {
  final ScrollController _scrollController = ScrollController();
  double _scrollAnterior = 0;
  @override
  void initState() {
    _scrollController.addListener(() => {
          if (_scrollController.offset > _scrollAnterior &&
              _scrollController.offset > 1)
            {
              Provider.of<_MenuModel>(context, listen: false).mostrar = false,
            }
          else
            {
              Provider.of<_MenuModel>(context, listen: false).mostrar = true,
            },
          _scrollAnterior = _scrollController.offset
        });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      // shrinkWrap: true,


      physics: BouncingScrollPhysics(),
      controller: _scrollController,
      slivers: <Widget>[
        SliverAppBar(
          floating: true,
          backgroundColor: Colors.green,
          title: Text('Turist App'),
          centerTitle: true,
          elevation: 0,
        ),
        SliverList(

          delegate: SliverChildListDelegate([
            _ListaSitios(
              sitioProvider: widget.sitioProvider,
            )
          ]),
        )
      ],
    );
  }
}

class _ListaSitios extends StatelessWidget {
  final SitioProvider sitioProvider;

  const _ListaSitios({Key key, this.sitioProvider}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(

      itemCount: sitioProvider.sitios.length,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        return _Card(
            context: context, index: index, listaSitios: sitioProvider.sitios);
      },
    );
  }
}

class _Card extends StatelessWidget {
  final BuildContext context;
  final int index;
  final List<Sitio> listaSitios;

  const _Card({Key key, this.context, this.index, this.listaSitios})
      : super(key: key);
  @override
  @override
  Widget build(BuildContext context) {
    final String url = 'http://10.0.2.2:3000/sitio/imagen';
    final String urlHeroku = 'https://turist-app.herokuapp.com';
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.green[100],
                  blurRadius: 1,
                  spreadRadius: 1,
                  offset: Offset(0, 0))
            ],
            borderRadius: BorderRadius.circular(20)),
        child: Column(
          children: <Widget>[
            GestureDetector(
              child: Container(
                  width: size.width * 0.9,
                  height: size.height * 0.3,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: FadeInImage(
                      placeholder: AssetImage('assets/img/loading.gif'),
                      image: NetworkImage(
                          "$url/${listaSitios[index].imagenes[0]}"),
                      fit: BoxFit.cover,
                    ),
                  )),
              onTap: (){
                Navigator.pushNamed(context, 'sitioTuristico',arguments: listaSitios[index]);
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: size.width * 0.08,
                ),
                Text(
                  listaSitios[index].nombre,
                  style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w500),
                )
              ],
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: size.width * 0.08,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      'ubicacion',
                      style: TextStyle(fontSize: 20),
                    ),
                    Icon(
                      Icons.place,
                      color: Colors.blueGrey,
                    )
                  ],
                ),
                Spacer(),
                Container(
                  width: size.width * 0.2,
                  height: size.height * 0.05,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(20)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      SizedBox(),
                      Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                      Text(
                        '3',
                        style: TextStyle(fontSize: 24.0, color: Colors.white),
                      ),
                      SizedBox(),
                    ],
                  ),
                ),
                SizedBox(
                  width: size.width * 0.08,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class _MenuModel with ChangeNotifier {
  bool _mostrar = true;
  bool get mostrar => this._mostrar;

  set mostrar(bool value) {
    this._mostrar = value;
    notifyListeners();
  }
}
