
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class MenuBotton {
  final IconData icon;
  final Function onPress;

  MenuBotton({@required this.icon, @required this.onPress});
}

class MenuBottonPage extends StatelessWidget {
  final bool mostrar;
  MenuBottonPage({this.mostrar = true});

  final List<MenuBotton> items = [
    MenuBotton(
        icon: Icons.landscape,
        onPress: () {
          print('landsc');
          // return MaterialPageRoute(builder: (_) => FeedPage());
        }),
    MenuBotton(
        icon: FontAwesomeIcons.hotel,
        onPress: () {
          print('hotel');
          // return MaterialPageRoute(builder: (_) => HotelPage());
        }),
    MenuBotton(
        icon: Icons.restaurant_menu,
        onPress: () {
          print('restaurant');
          // return MaterialPageRoute(builder: (_) => RestaurantePage());
        }),
  ];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return ChangeNotifierProvider(
        create: (_) => _MenuModel(),
        child: AnimatedOpacity(
          duration: Duration(milliseconds: 250),
          opacity: (mostrar == true) ? 1 : 0,
          child: _MenuBackground(
            size: size,
            child: _MenuItems(menuItems: items),
          ),
        ));
  }
}

class _MenuBackground extends StatelessWidget {
  final Widget child;
  const _MenuBackground({
    Key key,
    @required this.size,
    @required this.child,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: child,
      width: size.width * 0.6,
      height: size.height * 0.07,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(100.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black26,
                offset: Offset(1, 4),
                blurRadius: 10,
                spreadRadius: -3)
          ]),
    );
  }
}

class _MenuItems extends StatelessWidget {
  final List<MenuBotton> menuItems;
  const _MenuItems({Key key, this.menuItems}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: List.generate(
          menuItems.length, (i) => _MenuButton(i: i, item: menuItems[i])),
    );
  }
}

class _MenuButton extends StatelessWidget {
  final int i;
  final MenuBotton item;
  const _MenuButton({Key key, this.i, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _MenuModel provider = Provider.of<_MenuModel>(context);
    return GestureDetector(
      onTap: () {
        provider.itemSeleccionado = i;
        item.onPress();
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        child: FaIcon(
          item.icon,
          size: (provider.itemSeleccionado == i) ? 35.0 : 25.0,
          color:
              (provider.itemSeleccionado == i) ? Colors.green[700] : Colors.green[200],
        ),
      ),
    );
  }
}

class _MenuModel with ChangeNotifier {
  int _itemSeleccionado = 0;

  int get itemSeleccionado => this._itemSeleccionado;

  set itemSeleccionado(int value) {
    this._itemSeleccionado = value;
    notifyListeners();
  }
}
