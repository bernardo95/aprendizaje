import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private url="http://localhost:3000";
  private headers = new HttpHeaders();
    private adminService: AdminService
  

  constructor(private http:HttpClient) { 
    
  }
  
  _crearSitioTurisitco(sitioTuristico){
    this.headers.set('Content-Type','application/json');
    return this.http.post(`${this.url}/sitios`,sitioTuristico,{headers: this.headers});
  }

  _cargarImagenesSitioTuristico(imagenes,id){
    console.log(imagenes);
    
    return this.http.post(`${this.url}/sitios/upload/${id}`,imagenes);
  }
  _cargarSitios(){
    this.headers.set('Content-Type','application/json');
    return this.http.get(`${this.url}/sitios`,{headers: this.headers});
  }
}
