export class SitioTuristicoModel {
    
    id: String;
    nombreSitio: String;
    descripcionSitio: String;
    horario:Object;
    ubicacion:Object;
    categoria:String;
    facildiadAcceso:Number;
    imagenes:[String];
    constructor(nombreSitio,descripcionSitio,horario,ubicacion,categoria,facildiadAcceso,imagenes){
        this.nombreSitio = nombreSitio;
        this.descripcionSitio = descripcionSitio;
        this.horario = horario;
        this.ubicacion = ubicacion;
        this.categoria = categoria;
        this.facildiadAcceso = facildiadAcceso;
        this.imagenes = imagenes;
    }
}