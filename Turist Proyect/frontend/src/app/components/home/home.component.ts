import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
   sitios:any[]=[];
  constructor(private adminService: AdminService) {
    this.cargarSitios();
   }

  ngOnInit(): void {
  }
  async cargarSitios()  {
    this.adminService._cargarSitios().subscribe(data=>{
      this.sitios = data['sitiosDB'];
      this.sitios.forEach(element => {
        element['photoUrl'] = "http://localhost:3000/sitio/imagen/" + element['imagenes'][0];
      });
      console.log(this.sitios);
    })
    
    
  }

}
