import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { SitioTuristicoModel } from '../../models/sitio.model';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  uploadFiles: Array<File>;
  forma: FormGroup;
  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private adminService: AdminService
  ) {
    this.crearFormulario();
  }

  ngOnInit(): void {}
  get imagenesGet() {
    return this.forma.get('imagenes') as FormArray;
  }

  crearFormulario() {
    this.forma = this.fb.group({
      nombreSitio: ['', Validators.required],
      descripcionSitio: ['', Validators.required],
      horarios: this.fb.group({
        horario1Open: ['', Validators.required],
        horario1Close: ['', Validators.required],
        horario2Open: ['', Validators.required],
        horario2Close: ['', Validators.required]
      }),
      coordenadas: this.fb.group({
        log: ['', Validators.required],
        lat: ['', Validators.required]
      }),
      categoria: ['', Validators.required],
      facilidadAcceso: ['', Validators.required]
      // imagenes: ['',Validators.required]
    });
  }
  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  guardar() {
    if (this.forma.invalid) {
      return;
    }
    const formData = new FormData();
    for (let i = 0; i < this.uploadFiles.length; i++) {
      formData.append('photos', this.uploadFiles[i], this.uploadFiles[i].name);
    }

    console.log(this.forma);
    
    

    // this.adminService._crearSitioTurisitco(this.forma.value).subscribe(resp => {
    //   console.log(resp['sitio_turistico']._id);
    //   const id = resp['sitio_turistico']._id;  
    //   // const imagenes = formData.getAll('photos');    
    //   // console.log(formData.getAll('photos'));
    //   this.adminService._cargarImagenesSitioTuristico(formData, id).subscribe(resp => {
    //     console.log('put peticion', resp);
    //   });
    // });
  }

  onFileChange(event) {
    console.log('FileChange', event.target.files);
    this.uploadFiles = event.target.files;
  }
}
