import { RouterModule, Routes } from '@angular/router';
import { FormularioComponent } from './components/formularioSitio/formulario.component';
import { HomeComponent } from './components/home/home.component';

const APP_ROUTES: Routes = [
    {path: 'home', component: HomeComponent},
    { path: 'formularioSitio', component: FormularioComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES,{useHash:true});