require('./config/config');

const express = require('express');
const mongose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const path = require('path');
const bodyParse = require('body-parser');
const app = express();

//morgan dev cli monitor
app.use(morgan('dev'));

// parse application/x-www-form-urlencoded
app.use(bodyParse.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParse.json());

// cors
app.use(cors());

//Configuración global de rutas
app.use(require('./routes/index_routes'));

//habilitar la carpeta public
app.use(express.static(path.resolve(__dirname, '../public/')));


mongose.connect(process.env.URLDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }, (err) => {
    if (err) throw err;

    console.log('Base de datos Online');

});

app.listen(process.env.PORT, () => {
    console.log('Escuchando puerto', process.env.PORT);

});