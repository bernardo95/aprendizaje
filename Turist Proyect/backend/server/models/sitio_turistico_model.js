const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let categoriasValidas = {
    values: ['SITIO', 'HOTEL', 'RESTAURANTE'],
    message: '{VALUE} no es una categoria valida'
};

let Schema = mongoose.Schema;

let sitioTuristicoScheme = Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario'],
        unique: true
    },
    imagenes: {
        type: [String],
        require: false
    },
    descripcion: {
        type: String,
        require: [true, 'La descripcion es necesaria']
    },
    horario: {
        type: Schema.Types.Mixed,
        require: true
    },
    planes: {
        // type: Schema.Types.ObjectId,
        //     ref: 'Planes'
    },
    // calificacion: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'Calificacion'
    // },
    coordenadas: {
        type: Schema.Types.Mixed,
        require: [true, 'La ubicación es requerida']
    },
    servicios: {
        type: [String],
        require: false
    },
    categoria: {
        type: String,
        enum: categoriasValidas,
        require: true
    },
    // reserva: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'Reserva'
    // },
    facilidad_acceso: {
        type: String,
        require: true
    }

});
sitioTuristicoScheme.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Sitio_turistico', sitioTuristicoScheme);