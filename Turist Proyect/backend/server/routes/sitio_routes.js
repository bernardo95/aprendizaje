const express = require('express');
const _ = require('underscore');
const Sitio = require('../models/sitio_turistico_model');
const { verificaToken, verificaAdmin_Role } = require('../middleware/autentication');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const path = require('path');
const lodash = require('lodash');
const { split } = require('lodash');
const app = express();

//Peticion post de creacion de un sitio turistico
app.post('/sitios', [], (req, res) => {
    let body = req.body;

    let sitio = new Sitio({
        nombre: body.nombreSitio,
        descripcion: body.descripcionSitio,
        horario: body.horarios,
        coordenadas: body.coordenadas,
        servicios: body.servicios,
        categoria: body.categoria,
        facilidad_acceso: body.facilidadAcceso,
    });
    //llamada al metodo para crear el sitio turistico en la DB
    sitio.save((err, sitioDB) => {
        //Verifica si hay error al crear el sitio turistico en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'Error en la creacion del sitio turistico en la DB',
                    err
                }
            });
        }
        //respuesta en caso de salir bien la creacion del sitio turistico y carga de imagenes
        res.status(201).json({
            ok: true,
            sitio_turistico: sitioDB
        });
    });
});

//cargarImagenes
app.post('/sitios/upload/:id', async(req, res) => {
    let id = req.params.id;
    let pathImagen = path.resolve(__dirname, `../../uploads/sitios/${id}`);

    fs.mkdirSync(pathImagen);

    //verifica si hay archivos en el request
    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: 'No file source'
        });
    }
    //loop all files
    let data = [];
    let images = [];
    lodash.forEach(lodash.keysIn(req.files.photos), (key) => {
        let photo = req.files.photos[key];
        //creamos un nombre unico para guardarlo en la DB/uploads compartidos
        let nombreArchivoUnico = `${id}-${new Date().getMilliseconds()}-${photo.name}`;
        //move photo to uploads directory
        photo.mv(`./uploads/sitios/${id}/` + nombreArchivoUnico);

        //push file details
        data.push({
            name: photo.name,
            mimetype: photo.mimetype,
            size: photo.size
        });
        //push name image for sitio argument image
        images.push(nombreArchivoUnico);
    });
    Sitio.findById(id, (err, sitioDB) => {
        if (err) {
            // borrarArchivo(nombreArchivoUnico);   
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'ocurrio un error al subir las imagenes para el sitio asociado',
                    err
                }
            });
        }
        if (!sitioDB) {
            // borrarArchivo(nombreArchivoUnico);
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'sitio no existe'
                }
            });
        }
        sitioDB.imagenes = images;
        sitioDB.save((err, sitioActualizadoImg) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            //return response
            return res.status(200).json({
                status: true,
                message: 'Files are uploaded',
                data: data,
                sitioDB: sitioActualizadoImg
            });
        });
    });
});

app.get('/sitio/imagen/:img', [], async(req, res) => {

    let img = req.params.img;
    let id = img.split('-');
    id = id[0];
    // console.log(id);
    let pathImagen = path.resolve(__dirname, `../../uploads/sitios/${id}/${img}`);
    let pathNoImagen = path.resolve(__dirname, '../assets/no_image.jpg');
    // console.log(pathImagen);

    if (fs.existsSync(pathImagen)) {
        res.sendFile(pathImagen);
    } else {
        res.sendFile(pathNoImagen);
    }

});

function borrarArchivo(nombreImagen) {
    let pathImagen = path.resolve(__dirname, `./server/uploads/sitios/${nombreImagen}`);
    if (fs.existsSync(pathImagen)) {
        fs.unlinkSync(pathImagen);
    }
}

//Peticion get para listar los sitios turisticos
app.get('/sitios', [], (req, res) => {
    Sitio.find({ categoria: 'SITIO' }, 'nombre imagenes descripcion horario ubicacion categoria facilidad_acceso')
        .exec((err, sitiosDB) => {
            //verifica si hay error en la busquedo de los usuarios a la DB
            if (err) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: 'No se pudo buscar los sitios turisticos en la base de datos',
                        err
                    }
                });
            }
            //devuelve el listado de los usuarios en la DB
            Sitio.countDocuments({ categoria: 'SITIO' }, (err, conteo) => {
                res.status(200).json({
                    ok: true,
                    cuantos: conteo,
                    sitiosDB,
                });
            });
        });
});

//Peticion get para mostar un sitio turistico
app.get('/sitio/:id', [], (req, res) => {
    let id = req.params.id;

    //metodo del mongose para encontrar por parametro en la BD
    Sitio.findOne({ _id: id })
        .exec((err, sitioDB) => {
            //verifica si hay un error al buscar en la DB
            if (err) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: 'No se pudo buscar el usuario en la base de datos',
                        err
                    }
                });
            }
            //devuelve el usuario encontrado
            res.status(200).json({
                ok: true,
                sitioDB
            });
        });
});

//Peticion put para actualizar un usuario
app.put('/sitio/:id', [verificaToken], (req, res) => {

    let id = req.params.id;

    let body = _.pick(req.body, ['nombre', 'imagenes', 'descripcion', 'horario', 'ubicacion', 'categoria', 'facilidad_acceso']);

    //metodo de mongose para encontrar uno y actualizar
    Sitio.findOneAndUpdate({ _id: id }, body, { new: true }, (err, sitioDB) => {
        //verifica si hay un error al actualizar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo actualizar el sitio turistico en la base de datos',
                    err
                }
            });
        }

        //devuelve el usuario actualizado
        res.status(200).json({
            ok: true,
            usuario: sitioDB,
            message: 'Sitio turistico actualizado correctamente'
        });

    });
});

//Peticion delete para deshabilitar un usuario
app.delete('/sitio/:id', [verificaToken], (req, res) => {
    let id = req.params.id;

    //metodo de mongose para buscar y actualizar
    Sitio.findOneAndRemove({ _id: id }, (err, sitioDB) => {
        //verifica si hay un error al eliminar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo eliminar el sitio turistico de la base de datos',
                    err
                }
            });
        }
        //verifica si el sitio turistico no existe en la base de datos
        if (!sitioDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Sitio turistico no encontrado'
                }
            });
        }

        //Devuelve el sitio turistico eliminado
        res.status(200).json({
            ok: true,
            usuario: sitioDB,
            message: 'Sitio turistico eliminado correctamente'
        });

    });
});
module.exports = app;