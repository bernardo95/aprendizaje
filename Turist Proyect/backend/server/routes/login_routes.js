const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
// const { OAuth2Client } = require('google-auth-library');
// const client = new OAuth2Client(process.env.CLIENT_ID);
const Usuario = require('../models/usuario_model');
const app = express();

//Peticion post para logear usuario y asignar token
app.post('/login', (req, res) => {

    let body = req.body;

    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {

        //verificacion si existe un error interno de la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'Error interno en al busqueda del usuario en la DB'
                }
            });
        }
        //verifica que el usuario existe en la base de datos
        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario o contraseña incorrectos'
                }
            });
        }
        //Verifica si las contraseñas coinciden
        if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario o contraseña incorrectos'
                }
            });
        }
        let token = jwt.sign({
            usuario: usuarioDB
        }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });

        return res.status(200).json({
            ok: true,
            usuario: usuarioDB,
            message: 'usuario logeado',
            token
        });
    });
});

//metodo de logeo de usuario por google
app.post('/login/google', async(req, res) => {
    let body = req.body;

    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
        if (err) {
            //verifica si el servidor tiene un error al solicitar la busqueda
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'Error interno del servidor al buscar',
                    err
                }
            });
        }

        if (usuarioDB) {
            //si el usuario existe en la base de datos pero no creo la cuenta usando google
            if (usuarioDB.google === false) {
                return res.status(400).json({
                    ok: false,
                    google: false,
                    err: {
                        message: 'Debe de usar su autenticación normal'
                    }
                });
            } else {
                let token = jwt.sign({
                    usuario: usuarioDB
                }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });


                return res.status(200).json({
                    ok: true,
                    logged: true,
                    usuario: usuarioDB,
                    message: 'usuario logeado',
                    token,
                });

            }
        } else {
            // Si el usuario no existe en nuestra base de datos y esta iniciando con google se crea
            let usuario = new Usuario();

            usuario.nombre = body.nombre;
            usuario.apellido = body.apellido;
            usuario.email = body.email;
            usuario.fecha_nacimiento = body.fecha_nacimiento;
            usuario.genero = body.genero;
            usuario.img_profile = body.img_profile;
            usuario.google = true;
            usuario.password = ':)';

            usuario.save((err, usuarioDB) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        error: {
                            message: 'Error interno del servidor al crear el usuario',
                            err
                        }
                    });
                }

                let token = jwt.sign({
                    usuario: usuarioDB
                }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });


                return res.status(200).json({
                    ok: true,
                    google: false,
                    usuario: usuarioDB,
                    message: 'Usuario creado y logeado correctamente',
                    token,
                });
            });
        }
    });
});

//metodo para verificar si el usuario debe ingresar por facebook o ya se autentico por otra red social
app.post('/verify/facebook', async(req, res) => {
    let body = req.body;
    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
        if (err) {
            //verifica si el servidor tiene un error al solicitar la busqueda
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'Error interno del servidor al buscar',
                    err
                }
            });
        }
        if (!usuarioDB) {
            return res.status(200).json({
                ok: true,
                existe: false
            });
        }
        if (usuarioDB.facebook === false) {
            return res.status(200).json({
                ok: true,
                facebook: false,
                message: 'El usuario ya se autentico por otro metodo'
            });
        }
        if (usuarioDB.facebook === true) {
            return res.status(200).json({
                ok: true,
                facebook: true
            });
        }
    });
});

//metodo de logeo de usuario por facebook
app.post('/login/facebook', async(req, res) => {
    let body = req.body;

    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
        if (err) {
            //verifica si el servidor tiene un error al solicitar la busqueda
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'Error interno del servidor al buscar',
                    err
                }
            });
        }

        if (usuarioDB) {
            //si el usuario existe en la base de datos pero no creo la cuenta usando google
            if (usuarioDB.facebook === false) {
                return res.status(400).json({
                    ok: false,
                    facebook: false,
                    err: {
                        message: 'Debe de usar su autenticación normal'
                    }
                });
            } else {
                let token = jwt.sign({
                    usuario: usuarioDB
                }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });


                return res.status(200).json({
                    ok: true,
                    logged: true,
                    usuario: usuarioDB,
                    message: 'usuario logeado',
                    token,
                });

            }
        } else {
            // Si el usuario no existe en nuestra base de datos y esta iniciando con google se crea
            let usuario = new Usuario();

            usuario.nombre = body.nombre;
            usuario.apellido = body.apellido;
            usuario.email = body.email;
            usuario.fecha_nacimiento = body.fecha_nacimiento;
            usuario.genero = body.genero;
            usuario.img_profile = body.img_profile;
            usuario.facebook = true;
            usuario.password = ':)';

            usuario.save((err, usuarioDB) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        error: {
                            message: 'Error interno del servidor al crear el usuario',
                            err
                        }
                    });
                }

                let token = jwt.sign({
                    usuario: usuarioDB
                }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });


                return res.status(200).json({
                    ok: true,
                    facebook: true,
                    usuario: usuarioDB,
                    message: 'Usuario creado y logeado correctamente',
                    token,
                });
            });
        }
    });
});


module.exports = app;