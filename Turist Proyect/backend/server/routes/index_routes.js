const express = require('express');

const app = express();

app.use(require('./usuario_routes'));
app.use(require('./login_routes'));
app.use(require('./sitio_routes'));

module.exports = app;