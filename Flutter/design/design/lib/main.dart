import 'package:design/pages/basico_page.dart';
import 'package:design/pages/botones_page.dart';
import 'package:design/pages/scroll_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle( SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent
));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Design App',
      initialRoute: 'botones',
      routes:{
        'basico' : (BuildContext context)=> BasicoPage(),
        'scroll' : (BuildContext context)=> ScrollPage(),
        'botones' : (BuildContext context)=> BotonesPage(),

      }
    );
  }
}