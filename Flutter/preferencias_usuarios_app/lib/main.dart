import 'package:flutter/material.dart';
import 'package:preferencias_usuarios_app/src/pages/home_page.dart';
import 'package:preferencias_usuarios_app/src/pages/settings_page.dart';
import 'package:preferencias_usuarios_app/src/shared_prefs/preferencias_usuario.dart';
 
void main() async{

  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  
  runApp(MyApp());

} 
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Preferencias App',
      initialRoute: HomePage.routeName,
      routes: {
        HomePage.routeName : (BuildContext context)  => HomePage(),
        SettingsPages.routeName : (BuildContext context)=> SettingsPages(),
      },
    );
  }
}