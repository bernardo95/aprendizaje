import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario.internal();

  factory PreferenciasUsuario(){
    return _instancia;
  }

  SharedPreferences _prefs;

  PreferenciasUsuario.internal();

  initPrefs() async{
   this._prefs = await SharedPreferences.getInstance();

  }
  // bool _colorSegundario;
  // int _genero;
  // String _nombre;

  //GET y SET del genero

  get genero{
    return _prefs.getInt('genero') ?? 1;
  }

  set genero(int value){
    _prefs.setInt('genero', value);
  }

  
}