import 'package:flutter/material.dart';
import 'package:preferencias_usuarios_app/src/shared_prefs/preferencias_usuario.dart';
import 'package:preferencias_usuarios_app/src/widgets/menu_wiget.dart';

class HomePage extends StatelessWidget {

  final prefs = new PreferenciasUsuario();

  static final String routeName = 'home';

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Precefenrias de usuario'),
      ),
      drawer: MenuWidget(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Color segundario'),
          Divider(),
          Text('Genero: ${prefs.genero}'),
          Divider(),
          Text('Nombre del usuario'),
          Divider(),
        ],
      ),
    );
  }

}