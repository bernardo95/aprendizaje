import 'package:flutter/material.dart';
import 'package:preferencias_usuarios_app/src/widgets/menu_wiget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPages extends StatefulWidget {

  static final String routeName = 'settings';

  @override
  _SettingsPagesState createState() => _SettingsPagesState();
}

class _SettingsPagesState extends State<SettingsPages> {
  bool _colorSegundario = true;
  int _genero = 1;
  String _nombre = 'Bernardo';
  TextEditingController _textController;

  @override
  void initState() {
    super.initState();
    cargarPrefs();    
    _textController = new TextEditingController(text: _nombre);
  }

  cargarPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _genero = prefs.getInt('genero');
    setState(() {
      
    });
  }

    _setSelectRadio(int value) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('genero', value);
    _genero = value;
    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ajustes'),
      ),
      drawer: MenuWidget(),
      body: ListView(
        children: <Widget>[
          Container(
            child: Text('Settings',style: TextStyle(fontSize: 35.0,fontWeight: FontWeight.bold ),),
            padding: EdgeInsets.all(5.0),
          ),
          Divider(),
          SwitchListTile(
            value: _colorSegundario,
            title: Text('Color segundario'),
            onChanged: (value){
              _colorSegundario = value;
              setState(() {
                
              });
            },
          ),
          RadioListTile(
            value: 1,
            title: Text('Masculino'),
            groupValue: _genero,
            onChanged: _setSelectRadio(_genero),
          ),
          RadioListTile(
            value: 2,
            title: Text('Femenino'),
            groupValue: _genero,
            onChanged: (value){
              _genero = value;
              setState(() {
                
              });
            },
          ),

          Divider(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                labelText: 'Nombre',
                helperText: 'Nombre de la persona usando el telefono',
              ),
              onChanged: (value){},
            ),
          )
        ],
      ),
    );
  }

 
}