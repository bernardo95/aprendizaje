import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoes_app/src/models/zapato_model.dart';
import 'package:shoes_app/src/pages/zapato_desc_page.dart';

class ZapatoSizePreview extends StatelessWidget {
  final bool fullScreen;

  const ZapatoSizePreview({ this.fullScreen = false});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if (!fullScreen) {

          Navigator.push(context, MaterialPageRoute(builder: (_)=>ZapatoDescPage()));
        }
      
      },
          child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: (fullScreen==false)? 25: 5, 
          vertical: (fullScreen==false)? 5 : 1,
          ),
        child: Container(
          width: double.infinity,
          height: (!fullScreen) ? 380 : 350,
          decoration: BoxDecoration(
            color: Color(0xffFFCF53),
            borderRadius: (!fullScreen) ? BorderRadius.circular(50) : BorderRadius.only(
              bottomLeft: Radius.circular(40),
              bottomRight: Radius.circular(40),
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20)
              ),
          ),
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              //zapato con su sombra
              _ZapatoConSombra(),
              //tallas
              if (!fullScreen) 
              _ZapatosTallas()
            ],
          ),
        ),
      ),
    );
  }
}

class _ZapatoConSombra extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final zapatoModel = Provider.of<ZapatoModel>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child: Stack(
        children: <Widget>[
          Positioned(bottom: 0, right: 0, child: _ZapatoSombra()),
          Image(image: AssetImage(zapatoModel.assets))
        ],
      ),
    );
  }
}

class _ZapatoSombra extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: -0.5,
      child: Container(
        width: 250,
        height: 80,
        decoration: BoxDecoration(
            // color: Colors.grey,
            borderRadius: BorderRadius.circular(100),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Color(0xffEAA14E),
                blurRadius: 40,
              ),
            ]),
        // child: child,
      ),
    );
  }
}

class _ZapatosTallas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _TallaZapatoCaja(
            numero: 7,
          ),
          _TallaZapatoCaja(
            numero: 7.5,
          ),
          _TallaZapatoCaja(
            numero: 8,
          ),
          _TallaZapatoCaja(
            numero: 8.5,
          ),
          _TallaZapatoCaja(
            numero: 9,
          ),
          _TallaZapatoCaja(
            numero: 9.5,
          ),
        ],
      ),
    );
  }
}

class _TallaZapatoCaja extends StatelessWidget {
  final double numero;

  const _TallaZapatoCaja({this.numero});

  @override
  Widget build(BuildContext context) {
  final zapatoModel = Provider.of<ZapatoModel>(context);
    return GestureDetector(
      onTap: (){
        zapatoModel.talla = this.numero;
      },
          child: Container(
        alignment: Alignment.center,
        child: Text(
          '${numero.toString().replaceAll('.0', '')}',
          style: TextStyle(
              fontSize: 20,
              color: (this.numero == zapatoModel.talla) ? Colors.white : Color(0xffF1A23A),
              fontWeight: FontWeight.bold),
        ),
        width: 40,
        height: 40,
        decoration: BoxDecoration(
            color: (this.numero == zapatoModel.talla) ? Color(0xffF1A23A) : Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              if (this.numero == zapatoModel.talla)
               BoxShadow(color: Color(0xffF1A23A),blurRadius: 20,offset: Offset(0, 8))
            ]
        ),
      ),
    );
  }
}
