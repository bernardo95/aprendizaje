import 'package:flutter/material.dart';
import 'package:shoes_app/src/widgets/custom_widgets.dart';


class CarritoBoton extends StatelessWidget {
  final double monto;

  const CarritoBoton({this.monto});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            SizedBox(width: 20,),
            Text('\$$monto',style: TextStyle(fontSize: 28,fontWeight: FontWeight.bold),),
            Spacer(),
            BotonCarrito(texto: 'Add to cart',),
            SizedBox(width: 20,),
          ],
        ),
        width: double.infinity,
        height: 80,
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.1),
          borderRadius: BorderRadius.circular(100)
        ),
      ),
    );
  }
}