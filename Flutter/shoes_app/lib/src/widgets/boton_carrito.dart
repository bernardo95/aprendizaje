import 'package:flutter/material.dart';

class BotonCarrito extends StatelessWidget {
  final String texto;
  final double alto;
  final double ancho;
  final Color color;

  BotonCarrito({@required this.texto, this.alto = 50, this.ancho = 150, this.color= Colors.orange});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: this.ancho,
      height: this.alto,
      child: Text('$texto',style: TextStyle(color: Colors.white),),
      decoration: BoxDecoration(
        color: this.color,
        borderRadius: BorderRadius.circular(100)
      ),
    );
  }
}