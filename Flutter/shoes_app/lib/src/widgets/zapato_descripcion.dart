import 'package:flutter/material.dart';


class ZapatoDescripcion extends StatelessWidget {
final String titulo;
final String descripcion;

  const ZapatoDescripcion({@required this.titulo,@required this.descripcion});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(this.titulo,style: TextStyle(fontSize: 30,fontWeight:FontWeight.w700),),
            SizedBox(height: 20,),
            Text(this.descripcion,style: TextStyle(color: Colors.black54,height: 1.4),),
          ],
        ),
      ),
    );
  }
}