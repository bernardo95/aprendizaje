import 'package:flutter/material.dart';

class ZapatoModel with ChangeNotifier {
  String _assets = 'assets/imgs/azul.png';
  double _talla = 9.0;

  String get assets => this._assets;

  set assets(String value){
    this._assets = value;
    notifyListeners();
  }

  double get talla => this._talla;

  set talla(double value){
    this._talla = value;
    notifyListeners();
  }
}