import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoes_app/src/models/zapato_model.dart';
import 'package:shoes_app/src/pages/zapatos_page.dart';
import 'package:shoes_app/src/widgets/custom_widgets.dart';

class ZapatoDescPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  AnimationController bounceController1 = Provider.of<ControllersAnimation>(context)._bounceController1;
  AnimationController fadeController1 = Provider.of<ControllersAnimation>(context)._fadeController1;

    return Scaffold(
        body: SafeArea(
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Hero(tag: 'zapato-1', child: ZapatoSizePreview(fullScreen: true)),
              Positioned(
                top: 20,
                child: FloatingActionButton(
                  elevation: 0,
                  highlightElevation: 0,
                  backgroundColor: Colors.transparent,
                  child: Icon(
                    Icons.chevron_left,
                    color: Colors.white,
                    size: 40,
                  ),
                  onPressed: () {
                  //  bounceController1.dispose();
                  //  fadeController1.dispose();
                    Navigator.pop(context,
                        MaterialPageRoute(builder: (_) => ZapatoPage()));
                  },
                ),
              )
            ],
          ),
          Expanded(
              child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                ZapatoDescripcion(
                    titulo: 'Nike Air Max 720',
                    descripcion:
                        "The Nike Air Max 720 goes bigger than ever before with Nike's taller Air unit yet, offering more air underfoot for unimaginable, all-day comfort. Has Air Max gone too far? We hope so."),
                _MontoComprar(
                  monto: 180.0,
                ),
                _Colores(),
                _BotonesSelectColorSetting()
              ],
            ),
          ))
        ],
      ),
    ));
  }
}

class _BotonesSelectColorSetting extends StatelessWidget {
  const _BotonesSelectColorSetting({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _BotonSombreado(
            icon: Icon(Icons.star, color: Colors.red, size: 25),
          ),
          _BotonSombreado(
            icon: Icon(Icons.shopping_cart,
                color: Colors.grey.withOpacity(0.5), size: 25),
          ),
          _BotonSombreado(
            icon: Icon(Icons.settings,
                color: Colors.grey.withOpacity(0.5), size: 25),
          ),
        ],
      ),
    );
  }
}

class _BotonSombreado extends StatelessWidget {
  final Icon icon;

  const _BotonSombreado({this.icon});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: this.icon,
      width: 45,
      height: 45,
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
                color: Colors.black26,
                spreadRadius: -5,
                blurRadius: 20,
                offset: Offset(0, 6))
          ]),
    );
  }
}

class _Colores extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: <Widget>[
          Expanded(
              child: Stack(
            children: <Widget>[
              Positioned(
                child: _BotonColor(
                  color: Color(0xff364D56),
                  url: 'assets/imgs/negro.png',
                  index: 4,
                ),
              ),
              Positioned(
                left: 30,
                child: _BotonColor(
                  color: Color(0xff2099F1),
                  index: 3,
                  url: 'assets/imgs/azul.png',

                ),
              ),
              Positioned(
                left: 60,
                child: _BotonColor(
                  color: Color(0xffFFAD29),
                  index: 2,
                  url: 'assets/imgs/amarillo.png',

                ),
              ),
              Positioned(
                left: 90,
                child: _BotonColor(
                  color: Color(0xffC6D642),
                  index: 1,
                  url: 'assets/imgs/verde.png',
                ),
              )
            ],
          )),
          // Spacer(),
          BotonCarrito(
            texto: 'More related items',
            alto: 35,
            color: Color(0xffFFC675),
          )
        ],
      ),
    );
  }
}

class _BotonColor extends StatelessWidget {
  final Color color;
  final int index;
  final String url;
  const _BotonColor({this.color, this.index, this.url});

  @override
  Widget build(BuildContext context) {
    final zapatoModel = Provider.of<ZapatoModel>(context);

    return FadeInLeft(
      delay: Duration(milliseconds: this.index*100),
      duration: Duration(milliseconds: 300),
      child: GestureDetector(
        onTap: (){
          zapatoModel.assets = url;
        },
              child: Container(
          width: 45,
          height: 45,
          decoration: BoxDecoration(color: this.color, shape: BoxShape.circle),
        ),
      ),
    );
  }
}

class _MontoComprar extends StatelessWidget {
  final double monto;

  const _MontoComprar({this.monto});
  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top: 15, bottom: 20),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Text(
            '\$$monto',
            style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
          ),
          Spacer(),
          Bounce(
            delay: Duration(seconds: 1),
            from: 8,
            child: BotonCarrito(
              texto: 'Buy Now',
              ancho: 120,
              alto: 45,
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }
}

class ControllersAnimation with ChangeNotifier {
  AnimationController _bounceController1;
  AnimationController _fadeController1;


  AnimationController get bounceController1 => this._bounceController1;
  AnimationController get fadeController1 => this._fadeController1;

  set bounceController1(AnimationController controller){
    this._bounceController1 = controller;
    notifyListeners();
  }

  set fadeController1(AnimationController controller){
    // this._fadeController1 = controller;
    notifyListeners();
  }

}
