import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoes_app/src/models/zapato_model.dart';
import 'package:shoes_app/src/pages/zapato_desc_page.dart';

import 'src/pages/zapatos_page.dart';
 
void main() => runApp(MultiProvider(
  providers: [
    ChangeNotifierProvider(create: (_)=>ZapatoModel()),
    ChangeNotifierProvider(create: (_)=>ControllersAnimation())
  ],
  child: MyApp()));
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: ZapatoPage()
    );
  }
}