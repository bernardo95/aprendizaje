import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qr_reader_app/src/bloc/scans_bloc.dart';
import 'package:qr_reader_app/src/models/scan_model.dart';
import 'package:qr_reader_app/src/pages/direcciones_page.dart';
import 'package:qr_reader_app/src/pages/mapas_page.dart';
import 'package:qr_reader_app/src/utils/utils.dart' as utils ;
import 'package:qrcode_reader/qrcode_reader.dart';

class HomePage extends StatefulWidget {
  
  
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final scansBloc = new ScansBloc();
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR Scanner'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: (){
              scansBloc.borrarScanTodos();
            },
          )
        ],
      ),
      body: _callPage(currentIndex),
      bottomNavigationBar: _crearBotomNavigatorBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.filter_center_focus),
        onPressed: (){
          _scanQr(context);
        },
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }

 Widget _crearBotomNavigatorBar() {
   return BottomNavigationBar(
    currentIndex: currentIndex,
    onTap: (index){
      setState(() {
       currentIndex = index; 
      });
    },
    items: [
      BottomNavigationBarItem(
        title: Text('Mapas'),
        icon: Icon(Icons.map)
      ),
      BottomNavigationBarItem(
        title: Text('Direcciones'),
        icon: Icon(Icons.call_split)
      ),
    ],
   );
 }

 Widget _callPage(int paginaActual) {
   switch (paginaActual) {
     case 0: return MapasPage();
     case 1: return DireccionesPage();
       
     default:
     return MapasPage();
   }
 }

   void _scanQr(BuildContext context) async{

    String futureString ='';
    try {
     futureString = await QRCodeReader().scan();
    } catch (e) {
      futureString = e.toString();
    }

    if(futureString!=null) {
      final scan = ScanModel(valor: futureString);
      scansBloc.agregarScan(scan);
      if (Platform.isIOS) {
        Future.delayed(Duration(milliseconds: 750),(){
      utils.launchURL(scan,context);
        });
      }else{
      utils.launchURL(scan,context);
      }
    }
  }

}