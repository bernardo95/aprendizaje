
import 'package:flutter/material.dart';



class ContadorPage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ContadorPageState();
  }

}

class _ContadorPageState extends State<ContadorPage>{

  final _estiloTexto = new TextStyle(fontSize: 20);
  int _conteo = 0;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Mi App'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Numero de clicks:',style: _estiloTexto,),
            Text('$_conteo',style: _estiloTexto,),
          ],
        ),
      ),
      floatingActionButton: _crearBotones(),
    );
  }

  Widget _crearBotones(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(width: 30,),
        FloatingActionButton(onPressed: _resetear, child: Icon(Icons.restore),),
        Expanded(child: SizedBox()),
        FloatingActionButton(onPressed: _restar, child: Icon(Icons.remove),),
        SizedBox(width: 5,),
        FloatingActionButton(onPressed: _agregar, child: Icon(Icons.add),),
        SizedBox(width: 5,),
      ],
    );
  }

  void _agregar(){
    _conteo++;
    setState(() {
      
    });
  }
  void _restar(){
    if (_conteo>0) {
      _conteo--;
    }
    setState(() {
      
    });
  }
    void _resetear(){
    _conteo=0;
    setState(() {
      
    });
  }
}