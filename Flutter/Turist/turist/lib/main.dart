
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:turist/src/bloc/provider.dart';
import 'package:turist/src/pages/feed_page.dart';
import 'package:turist/src/pages/formulario_page.dart';
import 'package:turist/src/pages/habitaciones/achiotte_habitacion.dart';
import 'package:turist/src/pages/habitaciones/casa_suite_habitacion.dart';
import 'package:turist/src/pages/habitaciones/chalets_habitacion.dart';
import 'package:turist/src/pages/habitaciones/sams_habitacion.dart';
import 'package:turist/src/pages/habitaciones/serrana_habitacion.dart';
import 'package:turist/src/pages/habitaciones/sonesta_habitacion.dart';
import 'package:turist/src/pages/hotel_page.dart';
import 'package:turist/src/pages/login_page.dart';
import 'package:turist/src/pages/registro.dart';
import 'package:turist/src/pages/restaurante_page.dart';
import 'package:turist/src/pages/settings.dart';
import 'package:turist/src/pages/sitios/barichara.dart';
import 'package:turist/src/pages/sitios/guadalupe.dart';
import 'package:turist/src/pages/sitios/mesa_santos.dart';
import 'package:turist/src/pages/sitios/panachi.dart';
import 'package:turist/src/pages/sitios/sangli.dart';
import 'package:turist/src/pages/sitios/santisimo_page.dart';
import 'package:turist/src/pages/sitios/zapatoca.dart';
import 'package:turist/src/preferencias_usuario/preferencias_usuario.dart';

 
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  runApp(MyApp());

}

 
  class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
  
  final prefs = new PreferenciasUsuario();
    // print(prefs.token);
    
  return Provider(
      child:  MaterialApp(
      debugShowCheckedModeBanner: false,
      // initialRoute: 'feed',
      // onGenerateRoute: (settings){

      // },
      routes: {
        'myapp' : (BuildContext context)=>MyApp(),
        'login' : (BuildContext context)=> LoginPage(),
        'registro' : (BuildContext context)=> RegistroPage(),
        'feed' : (BuildContext context)=> FeedPage(),
        'santisimo' : (BuildContext context)=> SantisimoPage(),
        'sangil' : (BuildContext context)=> SangilPage(),
        'panachi' : (BuildContext context)=> PanachiPage(),
        'guadalupe' : (BuildContext context)=> GuadalupePage(),
        'barichara' : (BuildContext context)=> BaricharaPage(),
        'mesa_santos' : (BuildContext context)=> MesaSantosPage(),
        'zapatoca' : (BuildContext context)=> ZapatocaPage(),
        'settings' : (BuildContext context)=> SettingsPage(),
        'achiotte_habitacion' : (BuildContext context)=> AchiotteHabitacionPage(),
        'chalets_habitacion' : (BuildContext context)=> ChaletsHabitacionPage(),
        'casa_suite_habitacion' : (BuildContext context)=> CasaSuiteHabitacionPage(),
        'serrana_habitacion' : (BuildContext context)=> SerranaHabitacionPage(),
        'sonesta_habitacion' : (BuildContext context)=> SonestaHabitacionPage(),
        'sams_habitacion' : (BuildContext context)=> SamsHabitacionPage(),
        'formulario' : (BuildContext context)=> FormularioPage(),
      },
      theme: ThemeData(
        primaryColor: Colors.green
      ),
      home: MyBottomNavigationBar(),
    ),
  );
   
  }
}

class MyBottomNavigationBar extends StatefulWidget {
  @override
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {

  int _currentIndex = 0;
  final List<Widget> _children = [
    FeedPage(),
    HotelPage(),
    RestaurantePage()
  ];
  void onTappedBar(int index){
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: CurvedNavigationBar(
        onTap: onTappedBar,
        index: _currentIndex,
        // currentIndex: _currentIndex,
        items:[
          Icon(Icons.landscape),
            // title: Text('Sitios')
          
           Icon(Icons.hotel,),
            // title: Text('Holteles')
          
          Icon(Icons.restaurant,),
            // title: Text('Restaurantes')
          
        ],//items
        color: Colors.green,
        buttonBackgroundColor: Colors.white,
        backgroundColor: Colors.white,

        height: 60.0,
        animationDuration: Duration(milliseconds: 300),
        animationCurve: Curves.fastOutSlowIn,
      ),
    );
  }
}