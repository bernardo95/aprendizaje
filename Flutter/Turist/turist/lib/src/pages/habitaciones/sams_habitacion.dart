import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
class SamsHabitacionPage extends StatefulWidget {
  @override
  _SamsHabitacionPageState createState() => _SamsHabitacionPageState();
}

class _SamsHabitacionPageState extends State<SamsHabitacionPage> {
 int _current = 0;

  List<String> images = [
    'assets/sam1.jpg',
    'assets/sam2.jpg',
    'assets/sam3.jpg',
    'assets/sam4.jpg',
    'assets/sam5.jpg',
    'assets/sam6.jpg',
  ];

  List<T> map<T>(List list, Function handle){
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handle(i,list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
  var deviceData = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Sam’s VIP Hostel'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
           child: Column(
            children: <Widget>[
              _carousel(deviceData),
              _punticos(),
            ],
           ),
          ),
          _descripcion(),
          SizedBox(height: 10.0,),
          Container(
            margin: EdgeInsets.only(left: deviceData.width*0.15),
            child: Text('Servicos del establecimiento', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            ),
          _servicosEstablecimiento(),
          // Divider(),
          SizedBox(height: 10.0,),
          Container(

            margin: EdgeInsets.only(left: deviceData.width*0.34),
            child: Text('Habitaciones', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            
          ),
            Divider(height: 5.0,),
          _habitacion1(deviceData),
          Divider(height: 5.0,),
          _habitacion2(deviceData),

          Divider()
        ],
      ),
    );    
  }

  Widget _habitacion1(Size deviceData) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white70,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 1,
          spreadRadius: 1,
          offset: Offset(1,1)
          )
      ]
      ),
      margin: EdgeInsets.only(top: 5.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image(
                  image: AssetImage('assets/sams_vip_hostel_san_gil.jpg'),
                  // height: 180.0,
                  width: deviceData.width*0.5,
                )
              ),
              Container(
                padding: EdgeInsets.only(left: 25.0),
                // alignment: Alignment.center,

                child: Column(
                  children: <Widget>[
                Text('Habitación doble',style: TextStyle(fontSize: 21.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                Text('\$84.900',style: TextStyle(color: Colors.green[600],fontSize: 25.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                  onPressed: (){},
                  child: Text('Seleccionar',style: TextStyle(color: Colors.white),),
                  color: Colors.green,
                )
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 10.0,),
          Container(

            margin: EdgeInsets.only(left: deviceData.width*0.05),
            child: Text('Servicios de habitacion', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            
          ),
          _servicosHabitacion1(),

        ],
      ),
    );
  }
   Widget _habitacion2(Size deviceData) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white70,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 1,
          spreadRadius: 1,
          offset: Offset(1,1)
          )
      ]
      ),
      margin: EdgeInsets.only(top: 5.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image(
                  image: AssetImage('assets/sams_vip_hostel_3_san_gil.jpg'),
                  // height: 180.0,
                  width: deviceData.width*0.5,
                )
              ),
              Container(
                padding: EdgeInsets.only(left: 25.0),
                // alignment: Alignment.center,

                child: Column(
                  children: <Widget>[
                Text('Habitación familiar',style: TextStyle(fontSize: 20.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                Text('\$150.000',style: TextStyle(color: Colors.green[600],fontSize: 25.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                  onPressed: (){},
                  child: Text('Seleccionar',style: TextStyle(color: Colors.white),),
                  color: Colors.green,
                )
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 10.0,),
          Container(

            margin: EdgeInsets.only(left: deviceData.width*0.05),
            child: Text('Servicios de habitacion', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            
          ),
          _servicosHabitacion2(),

        ],
      ),
    );
  }

  Widget _carousel(deviceData) {
    return CarouselSlider(
      height: 200,
      initialPage: 0,
      enlargeCenterPage: true,
      // autoPlay: true,
      onPageChanged: (int index){
        setState(() {
          _current = index;
        });
      },
      items: images.map((imgUrl){
        return Builder(
          builder: (BuildContext context){
            return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image(
                  image: AssetImage(imgUrl),
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  Widget _punticos() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: map<Widget>(
        images,
        (index,url){
          return Container(
            width: 10.0,
            height: 10.0,
            margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 2.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _current == index ? Colors.redAccent : Colors.green,
            ),
          );
        }
      ),
    );
  }

  
}

Widget _servicosEstablecimiento() {
  return Container(

    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Estacionamiento gratis',Icons.directions_car),
            SizedBox(height: 5.0,),
            _iconText('Piscina',Icons.pool),
            SizedBox(height: 5.0,),
            _iconText('Bar/Salón',Icons.local_bar),
            SizedBox(height: 5.0,),
            _iconText('Spa',Icons.spa),
            SizedBox(height: 5.0,),
            _iconText('Gimnasio',Icons.fitness_center),
            SizedBox(height: 5.0,),
            _iconText('Sala de estar',Icons.tv),
            SizedBox(height: 5.0,),
            _iconText('Cocina compartida',Icons.kitchen),
          ],
        ),
        SizedBox(width: 10.0,),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Wifi',Icons.wifi),
            SizedBox(height: 5.0,),
            _iconText('Sauna',Icons.hot_tub),
            SizedBox(height: 5.0,),
            _iconText('Almacenamiento',Icons.card_travel),
            SizedBox(height: 5.0,),
            _iconText('Actividades infantiles',Icons.child_care),
            SizedBox(height: 5.0,),
            _iconText('Restaurante',Icons.restaurant_menu),
            SizedBox(height: 5.0,),
            _iconText('Cambio de divisas',Icons.monetization_on),
            SizedBox(height: 5.0,),
            _iconText('lavanderia',Icons.local_laundry_service),
          ],
        )
      ],
    ),
  );
}

Widget _servicosHabitacion1() {
  return Container(

    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Aire acondicionado',Icons.ac_unit),
            SizedBox(height: 5.0,),
            _iconText('Servicio a la habitación',Icons.directions_run),
            SizedBox(height: 5.0,),
            _iconText('Personas 2',Icons.people),
          ],
        ),
        SizedBox(width: 15.0,),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Caja fuerte',Icons.lock),
            SizedBox(height: 5.0,),
            _iconText('Cocina pequeña',Icons.kitchen),
            SizedBox(height: 33.0,), 
          ],
        )
      ],
    ),
  );
}

Widget _servicosHabitacion2() {
  return Container(

    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Aire acondicionado',Icons.ac_unit),
            SizedBox(height: 5.0,),
            _iconText('Servicio a la habitación',Icons.directions_run),
            SizedBox(height: 5.0,),
            _iconText('Personas 4',Icons.people),
          ],
        ),
        SizedBox(width: 15.0,),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Caja fuerte',Icons.lock),
            SizedBox(height: 5.0,),
            _iconText('Cocina pequeña',Icons.kitchen),
            SizedBox(height: 33.0,), 
          ],
        )
      ],
    ),
  );
}

Widget _iconText(String text, IconData icon) {  
  return Row(
    children: <Widget>[
      Icon(icon,size: 30.0,),
      SizedBox(width: 5.0,),
      Text(text,style: TextStyle(fontSize: 15.0),),
    ],
  );
}

Widget _descripcion() {
  String text = "Está ubicado en el parque principal de San Gil y cerca de los puntos de inicio de actividades extremas. El hotel es fresco, tiene una pequeña piscina con vista a la ciudad y a las montañas. El personal es muy gentil y servicial. Las habitaciones son básicas, el precio es asequible y tiene un balcón amplio al lado de la recepción con una agradable vista. Está incluido un café y frutas al desayuno (Experiencia de Alexandra de Cali).";
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 10.0),
    child: Text(text, style: TextStyle(fontSize: 15,),)
    );
}
