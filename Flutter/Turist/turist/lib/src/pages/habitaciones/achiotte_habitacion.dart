import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class AchiotteHabitacionPage extends StatefulWidget {
  @override
  _AchiotteHabitacionPageState createState() => _AchiotteHabitacionPageState();
}

class _AchiotteHabitacionPageState extends State<AchiotteHabitacionPage> {
  int _current = 0;

  List<String> images = [
    'assets/achiotte1.jpg',
    'assets/achiotte2.jpg',
    'assets/achiotte3.jpg',
    'assets/achiotte4.jpg',
    'assets/achiotte5.jpg'
  ];

  List<T> map<T>(List list, Function handle) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handle(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    var deviceData = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Achiotte Hotel Boutique'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                _carousel(deviceData),
                _punticos(),
              ],
            ),
          ),
          _descripcion(),
          SizedBox(
            height: 10.0,
          ),
          Container(
              margin: EdgeInsets.only(left: deviceData.width * 0.15),
              child: Text(
                'Servicos del establecimiento',
                style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
              )),
          _servicosEstablecimiento(),
          // Divider(),
          SizedBox(
            height: 10.0,
          ),
          Container(
              margin: EdgeInsets.only(left: deviceData.width * 0.34),
              child: Text(
                'Habitaciones',
                style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
              )),
          Divider(
            height: 5.0,
          ),
          _habitacion1(deviceData),
          Divider(
            height: 5.0,
          ),
          _habitacion2(deviceData),
          Divider()
        ],
      ),
    );
  }

  Widget _habitacion1(Size deviceData) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white70,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black26,
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(1, 1))
          ]),
      margin: EdgeInsets.only(top: 5.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image(
                    image: AssetImage('assets/achiotte_hotel_barichara.jpg'),
                    // height: 180.0,
                    width: deviceData.width * 0.5,
                  )),
              Container(
                padding: EdgeInsets.only(left: 25.0),
                // alignment: Alignment.center,

                child: Column(
                  children: <Widget>[
                    Text(
                      'Habitación doble',
                      style: TextStyle(fontSize: 21.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      '\$210.000',
                      style:
                          TextStyle(color: Colors.green[600], fontSize: 25.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      onPressed: () {
                        String hotel = 'Achiotte Hotel Boutique';
                        String habitacion = 'Habitacion doble';
                        double precio = 210000;
                        String imgHabitacion = 'assets/achiotte_hotel_barichara.jpg';
                        Navigator.pushNamed(context, 'formulario',arguments: {
                          hotel,
                          habitacion,
                          precio,
                          imgHabitacion
                        });
                      },
                      child: Text(
                        'Seleccionar',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.green,
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
              margin: EdgeInsets.only(left: deviceData.width * 0.05),
              child: Text(
                'Servicios de habitacion',
                style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
              )),
          _servicosHabitacion1(),
        ],
      ),
    );
  }

  Widget _habitacion2(Size deviceData) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white70,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black26,
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(1, 1))
          ]),
      margin: EdgeInsets.only(top: 5.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image(
                    image: AssetImage('assets/achiotte_hotel_barichara3.jpg'),
                    // height: 180.0,
                    width: deviceData.width * 0.5,
                  )),
              Container(
                padding: EdgeInsets.only(left: 25.0),
                // alignment: Alignment.center,

                child: Column(
                  children: <Widget>[
                    Text(
                      'Habitación familiar',
                      style: TextStyle(fontSize: 20.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      '\$310.000',
                      style:
                          TextStyle(color: Colors.green[600], fontSize: 25.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      onPressed: () {
                        Navigator.pushNamed(context, 'formulario');
                      },
                      child: Text(
                        'Seleccionar',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.green,
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
              margin: EdgeInsets.only(left: deviceData.width * 0.05),
              child: Text(
                'Servicios de habitacion',
                style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
              )),
          _servicosHabitacion2(),
        ],
      ),
    );
  }

  Widget _carousel(deviceData) {
    return CarouselSlider(
      height: 200,
      initialPage: 0,
      enlargeCenterPage: true,
      // autoPlay: true,
      onPageChanged: (int index) {
        setState(() {
          _current = index;
        });
      },
      items: images.map((imgUrl) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image(
                  image: AssetImage(imgUrl),
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  Widget _punticos() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: map<Widget>(images, (index, url) {
        return Container(
          width: 10.0,
          height: 10.0,
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: _current == index ? Colors.redAccent : Colors.green,
          ),
        );
      }),
    );
  }
}

Widget _servicosEstablecimiento() {
  return Container(
    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Estacionamiento gratis', Icons.directions_car),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Piscina', Icons.pool),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Mascotas', Icons.pets),
          ],
        ),
        SizedBox(
          width: 15.0,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Wifi', Icons.wifi),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Desayuno gratis', Icons.fastfood),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Lavanderia', Icons.local_laundry_service),
          ],
        )
      ],
    ),
  );
}

Widget _servicosHabitacion1() {
  return Container(
    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Balcon Privado', Icons.local_hotel),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Servicio de despertador', Icons.alarm),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Refrigerador', Icons.kitchen),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Bañera', Icons.hot_tub),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Personas 2', Icons.people),
          ],
        ),
        SizedBox(
          width: 15.0,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Caja Fuerte', Icons.lock),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Minibar', Icons.local_bar),
            SizedBox(
              height: 5.0,
            ),
            _iconText('TV', Icons.tv),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Aire acondicionado', Icons.ac_unit),
            SizedBox(
              height: 33.0,
            ),
          ],
        )
      ],
    ),
  );
}

Widget _servicosHabitacion2() {
  return Container(
    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Balcon Privado', Icons.local_hotel),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Servicio de despertador', Icons.alarm),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Refrigerador', Icons.kitchen),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Bañera', Icons.hot_tub),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Personas 4', Icons.people),
          ],
        ),
        SizedBox(
          width: 15.0,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Caja Fuerte', Icons.lock),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Minibar', Icons.local_bar),
            SizedBox(
              height: 5.0,
            ),
            _iconText('TV', Icons.tv),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Aire acondicionado', Icons.ac_unit),
            SizedBox(
              height: 5.0,
            ),
            _iconText('Escritorio', Icons.library_books),
          ],
        )
      ],
    ),
  );
}

Widget _iconText(String text, IconData icon) {
  return Row(
    children: <Widget>[
      Icon(
        icon,
        size: 30.0,
      ),
      SizedBox(
        width: 5.0,
      ),
      Text(
        text,
        style: TextStyle(fontSize: 15.0),
      ),
    ],
  );
}

Widget _descripcion() {
  String text =
      "Barichara Achiotte Hotel Boutique, es un lugar de descanso y tranquilidad. Donde nuestros clientes disfrutan de un excelente servicio y de unas instalaciones cómodas, dónde se respira paz y se está en contacto con la naturaleza. Estamos a 2 calles del parque principal, en la zona más tranquila de Barichara y contamos con una vista espectacular a las montañas. Los esperamos para que descubran el Pueblito más bonito de Colombia.";
  return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 15,
        ),
      ));
}

// body: Container(
//       child: PageView.builder(
//         controller: _pageController,
//         itemCount: images.length,
//         itemBuilder: (context,position){
//           return imageSlider(position);
//         }
//       ),
//     )
// Widget imageSlider(int position) {
//   double value = 1;
//   return AnimatedBuilder(
//     animation: _pageController,
//     builder: (ccontext,widget){
//       if (_pageController.position.haveDimensions) {
//         value = _pageController.page - position;
//         value = (1- (value.abs()*0.3)).clamp(0.0, 1.0);
//       }
//       return Container(

//        child: widget,
//       );
//     },
//     child: Container(
//       margin: EdgeInsets.all(5),
//       child: Column(
//         children: <Widget>[
//           ClipRRect(
//             borderRadius: BorderRadius.circular(10),
//             child: Image(
//             image: AssetImage(images[position]),
//             height: Curves.easeInOut.transform(value)*250.0,
//             width: Curves.easeInOut.transform(value)* 380.0,
//             fit: BoxFit.cover,
//           ),
//           ),
//         ],
//       ),
//     ),
//   );
// }
