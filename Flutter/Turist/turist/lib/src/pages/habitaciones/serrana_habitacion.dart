import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
class SerranaHabitacionPage extends StatefulWidget {
  @override
  _SerranaHabitacionPageState createState() => _SerranaHabitacionPageState();
}

class _SerranaHabitacionPageState extends State<SerranaHabitacionPage> {
  int _current = 0;

  List<String> images = [
    'assets/serrana1.jpg',
    'assets/serrana2.jpg',
    'assets/serrana3.jpg',
    'assets/serrana4.jpg',
    'assets/serrana5.jpg'
  ];

  List<T> map<T>(List list, Function handle){
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handle(i,list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
  var deviceData = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Hotel La Serrana Hostal Spa'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
           child: Column(
            children: <Widget>[
              _carousel(deviceData),
              _punticos(),
            ],
           ),
          ),
          _descripcion(),
          SizedBox(height: 10.0,),
          Container(
            margin: EdgeInsets.only(left: deviceData.width*0.15),
            child: Text('Servicos del establecimiento', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            ),
          _servicosEstablecimiento(),
          // Divider(),
          SizedBox(height: 10.0,),
          Container(

            margin: EdgeInsets.only(left: deviceData.width*0.34),
            child: Text('Habitaciones', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            
          ),
            Divider(height: 5.0,),
          _habitacion1(deviceData),
          Divider(height: 5.0,),
          Divider()
        ],
      ),
    );    
  }

  Widget _habitacion1(Size deviceData) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white70,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 1,
          spreadRadius: 1,
          offset: Offset(1,1)
          )
      ]
      ),
      margin: EdgeInsets.only(top: 5.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image(
                  image: AssetImage('assets/la_serrana_hostal_2_las_gachas.jpg'),
                  // height: 180.0,
                  width: deviceData.width*0.5,
                )
              ),
              Container(
                padding: EdgeInsets.only(left: 25.0),
                // alignment: Alignment.center,

                child: Column(
                  children: <Widget>[
                Text('Habitación doble',style: TextStyle(fontSize: 21.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                Text('\$190.000',style: TextStyle(color: Colors.green[600],fontSize: 25.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                  onPressed: (){},
                  child: Text('Seleccionar',style: TextStyle(color: Colors.white),),
                  color: Colors.green,
                )
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 10.0,),
          Container(

            margin: EdgeInsets.only(left: deviceData.width*0.05),
            child: Text('Servicios de habitacion', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            
          ),
          _servicosHabitacion1(),

        ],
      ),
    );
  }

  Widget _carousel(deviceData) {
    return CarouselSlider(
      height: 200,
      initialPage: 0,
      enlargeCenterPage: true,
      // autoPlay: true,
      onPageChanged: (int index){
        setState(() {
          _current = index;
        });
      },
      items: images.map((imgUrl){
        return Builder(
          builder: (BuildContext context){
            return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image(
                  image: AssetImage(imgUrl),
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  Widget _punticos() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: map<Widget>(
        images,
        (index,url){
          return Container(
            width: 10.0,
            height: 10.0,
            margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 2.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _current == index ? Colors.redAccent : Colors.green,
            ),
          );
        }
      ),
    );
  }
}

Widget _servicosEstablecimiento() {
  return Container(

    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Estacionamiento gratis',Icons.directions_car),
            SizedBox(height: 5.0,),
            _iconText('Piscina',Icons.pool),
            SizedBox(height: 5.0,),
            _iconText('Sauna',Icons.hot_tub),
            SizedBox(height: 5.0,),
            _iconText('Transporte aeropuerto',Icons.local_taxi),
            SizedBox(height: 5.0,),
            _iconText('Desayuno gratis',Icons.fastfood),
          ],
        ),
        SizedBox(width: 15.0,),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Wifi',Icons.wifi),
            SizedBox(height: 5.0,),
            _iconText('Bar/Salón',Icons.local_bar),
            SizedBox(height: 5.0,),
            _iconText('Spa',Icons.spa),
            SizedBox(height: 5.0,),
            _iconText('Restaurante',Icons.restaurant),
            SizedBox(height: 33.0,),

          ],
        )
      ],
    ),
  );
}

Widget _servicosHabitacion1() {
  return Container(

    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Bañera/Ducha',Icons.hot_tub),
            SizedBox(height: 5.0,),
            _iconText('Secador pelo',Icons.hot_tub),            
          ],
        ),
        SizedBox(width: 15.0,),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Productos de tocador',Icons.hot_tub),
            SizedBox(height: 5.0,),
            _iconText('Personas 2',Icons.people),
            // SizedBox(height: 33.0,),

          ],
        )
      ],
    ),
  );
}

Widget _iconText(String text, IconData icon) {  
  return Row(
    children: <Widget>[
      Icon(icon,size: 30.0,),
      SizedBox(width: 5.0,),
      Text(text,style: TextStyle(fontSize: 15.0),),
    ],
  );
}

Widget _descripcion() {
  String text = "Pensando en ofrecer un lugar en el que usted pueda descansar, relajarse y pasar un momento de total tranquilidad en medio de la ciudad, es que ponemos a su disposición el mejor sitio para lograr todo lo anterior. La Serrana hostal Spa, ubicado en pleno centro del Socorro (Cra. 13 N. 17-26) cuenta con piscina, sauna, zona de masajes, salas para lectura, cómodas y amplias habitaciones. Servicios que tienen todas las condiciones para que cualquier día de la semana, usted le dé rienda suelta al sosiego y la calma.";
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 10.0),
    child: Text(text, style: TextStyle(fontSize: 15,),)
    );
}