import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
class CasaSuiteHabitacionPage extends StatefulWidget {
  @override
  _CasaSuiteHabitacionPageState createState() => _CasaSuiteHabitacionPageState();
}

class _CasaSuiteHabitacionPageState extends State<CasaSuiteHabitacionPage> {
  int _current = 0;

  List<String> images = [
    'assets/curiti1.jpg',
    'assets/curiti2.jpg',
    'assets/curiti3.jpg',
    'assets/curiti4.jpg',
    'assets/curiti5.jpg'
  ];

  List<T> map<T>(List list, Function handle){
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handle(i,list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
  var deviceData = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Hotel casa suite Curití'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
           child: Column(
            children: <Widget>[
              _carousel(deviceData),
              _punticos(),
            ],
           ),
          ),
          _descripcion(),
          SizedBox(height: 10.0,),
          Container(
            margin: EdgeInsets.only(left: deviceData.width*0.15),
            child: Text('Servicos del establecimiento', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            ),
          _servicosEstablecimiento(),
          // Divider(),
          SizedBox(height: 10.0,),
          Container(

            margin: EdgeInsets.only(left: deviceData.width*0.34),
            child: Text('Habitaciones', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            
          ),
            Divider(height: 5.0,),
          _habitacion1(deviceData),
          Divider(height: 5.0,),
          _habitacion2(deviceData),

          Divider()
        ],
      ),
    );    
  }

  Widget _habitacion1(Size deviceData) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white70,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 1,
          spreadRadius: 1,
          offset: Offset(1,1)
          )
      ]
      ),
      margin: EdgeInsets.only(top: 5.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image(
                  image: AssetImage('assets/hotel_casa_suite_curiti_panachi_2.jpg'),
                  // height: 180.0,
                  width: deviceData.width*0.5,
                )
              ),
              Container(
                padding: EdgeInsets.only(left: 25.0),
                // alignment: Alignment.center,

                child: Column(
                  children: <Widget>[
                Text('Habitación doble',style: TextStyle(fontSize: 21.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                Text('\$115.000',style: TextStyle(color: Colors.green[600],fontSize: 25.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                  onPressed: (){},
                  child: Text('Seleccionar',style: TextStyle(color: Colors.white),),
                  color: Colors.green,
                )
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 10.0,),
          Container(

            margin: EdgeInsets.only(left: deviceData.width*0.05),
            child: Text('Servicios de habitacion', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            
          ),
          _servicosHabitacion1(),

        ],
      ),
    );
  }
   Widget _habitacion2(Size deviceData) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white70,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 1,
          spreadRadius: 1,
          offset: Offset(1,1)
          )
      ]
      ),
      margin: EdgeInsets.only(top: 5.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image(
                  image: AssetImage('assets/hotel_casa_suite_curiti_panachi_3.jpg'),
                  // height: 180.0,
                  width: deviceData.width*0.5,
                )
              ),
              Container(
                padding: EdgeInsets.only(left: 25.0),
                // alignment: Alignment.center,

                child: Column(
                  children: <Widget>[
                Text('Habitación familiar',style: TextStyle(fontSize: 20.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                Text('\$200.000',style: TextStyle(color: Colors.green[600],fontSize: 25.0 ),),
                SizedBox(
                  height: 10.0,
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                  onPressed: (){},
                  child: Text('Seleccionar',style: TextStyle(color: Colors.white),),
                  color: Colors.green,
                )
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 10.0,),
          Container(

            margin: EdgeInsets.only(left: deviceData.width*0.05),
            child: Text('Servicios de habitacion', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),)
            
          ),
          _servicosHabitacion2(),

        ],
      ),
    );
  }

  Widget _carousel(deviceData) {
    return CarouselSlider(
      height: 200,
      initialPage: 0,
      enlargeCenterPage: true,
      // autoPlay: true,
      onPageChanged: (int index){
        setState(() {
          _current = index;
        });
      },
      items: images.map((imgUrl){
        return Builder(
          builder: (BuildContext context){
            return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image(
                  image: AssetImage(imgUrl),
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  Widget _punticos() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: map<Widget>(
        images,
        (index,url){
          return Container(
            width: 10.0,
            height: 10.0,
            margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 2.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _current == index ? Colors.redAccent : Colors.green,
            ),
          );
        }
      ),
    );
  }

  
}

Widget _servicosEstablecimiento() {
  return Container(

    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Estacionamiento afuera',Icons.directions_car),
            SizedBox(height: 5.0,),
            _iconText('Piscina',Icons.pool),
            SizedBox(height: 5.0,),
            _iconText('Bar/Salón',Icons.local_bar),
            SizedBox(height: 5.0,),
            _iconText('Spa',Icons.spa),
            SizedBox(height: 5.0,),
            _iconText('Tienda regalos',Icons.store_mall_directory),
            SizedBox(height: 5.0,),
            _iconText('Sala TV compartida',Icons.tv),
            SizedBox(height: 5.0,),
            _iconText('Sala de reuniones',Icons.business_center),
          ],
        ),
        SizedBox(width: 10.0,),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Wifi',Icons.wifi),
            SizedBox(height: 5.0,),
            _iconText('Saunas',Icons.hot_tub),
            SizedBox(height: 5.0,),
            _iconText('Alquiler bicicleta',Icons.directions_bike),
            SizedBox(height: 5.0,),
            _iconText('Actividades infantiles',Icons.child_care),
            SizedBox(height: 5.0,),
            _iconText('Restaurante',Icons.restaurant_menu),
            SizedBox(height: 5.0,),
            _iconText('Karaoke',Icons.music_note),
            SizedBox(height: 5.0,),
            _iconText('Cambio de divisas',Icons.monetization_on),
          ],
        )
      ],
    ),
  );
}

Widget _servicosHabitacion1() {
  return Container(

    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Servicio de limpieza',Icons.delete),
            SizedBox(height: 5.0,),
            _iconText('Servicio a la habitación',Icons.directions_run),
            
          ],
        ),
        SizedBox(width: 15.0,),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Minibar',Icons.local_bar),
            SizedBox(height: 5.0,),
            _iconText('Personas 2',Icons.people),
            // SizedBox(height: 33.0,), 
          ],
        )
      ],
    ),
  );
}
Widget _servicosHabitacion2() {
  return Container(

    margin: EdgeInsets.only(left: 10.0),
    child: Row(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Servicio de limpieza',Icons.delete),
            SizedBox(height: 5.0,),
            _iconText('Servicio a la habitación',Icons.directions_run),
            
          ],
        ),
        SizedBox(width: 15.0,),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _iconText('Minibar',Icons.local_bar),
            SizedBox(height: 5.0,),
            _iconText('Personas 4',Icons.people),
            // SizedBox(height: 33.0,), 
          ],
        )
      ],
    ),
  );
}
Widget _iconText(String text, IconData icon) {  
  return Row(
    children: <Widget>[
      Icon(icon,size: 30.0,),
      SizedBox(width: 5.0,),
      Text(text,style: TextStyle(fontSize: 15.0),),
    ],
  );
}

Widget _descripcion() {
  String text = "Es un hotel familiar que le ofrece 8 acogedoras habitaciones con capacidad de 3,5 y 7 personas, muy cercano al parque principal y con atención personalizada.";
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 10.0),
    child: Text(text, style: TextStyle(fontSize: 15,),)
    );
}
