import 'package:flutter/material.dart';
import 'package:turist/src/widgets/menu_drawer.dart';

class RestaurantePage extends StatefulWidget {

  @override
  _RestaurantePageState createState() => _RestaurantePageState();
}

class _RestaurantePageState extends State<RestaurantePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Turist'),
        actions: <Widget>[
         IconButton(
           alignment: Alignment.centerRight,
           padding: EdgeInsets.only(right: 15.0),
           icon: Icon(Icons.search),
           iconSize: 35.0,
           onPressed: (){},
         ) 
        ],
      ),
      drawer: MenuWidget(),
      body: Center(
        child: Text('Muy pronto'),
      ),
    );
  }
}

