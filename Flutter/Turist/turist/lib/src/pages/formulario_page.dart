import 'package:flutter/material.dart';
import 'dart:convert';


class FormularioPage extends StatefulWidget {

  @override
  _FormularioPageState createState() => _FormularioPageState();
}

class _FormularioPageState extends State<FormularioPage> {
  @override
  Widget build(BuildContext context) {
  final argumentos = ModalRoute.of(context).settings.arguments;

  String decodeData = argumentos.toString();

  List<String> splitData = decodeData.split(",");

  splitData[0] = splitData[0].split("{")[1];
  splitData[3] = splitData[3].split("}")[0];


  final String hotel = splitData[0];
  final String habitacion = splitData[1];
  final double precio = double.parse(splitData[2]);
  final String imgHabitacion = splitData[3];

  print(splitData);
    return Scaffold(
       appBar: AppBar(
         title: Text('Formulario de reserva'),
       ),
       body: Center(
         child: Text(imgHabitacion),
       ),
    );
  }
}