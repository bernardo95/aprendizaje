import 'package:flutter/material.dart';

class SangilPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppbar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox( height: 10.0 ),
                _calificacionHorario(context),
                _descripcion(),
              ]
            ),
          )
        
        ],
      ),
    );
  }

}

 Widget _crearAppbar() {
    return SliverAppBar(
      // elevation: 2,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text('San Gil',style: TextStyle(color: Colors.black, fontSize: 16.0),
        ),
        background: FadeInImage(
          image: AssetImage('assets/san_gil_2.jpg'),
          placeholder: AssetImage('assets/loading.gif'),
          fadeInDuration: Duration(milliseconds: 15),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

_calificacionHorario(BuildContext context) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: AssetImage('assets/san_gil.jpg'),
                height: 150.0,
                width: 140.0,

              ),
          ),
          SizedBox(width: 10.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Horario',style: Theme.of(context).textTheme.title, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Text('Lunes-Sabado 8am - 5pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                Text('Domingo-Festivos 8am - 6pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Row(
                  children: <Widget>[
                    Text( '4.5', style: Theme.of(context).textTheme.subhead,),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star_half),
                  ],
                )

              ],
            ),
          )
        ],
      ),
  );
}

Widget _descripcion( ) {

  final texto1 = 'Intentar remar dentro de los rápidos del río Fonce; entrar volado y salir nadando de la cueva del indio; saltar con un arnés en los tobillos desde una plataforma de 70 metros con Bungee Jumping o, tambien, andar en bicicleta sobre un cable en equilibrio a unos 160 metros de altura con el Bici Cable. Estos son unos, de los tantos deportes Extremos, que puedes hacer en San Gil: uno de esos sitios turístico de Santander fuera serie. Por esta razón, Si eres amante de la adrenalina y las emociones fuertes este será el lugar para ti';
  final texto2 = 'Ubicado a 2 horas y 30 minutos de Bucaramanga.';
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(texto1,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0),),
          SizedBox(height: 7.0,), 
          Text(texto2,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),
        ],
        )
      ),
    );

  }