import 'package:flutter/material.dart';

class ZapatocaPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppbar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox( height: 10.0 ),
                _calificacionHorario(context),
                _descripcion(),
              ]
            ),
          )
        
        ],
      ),
    );
  }

}

 Widget _crearAppbar() {
    return SliverAppBar(
      // elevation: 2,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text('Zapatoca',style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
        background: FadeInImage(
          image: AssetImage('assets/zapatoca.jpg'),
          placeholder: AssetImage('assets/loading.gif'),
          fadeInDuration: Duration(milliseconds: 15),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

_calificacionHorario(BuildContext context) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: AssetImage('assets/zapatoca_3.jpg'),
                height: 150.0,
                width: 140.0,

              ),
          ),
          SizedBox(width: 10.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Horario',style: Theme.of(context).textTheme.title, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Text('Lunes-Sabado 8am - 5pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                Text('Domingo-Festivos 8am - 6pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Row(
                  children: <Widget>[
                    Text( '4.3', style: Theme.of(context).textTheme.subhead,),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star_half ),
                  ],
                )

              ],
            ),
          )
        ],
      ),
  );
}

Widget _descripcion( ) {

  final texto1 = 'Conocida como la ciudad del clima de seda porque goza de un clima de 20°C. Es un pueblo colonial con gente amable, artesanías guanes y dulces típicos. Donde podrás aventurarte en la Cascada las Lajitas, el Mirador Guane, el Puente de la Concordia, La Cueva del Nitro. Así mismo, la Laguna del Sapo, el Museo Cosmos, la gran panorámica de Pico de la Vieja y Cataratas la Unión. Además, podrás recorrer las veredas de Zapatoca a caballo o en cuatrimotos. Este pueblo es uno de los sitios turísticos para visitar en Santander que te enamorarán.';
  final texto2 = 'Ubicado a 1 hora y 40 minutos de Bucaramanga.';

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(texto1,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0),),
          SizedBox(height: 7.0,), 
          Text(texto2,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),
        ],
        )
      ),
    );

  }