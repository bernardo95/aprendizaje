import 'package:flutter/material.dart';

class PanachiPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppbar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox( height: 10.0 ),
                _calificacionHorario(context),
                _descripcion(),
              ]
            ),
          )
        
        ],
      ),
    );
  }

}

 Widget _crearAppbar() {
    return SliverAppBar(
      // elevation: 2,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text('Parque Nacional del Chicamocha',style: TextStyle(color: Colors.black, fontSize: 16.0),
        ),
        background: FadeInImage(
          image: AssetImage('assets/panachi_2.jpg'),
          placeholder: AssetImage('assets/loading.gif'),
          fadeInDuration: Duration(milliseconds: 15),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

_calificacionHorario(BuildContext context) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: AssetImage('assets/panachi.jpg'),
                height: 150.0,
                width: 140.0,

              ),
          ),
          SizedBox(width: 10.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Horario',style: Theme.of(context).textTheme.title, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Text('Lunes-Sabado 8am - 5pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                Text('Domingo-Festivos 8am - 6pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Row(
                  children: <Widget>[
                    Text( '4.5', style: Theme.of(context).textTheme.subhead,),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star_half),
                  ],
                )

              ],
            ),
          )
        ],
      ),
  );
}

Widget _descripcion( ) {

  final texto1 = 'Ubicado en el corazón del imponente, majestuoso y fascinante Cañon del Chicamocha. El parque Panachi es uno de los destinos turísticos más importantes de Santander, donde se hace honor a la santandereanidad con un hermoso monumento. Es un lugar para visitar donde encontrarás:';
  final texto2 = '•	Plazas, un mirador °360, Avestruces y cabras.';
  final texto3 = '•	Asimismo, para los amantes de la aventura y la adrenalina, opciones como:';
  final texto4 = 'Parapente, Cablevuelo, Columpio extremo, Buggies, Teleférico y un Acuaparque.';
  final texto5 = 'Ubicado a 1 hora y 30 minutos de Bucaramanga.';


    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(texto1,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0),),
          SizedBox(height: 7.0,), 
          Text(texto2,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),
          SizedBox(height: 7.0,), 
          Text(texto3,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),
          SizedBox(height: 7.0,), 
          Text(texto4,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),
          SizedBox(height: 10.0,), 
          Text(texto5,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),
        ],
        )
      ),
    );

  }