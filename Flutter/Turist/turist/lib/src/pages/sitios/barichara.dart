import 'package:flutter/material.dart';

class BaricharaPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppbar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox( height: 10.0 ),
                _calificacionHorario(context),
                _descripcion(),
              ]
            ),
          )
        
        ],
      ),
    );
  }

}

 Widget _crearAppbar() {
    return SliverAppBar(
      // elevation: 2,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text('Barichara',style: TextStyle(color: Colors.black, fontSize: 16.0),
        ),
        background: FadeInImage(
          image: AssetImage('assets/barichara_2.jpg'),
          placeholder: AssetImage('assets/loading.gif'),
          fadeInDuration: Duration(milliseconds: 15),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

_calificacionHorario(BuildContext context) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: AssetImage('assets/barichara.jpg'),
                height: 150.0,
                width: 140.0,

              ),
          ),
          SizedBox(width: 10.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Horario',style: Theme.of(context).textTheme.title, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Text('Lunes-Sabado 8am - 5pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                Text('Domingo-Festivos 8am - 6pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Row(
                  children: <Widget>[
                    Text( '4', style: Theme.of(context).textTheme.subhead,),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                  ],
                )

              ],
            ),
          )
        ],
      ),
  );
}

Widget _descripcion( ) {

  final texto1 = '¿Te gustaría viajar en el tiempo? Imagina recorriendo calles empedradas, a los lados casas con arquitectura colonial esbozando el rastro que los talladores santandereanos dejaron en cada roca tallada. Esa es Barichara, un pueblo que lleva el título de Patrimonio de Colombia y donde podrás encontrar un espacio para descansar y sentir la calma de la naturaleza con su mirador, las iglesias y catedrales llenas de historia. Además, para los amantes de la fotografía, poder capturar esos rincones coloniales de ensueño.';
  final texto2 = '•	Ideas de Planes: Taller de pintura con tierra, Tour Guiado, Pasadía en Bicicleta.';
  final texto3 = 'Se ubica a unas 3 horas de Bucaramanga.';

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(texto1,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0),),
          SizedBox(height: 7.0,), 
          Text(texto2,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),
          SizedBox(height: 7.0,), 
          Text(texto3,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),

        ],
        )
      ),
    );

  }