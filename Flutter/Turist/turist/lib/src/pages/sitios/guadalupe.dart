import 'package:flutter/material.dart';

class GuadalupePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppbar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox( height: 10.0 ),
                _calificacionHorario(context),
                _descripcion(),
              ]
            ),
          )
        
        ],
      ),
    );
  }

}

 Widget _crearAppbar() {
    return SliverAppBar(
      // elevation: 2,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text('Guadalupe: Las Gachas',style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
        background: FadeInImage(
          image: AssetImage('assets/guadalupe_2.jpg'),
          placeholder: AssetImage('assets/loading.gif'),
          fadeInDuration: Duration(milliseconds: 15),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

_calificacionHorario(BuildContext context) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: AssetImage('assets/guadalupe.jpg'),
                height: 150.0,
                width: 140.0,

              ),
          ),
          SizedBox(width: 10.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Horario',style: Theme.of(context).textTheme.title, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Text('Lunes-Sabado 8am - 5pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                Text('Domingo-Festivos 8am - 6pm', style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis ),
                 SizedBox(height: 7.0,),
                Row(
                  children: <Widget>[
                    Text( '4.5', style: Theme.of(context).textTheme.subhead,),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star ),
                    Icon( Icons.star_half),
                  ],
                )

              ],
            ),
          )
        ],
      ),
  );
}

Widget _descripcion( ) {

  final texto1 = 'Esto es uno de los sitios turísticos de Santander que no puedes creer que existan. Las gachas son unos Jacuzzis Naturales, donde podrás sumergirte y relajarte. Además, lo sorprendente es que unos son calientes y al lado hay otros muy fríos. Es un lugar con misterios de la naturaleza y perfecto para explorar una zona de Santander poco conocida.';
  final texto2 = 'Se ubica en Guadalupe Santander a 4 horas de Bucaramanga.';

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(texto1,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0),),
          SizedBox(height: 7.0,), 
          Text(texto2,textAlign: TextAlign.justify,style: TextStyle(fontSize: 15.0,)),
        ],
        )
      ),
    );

  }