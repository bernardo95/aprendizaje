
import 'package:flutter/material.dart';
import 'package:turist/src/widgets/menu_drawer.dart';

class FeedPage extends StatefulWidget {

  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Turist'),
        actions: <Widget>[
         IconButton(
           alignment: Alignment.centerRight,
           padding: EdgeInsets.only(right: 15.0),
           icon: Icon(Icons.search),
           iconSize: 35.0,
           onPressed: (){},
         ) 
        ],
      ),
      drawer: MenuWidget(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
        children: <Widget>[
          Divider(height: 5.0,),
          GestureDetector(
            child: _cardTipo1(context),
            onTap: (){
              Navigator.pushNamed(context, 'santisimo');
            },
          ),
          Divider(height: 15.0,),
           GestureDetector(
            child: _cardTipo2(context),
            onTap: (){
              Navigator.pushNamed(context, 'sangil');
            },
          ),
          Divider(height: 15.0,),
          GestureDetector(
            child: _cardTipo3(context),
            onTap: (){
              Navigator.pushNamed(context, 'panachi');
            },
          ),
          Divider(height: 15.0,),
          GestureDetector(
            child: _cardTipo4(context),
            onTap: (){
              Navigator.pushNamed(context, 'guadalupe');
            },
          ),
          Divider(height: 15.0,),
          GestureDetector(
            child: _cardTipo5(context),
            onTap: (){
              Navigator.pushNamed(context, 'barichara');
            },
          ),
          Divider(height: 15.0,),
          GestureDetector(
            child: _cardTipo6(context),
            onTap: (){
              Navigator.pushNamed(context, 'mesa_santos');
            },
          ),
          Divider(height: 15.0,),
          GestureDetector(
            child: _cardTipo7(context),
            onTap: (){
              Navigator.pushNamed(context, 'zapatoca');
            },
          ),
          Divider(height: 15.0,),

        ],
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   items: <BottomNavigationBarItem>[
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.collections),
      //       title: Text('Sitios')
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.hotel,),
      //       title: Text('Holteles')
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.restaurant,),
      //       title: Text('Restaurantes')
      //     ),
      //   ],
      // ),
    );
  }
}

  _cardTipo1(BuildContext context) { //santisimo
    final card =  Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          ClipRRect (
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage('https://sangiltravel.weebly.com/uploads/2/0/6/4/20648476/san-gil-travel-el-santisimo-14_orig.jpg'),
              placeholder: AssetImage('assets/loading.gif'),
              // fadeInDuration: Duration(microseconds: 200),
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Divider(height: 5.0,),
                Text('Ecoparque cerro del Santísimo',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w700,) ),
                Divider(height: 10.0,)
              ],
            )
            )
        ],
      ),
    );

    return Container(
      
      child: ClipRRect(
        child: card,
        borderRadius: BorderRadius.circular(20),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white60,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 10,
          spreadRadius: 2,
          offset: Offset(2,10)
          )
      ]
      ),
    );
  }
  _cardTipo2(BuildContext context) { //sangil
    final card =  Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          ClipRRect (
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: AssetImage('assets/san_gil_3.jpg'),
              placeholder: AssetImage('assets/loading.gif'),
              // fadeInDuration: Duration(microseconds: 200),
              // height: 250,
              // width: 380,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Divider(height: 5.0,),
                Text('San Gil: capital de los deportes extremos',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w700,) ),
                Divider(height: 10.0,)
              ],
            )
            )
        ],
      ),
    );

    return Container(
      
      child: ClipRRect(
        child: card,
        borderRadius: BorderRadius.circular(20),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white60,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 10,
          spreadRadius: 2,
          offset: Offset(2,10)
          )
      ]
      ),
    );
  }
  _cardTipo3(BuildContext context) { //panachi
    final card =  Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          ClipRRect (
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: AssetImage('assets/panachi_3.jpg'),
              placeholder: AssetImage('assets/loading.gif'),
              // fadeInDuration: Duration(microseconds: 200),
              // height: 250,
              // width: 380,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Divider(height: 5.0,),
                Text('Parque Nacional del Chicamocha',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w700,) ),
                Divider(height: 10.0,)
              ],
            )
            )
        ],
      ),
    );

    return Container(
      
      child: ClipRRect(
        child: card,
        borderRadius: BorderRadius.circular(20),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white60,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 10,
          spreadRadius: 2,
          offset: Offset(2,10)
          )
      ]
      ),
    );
  }
  _cardTipo4(BuildContext context) { //panachi
    final card =  Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          ClipRRect (
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: AssetImage('assets/guadalupe_2.jpg'),
              placeholder: AssetImage('assets/loading.gif'),
              // fadeInDuration: Duration(microseconds: 200),
              // height: 250,
              // width: 380,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Divider(height: 5.0,),
                Text('Guadalupe: Las Gachas: ¡Sí! Este lugar es real.',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w700,) ),
                Divider(height: 10.0,)
              ],
            )
            )
        ],
      ),
    );

    return Container(
      
      child: ClipRRect(
        child: card,
        borderRadius: BorderRadius.circular(20),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white60,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 10,
          spreadRadius: 2,
          offset: Offset(2,10)
          )
      ]
      ),
    );
  }
  _cardTipo5(BuildContext context) { //panachi
    final card =  Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          ClipRRect (
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: AssetImage('assets/barichara_2.jpg'),
              placeholder: AssetImage('assets/loading.gif'),
              // fadeInDuration: Duration(microseconds: 200),
              // height: 250,
              // width: 380,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Divider(height: 5.0,),
                Text('Barichara.',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w700,) ),
                Divider(height: 10.0,)
              ],
            )
            )
        ],
      ),
    );

    return Container(
      
      child: ClipRRect(
        child: card,
        borderRadius: BorderRadius.circular(20),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white60,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 10,
          spreadRadius: 2,
          offset: Offset(2,10)
          )
      ]
      ),
    );
  }
  _cardTipo6(BuildContext context) { //panachi
    final card =  Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          ClipRRect (
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: AssetImage('assets/los_santos_3.jpg'),
              placeholder: AssetImage('assets/loading.gif'),
              // fadeInDuration: Duration(microseconds: 200),
              // height: 250,
              // width: 380,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Divider(height: 5.0,),
                Text('La Mesa de los Santos.',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w700,) ),
                Divider(height: 15.0,)  
              ],
            )
            )
        ],
      ),
    );

    return Container(
      
      child: ClipRRect(
        child: card,
        borderRadius: BorderRadius.circular(20),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white60,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 10,
          spreadRadius: 2,
          offset: Offset(2,10)
          )
      ]
      ),
    );
  }
  _cardTipo7(BuildContext context) { //panachi
    final card =  Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          ClipRRect (
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: AssetImage('assets/zapatoca_2.jpg'),
              placeholder: AssetImage('assets/loading.gif'),
              // fadeInDuration: Duration(microseconds: 200),
              // height: 250,
              // width: 380,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Divider(height: 5.0,),
                Text('Zapatoca.',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w700,) ),
                Divider(height: 15.0,)
              ],
            )
            )
        ],
      ),
    );

    return Container(
      
      child: ClipRRect(
        child: card,
        borderRadius: BorderRadius.circular(20),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white60,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          blurRadius: 10,
          spreadRadius: 2,
          offset: Offset(2,10)
          )
      ]
      ),
    );
  }