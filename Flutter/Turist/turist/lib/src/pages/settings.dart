import 'package:flutter/material.dart';
import 'package:turist/src/widgets/menu_drawer.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _colorSegundario = false;
  int _genero = 1;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Configuración'),
      ),
      drawer: MenuWidget(),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
          ),
          SwitchListTile(
            value: _colorSegundario,
            title: Text('Color segundario'),
            onChanged: (value){
              setState(() {
                _colorSegundario=value;
              });
            },
          ),
          Divider(),
          RadioListTile(
            value: 1,
            title: Text('Masculino'),
            groupValue: _genero,
            onChanged: (value){
              setState(() {
              _genero = value;
              });
            },
          ),
          RadioListTile(
            value: 2,
            title: Text('Femenino'),
            groupValue: _genero,
            onChanged: (value){
              setState(() {
              _genero = value;
              });
            },
          ),

        ],
      ),
    );
  }
}