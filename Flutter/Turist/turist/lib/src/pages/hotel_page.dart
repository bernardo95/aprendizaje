import 'package:flutter/material.dart';
import 'package:turist/src/pages/hoteles/achiotte_hotel.dart';
import 'package:turist/src/pages/hoteles/casa_suite_curiti_hotel.dart';
import 'package:turist/src/pages/hoteles/chalets_suizos.dart';
import 'package:turist/src/pages/hoteles/sams_vip_hotel.dart';
import 'package:turist/src/pages/hoteles/serrana_hostal.dart';
import 'package:turist/src/pages/hoteles/sonesta_hotel.dart';
import 'package:turist/src/widgets/menu_drawer.dart';


class HotelPage extends StatefulWidget {
  
  @override
  _HotelPageState createState() => _HotelPageState();
}

class _HotelPageState extends State<HotelPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Turist'),
        actions: <Widget>[
         IconButton(
           alignment: Alignment.centerRight,
           padding: EdgeInsets.only(right: 15.0),
           icon: Icon(Icons.search),
           iconSize: 35.0,
           onPressed: (){},
         ) 
        ],
      ),
      drawer: MenuWidget(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 5,vertical: 5),
        children: <Widget>[
          Divider(height: 5.0,),
          GestureDetector(
            child: AchiotteHotelPage(),
            onTap: (){
              Navigator.pushNamed(context, 'achiotte_habitacion');
            },
          ),
          Divider(height: 5.0,),
          GestureDetector(
            child: ChaletsSuizosPage(),
            onTap: (){
              Navigator.pushNamed(context, 'chalets_habitacion');
            },
          ),
          Divider(height: 5.0,),
          GestureDetector(
            child: CasaSuiteHotelPage(),
            onTap: (){
              Navigator.pushNamed(context, 'casa_suite_habitacion');
            },
          ),
          Divider(height: 5.0,),
          GestureDetector(
            child: SerranaHostalPage(),
            onTap: (){
              Navigator.pushNamed(context, 'serrana_habitacion');
            },
          ),
          Divider(height: 5.0,),
          GestureDetector(
            child: SonestaHotelPage(),
            onTap: (){
              Navigator.pushNamed(context, 'sonesta_habitacion');
            },
          ),
          Divider(height: 5.0,),
          GestureDetector(
            child: SamsVIPHotelPage(),
            onTap: (){
              Navigator.pushNamed(context, 'sams_habitacion');
            },
          ),
        ],
      )
    );
  }
}


