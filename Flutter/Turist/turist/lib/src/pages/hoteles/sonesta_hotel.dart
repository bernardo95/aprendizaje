import 'package:flutter/material.dart';
class SonestaHotelPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Widget _imagenHotel() {
  return Container(
    child: ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: FadeInImage(
        placeholder: AssetImage('assets/loading.gif'),
        image: AssetImage('assets/sonesta_hotel_santisimo.jpg'),
        fit: BoxFit.fitHeight,
        width: 150.0,
        height: 280.0,
        ),
    ),
  );
}

Widget _infoHotel() {
  return Container(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
       Text('Sonesta hotel',textAlign: TextAlign.start,style: TextStyle(color: Colors.green[700],fontSize: 20.0,fontWeight: FontWeight.bold),),
       Divider(height: 20.0,),
       Row(
            children: <Widget>[
           Text( '5', style: Theme.of(context).textTheme.subhead,textAlign: TextAlign.start,),
           Icon( Icons.star ),
           Icon( Icons.star ),
           Icon( Icons.star ),
           Icon( Icons.star ),
           Icon( Icons.star),
          ],
         ),
       Divider(height: 20.0,),
       Row(
           children: <Widget>[
           Icon(Icons.location_on,color: Colors.blue,),
           Text( '4,3 km del cerro El Santisimo', style: Theme.of(context).textTheme.subhead,textAlign: TextAlign.start,),
           ],
        ),
       Divider(height: 20.0,),
        Row(
          children: <Widget>[
            Text('Dirección: ' , style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
            Text('carrera 27 # 29 – 145',),
          ]
        ),
        Row(
          children: <Widget>[
            Text('Cañaveral oriental Floridablanca')
          ],
        ),
        Row(
          children: <Widget>[
            Text('Área Metropolitana')
          ],
        ),
       Divider(height: 40.0,),
        Row(
          
          children: <Widget>[
            Text('\$209.000',style: TextStyle(color: Colors.green[600],fontSize: 20.0),),
            SizedBox(width: 35.0,),
            RaisedButton(
              textColor: Colors.white,
              onPressed: (){
              Navigator.pushNamed(context, 'sonesta_habitacion');
              },
              child: Text('Ver oferta',style: TextStyle(fontSize: 15.0),),
              color: Colors.green,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              
            ),
          ],
        ),
        // Text('')
      ],
    ),
  );

}
  return Container(
    child: Row(
      children: <Widget>[
        _imagenHotel(),
        SizedBox(width: 10.0,),
        _infoHotel(),
      ],
    ),
  );
  }
}