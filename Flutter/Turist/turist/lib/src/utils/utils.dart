import 'package:flutter/material.dart';


void mostrarAletar(BuildContext context, String mensaje){
showDialog(
  context: context,
  builder: (BuildContext){
    return AlertDialog(
      title: Text('Información incorrecta'),
      content: Text(mensaje),
      actions: <Widget>[
        FlatButton(
          child: Text('Ok'),
          onPressed: (){
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
);
}