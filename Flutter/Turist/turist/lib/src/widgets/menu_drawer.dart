import 'package:flutter/material.dart';
import 'package:turist/src/bloc/provider.dart';

class MenuWidget extends StatefulWidget {

  @override 
  _MenuWidgetState createState() => _MenuWidgetState();
}

class _MenuWidgetState extends State<MenuWidget> {
  @override
  Widget build(BuildContext context) {
  final bloc = Provider.of(context);
  

    return Drawer(
     child: ListView(
       padding: EdgeInsets.zero,
       children: <Widget>[
         DrawerHeader(
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             mainAxisAlignment: MainAxisAlignment.end,
             children: <Widget>[
               CircleAvatar(
                 backgroundImage: _imageValidator(bloc),
                 maxRadius: 35.0,
               ),
               SizedBox(height: 10.0,),
               Container(
                 padding: EdgeInsets.only(bottom: 5.0),
                 child: Text(_emailValidator(bloc),style: TextStyle(fontSize: 16.0,color: Colors.white),),
               ),
             ],
           ),
             decoration: BoxDecoration(
               image: DecorationImage(
                 image: AssetImage('assets/menu-img.jpg'),
                 fit: BoxFit.cover
               )
             ),
         ),
         ListTile(
           leading: Icon(Icons.account_circle,color:Colors.blue),
           title: Text('Cuenta'),
           onTap: (){},
         ),
         ListTile(
           leading: Icon(Icons.home,color:Colors.blue),
           title: Text('Home'),
           onTap: () => Navigator.pushReplacementNamed(context, 'feed'),
         ),
         ListTile(
           leading: Icon(Icons.filter_list,color:Colors.blue),
           title: Text('Categorias'),
           onTap: (){},
         ),
         ListTile(
           leading: Icon(Icons.notifications_active,color:Colors.blue),
           title: Text('Notificaciones'),
           onTap: (){},
         ),
         ListTile(
           leading: Icon(Icons.settings,color:Colors.blue),
           title: Text('Configuracion'),
           onTap: ()=>Navigator.pushReplacementNamed(context, 'settings'),
         ),
         ListTile(
           leading: Icon(Icons.exit_to_app,color:Colors.blue),
           title: Text(_textButtonValidator(bloc)),
           onTap: (){
             bloc.changeEmail('');
             bloc.changePassword('');
             Navigator.pushReplacementNamed(context, 'login');
           },// 
         ),
         
       ],
     ),
   );
  }
}

 _imageValidator(LoginBloc bloc) {
  if (bloc.password==null) {
    return AssetImage('assets/no-profile.jpg');
  } else {
    return AssetImage('assets/perfil.jpg');
  }
}

_emailValidator(LoginBloc bloc){
  if (bloc.email==null) {
    bloc.changeEmail('');
  }
    return bloc.email;
}
_textButtonValidator(LoginBloc bloc){
  var texto = '';
  if (bloc.password==null) {
    texto = 'Ingresar';
  } else {
    texto = 'Salir';
  }

  return texto;
}