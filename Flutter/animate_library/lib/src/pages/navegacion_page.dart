import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class NavegacionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _NotificacionesModel(),
      child: Scaffold(
        appBar: AppBar(
          title: Text('notificaciones'),
          backgroundColor: Colors.pink,
        ),
        floatingActionButton: BotonFlotante(),
        bottomNavigationBar: ButtonNavigation(),
      ),
    );
  }
}

class BotonFlotante extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int provider = Provider.of<_NotificacionesModel>(context).numero;
    AnimationController bounceController = Provider.of<_NotificacionesModel>(context).bounceController;

    return FloatingActionButton(
      child: FaIcon(FontAwesomeIcons.play),
      backgroundColor: Colors.pink,
      onPressed: () {
        provider++;
        Provider.of<_NotificacionesModel>(context, listen: false).numero = provider;
        if (provider >=2) {
          bounceController.forward(from: 0.0);
        }
      },
    );
  }
}

class ButtonNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int provider = Provider.of<_NotificacionesModel>(context).numero;
    return BottomNavigationBar(
      currentIndex: 0,
      selectedItemColor: Colors.pink,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            title: Text('Bones'), icon: FaIcon(FontAwesomeIcons.bone)),
        BottomNavigationBarItem(
            title: Text('Notificaciones'),
            icon: Stack(
              children: <Widget>[
                FaIcon(FontAwesomeIcons.bell),
                Positioned(
                  right: 0,
                  child: BounceInDown(
                    from: 10,
                    animate: (provider > 0) ? true : false,
                    child: Bounce(
                      duration: Duration(milliseconds: 500),
                      controller: (controller) {
                        Provider.of<_NotificacionesModel>(context)
                            .bounceController = controller;
                      },
                      from: 10,
                      child: Container(
                        child: Text(
                          '$provider',
                          style: TextStyle(color: Colors.white, fontSize: 10),
                        ),
                        alignment: Alignment.center,
                        width: 13,
                        height: 13,
                        decoration: BoxDecoration(
                            color: Colors.pink, shape: BoxShape.circle),
                      ),
                    ),
                  ),
                  // child: Icon(Icons.brightness_1,color: Colors.pink,size: 10,)
                )
              ],
            )),
        BottomNavigationBarItem(
            title: Text('Mi perro'), icon: FaIcon(FontAwesomeIcons.dog)),
      ],
    );
  }
}

class _NotificacionesModel with ChangeNotifier {
  int _numero = 0;
  AnimationController _bounceController;

  int get numero => this._numero;

  set numero(int value) {
    this._numero = value;
    notifyListeners();
  }

  AnimationController get bounceController => this._bounceController;

  set bounceController(AnimationController controller) {
    this._bounceController = controller;
    notifyListeners();
  }
}
