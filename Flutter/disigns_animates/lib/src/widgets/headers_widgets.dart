import 'package:flutter/material.dart';

class HeaderCuadrado extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      color: Color(0xff615AAB),
    );
  }
}

class HeaderCircular extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      // color: Color(0xff615AAB),
      decoration: BoxDecoration(
          color: Color(0xff615AAB),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              bottomRight: Radius.circular(50))),
    );
  }
}

class HeaderDiagonal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderDiagonalCustomPainter(),
      ),
    );
  }
}

class _HeaderDiagonalCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    //propiedades
    paint.color = Color(0xff615AAB);
    paint.style = PaintingStyle.fill; //bordes, fill relleno
    paint.strokeWidth = 5;

    final path = Path();

    //dibujar con el path y el pinter
    path.lineTo(0, size.height * 0.5);
    path.lineTo(size.width, size.height * 0.35);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class HeaderTriangular extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        // color: Color(0xff615AAB),
        child: CustomPaint(
          painter: _HeaderTirangularlCustomPainter(),
        ));
  }
}

class _HeaderTirangularlCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    //propiedades
    paint.color = Color(0xff615AAB);
    paint.style = PaintingStyle.fill; //bordes, fill relleno
    paint.strokeWidth = 5;

    final path = Path();

    //dibujar con el path y el pinter
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class HeaderPico extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderPicoCustomPainter(),
      ),
    );
  }
}

class _HeaderPicoCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    //propiedades
    paint.color = Color(0xff615AAB);
    paint.style = PaintingStyle.fill; //bordes, fill relleno
    paint.strokeWidth = 10;

    final path = Path();

    //dibujar con el path y el pinter
    path.lineTo(0, size.height*0.2);
    path.lineTo(size.width*0.5, size.height*0.3);
    path.lineTo(size.width, size.height*0.2);
    path.lineTo(size.width, 0);
    

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
class HeaderCurvo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderCurvoCustomPainter(),
      ),
    );
  }
}

class _HeaderCurvoCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    //propiedades
    paint.color = Color(0xff615AAB);
    paint.style = PaintingStyle.fill; //bordes, fill relleno
    paint.strokeWidth = 8;

    final path = Path();

    //dibujar con el path y el pinter
    path.lineTo(0, size.height*0.25);
    path.quadraticBezierTo(size.width*0.5, size.height*0.35, size.width, size.height*0.25);
    path.lineTo(size.width, 0);
    
    

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
class HeaderWaves extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderWavesCustomPainter(),
      ),
    );
  }
}

class _HeaderWavesCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    //propiedades
    paint.color = Color(0xff615AAB);
    paint.style = PaintingStyle.fill; //bordes, fill relleno
    paint.strokeWidth = 8;

    final path = Path();

    //dibujar con el path y el pinter
    path.lineTo(0, size.height*0.25);
    path.quadraticBezierTo(size.width*0.25, size.height*0.30, size.width*0.5, size.height*0.25);
    path.quadraticBezierTo(size.width*0.75, size.height*0.20, size.width, size.height*0.25);
    path.lineTo(size.width, 0);
    
    

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
