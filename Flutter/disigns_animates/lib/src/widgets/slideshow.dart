import 'package:disigns_animates/src/models/slider_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// import 'package:flutter_svg/svg.dart';

class Slideshow extends StatelessWidget {
  final List<Widget> slides;
  final bool posicion;
  final Color colorPrimario;
  final Color colorSegundario;
  final double bulletPrimario;
  final double bulletSecundario;

  const Slideshow(
      {@required this.slides,
      this.posicion = false,
      this.colorPrimario = Colors.blue,
      this.colorSegundario = Colors.grey,
      this.bulletPrimario = 12.0,
      this.bulletSecundario = 12.0});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => SliderModel(),
        child: SafeArea(
          child: Center(child: Builder(
            builder: (BuildContext context) {
              Provider.of<SliderModel>(context).colorPrimario =
                  this.colorPrimario;
              Provider.of<SliderModel>(context).colorSecundario =
                  this.colorSegundario;
              Provider.of<SliderModel>(context).bulletPrimario =
                  this.bulletPrimario;
              Provider.of<SliderModel>(context).bulletSecundario =
                  this.bulletSecundario;
              return _CrearEstructuraSlideShow(
                  posicion: posicion, slides: slides);
            },
          )),
        ));
  }
}

class _CrearEstructuraSlideShow extends StatelessWidget {
  const _CrearEstructuraSlideShow({
    Key key,
    @required this.posicion,
    @required this.slides,
  }) : super(key: key);

  final bool posicion;
  final List<Widget> slides;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      if (posicion) _Dots(this.slides.length),
      Expanded(child: _Slides(this.slides)),
      if (!posicion)
        _Dots(this.slides.length),

      // _Dots(this.slides.length),
    ]);
  }
}

class _Slides extends StatefulWidget {
  final List<Widget> slides;

  const _Slides(this.slides);

  @override
  __SlidesState createState() => __SlidesState();
}

class __SlidesState extends State<_Slides> {
  final PageController _pageViewController = PageController();

  @override
  void initState() {
    _pageViewController.addListener(() {
      // print('pagina actual: ${_pageViewController.page}');
      Provider.of<SliderModel>(context, listen: false).currentPage =
          _pageViewController.page;
    });
    super.initState();
  }

  @override
  void dispose() {
    _pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PageView(
        controller: _pageViewController,
        children: widget.slides.map((slide) => _Slide(slide)).toList(),
      ),
    );
  }
}

class _Slide extends StatelessWidget {
  final Widget slide;

  _Slide(this.slide);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.all(20),
      child: slide,
    );
  }
}

class _Dots extends StatelessWidget {
  final int length;

  const _Dots(this.length);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      // color: Colors.red,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(length, (i) => _Dot(i))),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;
  const _Dot(this.index);

  @override
  Widget build(BuildContext context) {
    final pageViewIndex = Provider.of<SliderModel>(context).currentPage;
    final colorPrimario = Provider.of<SliderModel>(context).colorPrimario;
    final colorSecundario = Provider.of<SliderModel>(context).colorSecundario;
    final bulletPrimario = Provider.of<SliderModel>(context).bulletPrimario;
    final bulletSecundario = Provider.of<SliderModel>(context).bulletSecundario;

    final size = (pageViewIndex >= index - 0.5 && pageViewIndex < index + 0.5) ? bulletPrimario : bulletSecundario;

    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      margin: EdgeInsets.symmetric(horizontal: 5),
      width: size,
      height: size,
      decoration: BoxDecoration(
          color: (pageViewIndex >= index - 0.5 && pageViewIndex < index + 0.5)
              ? colorPrimario
              : colorSecundario,
          shape: BoxShape.circle),
    );
  }
}
