import 'dart:math';

import 'package:flutter/material.dart';

class RadialProgress extends StatefulWidget {
  final porcentaje;
  final Color color;

  const RadialProgress({@required this.porcentaje, this.color = Colors.blue});
  @override
  _RadialProgressState createState() => _RadialProgressState();
}

class _RadialProgressState extends State<RadialProgress>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  double porcentajeAnterior;
  @override
  void initState() {
    porcentajeAnterior = widget.porcentaje;
    _controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000));
    _controller.addListener(() {});
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _controller.forward(from: 0.0);
    final diferenciaAnimar = widget.porcentaje - porcentajeAnterior;
    porcentajeAnterior = widget.porcentaje;
    return AnimatedBuilder(
      animation: _controller,
      // child: ,
      builder: (BuildContext context, Widget child) {
        return Container(
          padding: EdgeInsets.all(10),
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: _RadialProgress(
                (widget.porcentaje - diferenciaAnimar) +
                    (diferenciaAnimar * _controller.value),
                widget.color),
          ),
        );
      },
    );
    // return Container(
    //   padding: EdgeInsets.all(10),
    //   width: double.infinity,
    //   height: double.infinity,
    //   child: CustomPaint(
    //     painter: _RadialProgress(widget.porcentaje),
    //   ),
    // );
  }
}

class _RadialProgress extends CustomPainter {
  final porcentaje;
  final color;

  _RadialProgress(this.porcentaje, this.color);
  @override
  void paint(Canvas canvas, Size size) {
    //circulo
    final paint = Paint()
      ..strokeWidth = 4
      ..color = Colors.grey
      ..style = PaintingStyle.stroke;

    final center = Offset(size.width * 0.5, size.height * 0.5);
    final radius = min(size.width * 0.5, size.height * 0.5);
    canvas.drawCircle(center, radius, paint);

    //arco
    final arco = Paint()
      ..strokeWidth = 10
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    //parte que va llenando el arco
    double arcAngle = 2 * pi * (porcentaje / 100);
    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, arco);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
