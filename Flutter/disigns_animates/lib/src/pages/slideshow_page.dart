import 'package:disigns_animates/src/widgets/slideshow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class SlideShowPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.purple,
      body: Column(
        children: <Widget>[
          Expanded(child: MiSlideShow()),
          Expanded(child: MiSlideShow()),
        ],
      )
    );
  }
}

class MiSlideShow extends StatelessWidget {
  const MiSlideShow({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Slideshow(
      bulletPrimario: 14,
      bulletSecundario: 10,
      posicion: false,
      colorPrimario: Colors.pink,
      colorSegundario: Colors.grey,
      slides: <Widget>[
      SvgPicture.asset('assets/svg/slide-1.svg'),
      SvgPicture.asset('assets/svg/slide-2.svg'),
      SvgPicture.asset('assets/svg/slide-3.svg'),
      SvgPicture.asset('assets/svg/slide-4.svg'),
      SvgPicture.asset('assets/svg/slide-5.svg'),

    ],);
  }
}