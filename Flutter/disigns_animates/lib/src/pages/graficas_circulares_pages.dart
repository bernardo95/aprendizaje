import 'package:disigns_animates/src/widgets/radial_progress.dart';
import 'package:flutter/material.dart';


class GraficasCircularesPage extends StatefulWidget {

  @override
  _GraficasCircularesPageState createState() => _GraficasCircularesPageState();
}

class _GraficasCircularesPageState extends State<GraficasCircularesPage> {
  double porcentaje=0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),

        onPressed: (){
          setState(() {
            porcentaje+=10;
          });
          if (porcentaje>=100) {
            porcentaje = 0;
          }
        },
      ),
      body: Center(
        child: Container(
          width: 200,
          height: 200,
          // color: Colors.cyan,
          child: RadialProgress(porcentaje: porcentaje,color: Colors.purple,),
          ),
        // child: Text('$porcentaje %',style: TextStyle(fontSize: 50.0),),
        // child: Circular,
     ),
   );
  }
}