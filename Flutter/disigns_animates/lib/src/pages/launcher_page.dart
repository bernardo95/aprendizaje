import 'package:disigns_animates/src/theme/theme_changer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:disigns_animates/src/routes/routes.dart';
import 'package:provider/provider.dart';

class LauncherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Diseños en flutter'),
        ),
        drawer: _MenuPrincipal(),
        body: _ListaOpciones());
  }
}

class _ListaOpciones extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  final provider = Provider.of<ThemeChanger>(context);

    return ListView.separated(
      physics: BouncingScrollPhysics(),
      itemBuilder: (BuildContext context, int index) => ListTile(
        leading: FaIcon(
          pageRoutes[index].icon,
          color: provider.currentTheme.accentColor,
        ),
        title: Text(pageRoutes[index].titulo),
        trailing: Icon(
          Icons.chevron_right,
          color: provider.currentTheme.accentColor,
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (_) => pageRoutes[index].page));
        },
      ),
      itemCount: pageRoutes.length,
      separatorBuilder: (BuildContext context, int index) => Divider(
        color: provider.currentTheme.primaryColorLight,
      ),
    );
  }
}

class $pageRoutes {}

class _MenuPrincipal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  final theme = Provider.of<ThemeChanger>(context);
    return Drawer(
      child: SafeArea(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                width: double.infinity,
                height: 200,
                child: CircleAvatar(
                  backgroundColor: theme.currentTheme.accentColor,
                  child: Text(
                    'BV',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
              ),
              Expanded(child: _ListaOpciones()),
              ListTile(
                leading: Icon(
                  Icons.lightbulb_outline,
                  color: theme.currentTheme.accentColor,
                ),
                title: Text('Dark Mode'),
                trailing: Switch.adaptive(
                  onChanged: (bool value) {
                    theme.darkTheme = value;
                  },
                  value: theme.darkTheme,
                ),
              ),
              SafeArea(
                child: ListTile(
                  leading: Icon(
                    Icons.add_to_home_screen,
                    color: theme.currentTheme.accentColor,
                  ),
                  title: Text('Custom Theme'),
                  trailing: Switch.adaptive(
                    onChanged: (bool value) {
                    theme.customTheme = value;
                    },
                    value: theme.customTheme,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
