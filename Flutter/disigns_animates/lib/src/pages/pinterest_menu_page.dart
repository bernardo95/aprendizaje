import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PinterestButton {
  final Function onPressed;
  final IconData icon;

  PinterestButton({@required this.onPressed, @required this.icon});
}

class PinteresMenu extends StatelessWidget {
  final bool visible;
  final Color backGroundColor;
  final Color activeColor;
  final Color inactiveColor;
  final List<PinterestButton> items;

  PinteresMenu(
      {Key key,
      this.visible = true,
      this.backGroundColor = Colors.white,
      this.activeColor = Colors.black,
      this.inactiveColor = Colors.blueGrey,
      @required this.items
      });

  // final List<PinterestButton> items = [
  //   PinterestButton(
  //       icon: Icons.pie_chart,
  //       onPressed: () {
  //         print('pie_chart');
  //       }),
  //   PinterestButton(
  //       icon: Icons.search,
  //       onPressed: () {
  //         print('search');
  //       }),
  //   PinterestButton(
  //       icon: Icons.notifications,
  //       onPressed: () {
  //         print('notifications');
  //       }),
  //   PinterestButton(
  //       icon: Icons.supervised_user_circle,
  //       onPressed: () {
  //         print('supervised_user_circle');
  //       }),
  // ];

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => _MenuModel(),
        child: AnimatedOpacity(
          opacity: (visible == true) ? 1 : 0,
          duration: Duration(milliseconds: 200),
          child: Builder(
            builder: (BuildContext context) {
              Provider.of<_MenuModel>(context)._backGroundColor = this.backGroundColor;
              Provider.of<_MenuModel>(context)._activeColor = this.activeColor;
              Provider.of<_MenuModel>(context)._inactiveColor = this.inactiveColor;
              return _PinterestMenuBackground(
                child: _MenuItems(menuItems: items),
              );
            },
          ),
        ));
  }
}

class _PinterestMenuBackground extends StatelessWidget {
  final Widget child;
  const _PinterestMenuBackground({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<_MenuModel>(context);
    return Container(
      child: child,
      width: 250,
      height: 60,
      decoration: BoxDecoration(
          color: provider.backGroundColor,
          borderRadius: BorderRadius.circular(1000),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                // offset: Offset(0, 2),
                blurRadius: 10,
                spreadRadius: -5)
          ]),
    );
  }
}

class _MenuItems extends StatelessWidget {
  final List<PinterestButton> menuItems;

  const _MenuItems({Key key, this.menuItems}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children:
            List.generate(menuItems.length, (i) => _ItemMenu(i, menuItems[i])));
  }
}

class _ItemMenu extends StatelessWidget {
  final int index;
  final PinterestButton item;
  const _ItemMenu(this.index, this.item);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<_MenuModel>(context);
    final itemSelected = Provider.of<_MenuModel>(context).itemSelected;
    return GestureDetector(
      onTap: () {
        Provider.of<_MenuModel>(context, listen: false).itemSelected = index;
        item.onPressed();
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        child: Icon(
          item.icon,
          size: (itemSelected == index) ? 32.0 : 25.0,
          color: (itemSelected == index) ? provider.activeColor : provider.inactiveColor,
        ),
      ),
    );
  }
}

class _MenuModel with ChangeNotifier {
  int _itemSelected = 0;
  Color _backGroundColor = Colors.white;
  Color _activeColor = Colors.black;
  Color _inactiveColor = Colors.blueGrey;

  int get itemSelected => this._itemSelected;

  set itemSelected(int valor) {
    this._itemSelected = valor;
    notifyListeners();
  }

  Color get backGroundColor => this._backGroundColor;
  set backGroundColor(Color value) {
    this._backGroundColor = value;
    notifyListeners();
  }

  Color get inactiveColor => this._inactiveColor;
  set inactiveColor(Color value) {
    this._inactiveColor = value;
    notifyListeners();
  }

  Color get activeColor => this._activeColor;
  set activeColor(Color value) {
    this._activeColor = value;
    notifyListeners();
  }
}
