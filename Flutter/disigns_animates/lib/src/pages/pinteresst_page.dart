import 'package:disigns_animates/src/pages/pinterest_menu_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class PinterestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            _PinteresGrid(),
            _PinteresMenuPosition(size: size)
          ],
        ),
      ),
    );
  }
}

class _PinteresMenuPosition extends StatelessWidget {
  const _PinteresMenuPosition({
    Key key,
    @required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    final visible = Provider.of<_MenuModel>(context).visible;

    return Positioned(
        bottom: 30.0,
        // left: size.width*0.15,
        child: Container(
            width: size.width,
            child: Align(
                child: PinteresMenu(
              visible: visible,
              // backGroundColor: Colors.amber,
              // activeColor: Colors.red,
              // inactiveColor: Colors.blueGrey,
              items: [
                PinterestButton(
                    icon: Icons.pie_chart,
                    onPressed: () {
                      print('pie_chart');
                    }),
                PinterestButton(
                    icon: Icons.search,
                    onPressed: () {
                      print('search');
                    }),
                PinterestButton(
                    icon: Icons.notifications,
                    onPressed: () {
                      print('notifications');
                    }),
                PinterestButton(
                    icon: Icons.supervised_user_circle,
                    onPressed: () {
                      print('supervised_user_circle');
                    }),
              ],
            ))));
  }
}

class _PinteresGrid extends StatefulWidget {
  @override
  __PinteresGridState createState() => __PinteresGridState();
}

class __PinteresGridState extends State<_PinteresGrid> {
  final ScrollController _controller = ScrollController();
  double _scrollValueBefore = 0;
  @override
  void initState() {
    _controller.addListener(() => {
          if (_controller.offset > _scrollValueBefore &&
              _controller.offset > 150)
            {Provider.of<_MenuModel>(context, listen: false).visible = false}
          else
            {Provider.of<_MenuModel>(context, listen: false).visible = true},
          _scrollValueBefore = _controller.offset
        });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  final List<int> items = List.generate(200, (i) => i);

  @override
  Widget build(BuildContext context) {
    return new StaggeredGridView.countBuilder(
      controller: _controller,
      crossAxisCount: 4,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) => _PinterestItem(
        index: index,
      ),
      staggeredTileBuilder: (int index) =>
          new StaggeredTile.count(2, index.isEven ? 2 : 3),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }
}

class _PinterestItem extends StatelessWidget {
  final int index;
  const _PinterestItem({
    Key key,
    this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: Colors.blue, borderRadius: BorderRadius.circular(30)),
        child: new Center(
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: new Text('$index'),
          ),
        ));
  }
}

class _MenuModel with ChangeNotifier {
  bool _visible = true;

  bool get visible => this._visible;

  set visible(bool value) {
    this._visible = value;
    notifyListeners();
  }
}
