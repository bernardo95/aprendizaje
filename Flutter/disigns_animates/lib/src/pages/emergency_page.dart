import 'package:animate_do/animate_do.dart';
import 'package:disigns_animates/src/widgets/boton_emergency.dart';
import 'package:disigns_animates/src/widgets/emergency_header_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class _ItemBoton {
  final IconData icon;
  final String texto;
  final Color color1;
  final Color color2;

  _ItemBoton(this.icon, this.texto, this.color1, this.color2);
}

class EmergencyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final items = <_ItemBoton>[
      new _ItemBoton(FontAwesomeIcons.carCrash, 'Motor Accident',
          Color(0xff6989F5), Color(0xff906EF5)),
      new _ItemBoton(FontAwesomeIcons.plus, 'Medical Emergency',
          Color(0xff66A9F2), Color(0xff536CF6)),
      new _ItemBoton(FontAwesomeIcons.theaterMasks, 'Theft / Harrasement',
          Color(0xffF2D572), Color(0xffE06AA3)),
      new _ItemBoton(FontAwesomeIcons.biking, 'Awards', Color(0xff317183),
          Color(0xff46997D)),
      new _ItemBoton(FontAwesomeIcons.carCrash, 'Motor Accident',
          Color(0xff6989F5), Color(0xff906EF5)),
      new _ItemBoton(FontAwesomeIcons.plus, 'Medical Emergency',
          Color(0xff66A9F2), Color(0xff536CF6)),
      new _ItemBoton(FontAwesomeIcons.theaterMasks, 'Theft / Harrasement',
          Color(0xffF2D572), Color(0xffE06AA3)),
      new _ItemBoton(FontAwesomeIcons.biking, 'Awards', Color(0xff317183),
          Color(0xff46997D)),
      new _ItemBoton(FontAwesomeIcons.carCrash, 'Motor Accident',
          Color(0xff6989F5), Color(0xff906EF5)),
      new _ItemBoton(FontAwesomeIcons.plus, 'Medical Emergency',
          Color(0xff66A9F2), Color(0xff536CF6)),
      new _ItemBoton(FontAwesomeIcons.theaterMasks, 'Theft / Harrasement',
          Color(0xffF2D572), Color(0xffE06AA3)),
      new _ItemBoton(FontAwesomeIcons.biking, 'Awards', Color(0xff317183),
          Color(0xff46997D)),
    ];

    List<Widget> itemMap = items.map((item)=>FadeInLeft(child: BotonEmergency(icon: item.icon,text: item.texto,color1: item.color1,color2: item.color2,onPress: (){print('click');},))).toList();
    return Scaffold(
      // backgroundColor: Colors.lime,
      // body: _PageHeader()
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 250),
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                SizedBox(
                  height: 40,
                ),
                ...itemMap
              ],
            ),
          ),
          _Header()
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        IconHeader(
          icon: FontAwesomeIcons.plus,
          titulo: 'Haz solicitado',
          subtitulo: 'Asistencia Medica',
          color1: Color(0xff536CF6),
          color2: Color(0xff66A9F2),
        ),
        Positioned(
          right: 0,
          top: 50,
          child: RawMaterialButton(
            onPressed: (){},
            shape: CircleBorder(),
            padding: EdgeInsets.all(15),
            child: FaIcon(FontAwesomeIcons.ellipsisV,color: Colors.white,)),
        )
      ],
    );
  }
}

class _BotonCreate extends StatelessWidget {
  const _BotonCreate({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BotonEmergency(
      text: 'Motor Accident',
      icon: FontAwesomeIcons.carCrash,
      color1: Color(0xff6989f5),
      color2: Color(0xff906ef5),
      onPress: () => print('Motor Accident'),
    );
  }
}

class _PageHeader extends StatelessWidget {
  const _PageHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconHeader(
      icon: FontAwesomeIcons.plus,
      titulo: 'Haz Solicitado',
      subtitulo: 'Asistencia Medica',
      color1: Color(0xff66A9F2),
      color2: Color(0xff536CF6),
    );
  }
}
