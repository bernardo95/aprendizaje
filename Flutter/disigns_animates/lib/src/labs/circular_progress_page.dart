import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';


class CircularProgressPage extends StatefulWidget {

  @override
  _CircularProgressPageState createState() => _CircularProgressPageState();
}

class _CircularProgressPageState extends State<CircularProgressPage> with SingleTickerProviderStateMixin{ 
  AnimationController _controller;
  double porcentaje = 0.0;
  double nuevoPorcentaje = 0.0;

  @override
  void initState() {
    _controller = AnimationController(vsync: this,duration: Duration(milliseconds: 800));
    _controller.addListener((){
      // print(_controller.value);
      porcentaje = lerpDouble(porcentaje, nuevoPorcentaje, _controller.value);
      setState(() {
        
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        backgroundColor: Colors.pink,
        onPressed: (){
          porcentaje = nuevoPorcentaje;
          nuevoPorcentaje+=10;
          if (nuevoPorcentaje >= 100) {
            nuevoPorcentaje = 0;
            porcentaje = 0;
          }
          _controller.forward(from: 0.0);
          setState(() {
            
          });
        },
         ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(5),
          width: 300,
          height: 300,
          // color: Colors.red,
          child: CustomPaint(
            painter: _RadialProgress(porcentaje),
          )
     ),
      ),
   );
  }
}

class _RadialProgress extends CustomPainter{
  final porcentaje;

  _RadialProgress(this.porcentaje);
  @override
  void paint(Canvas canvas, Size size) {
    //circulo
    final paint = Paint()
      ..strokeWidth =4
      ..color = Colors.grey
      ..style = PaintingStyle.stroke;

    final center = Offset(size.width*0.5,size.height*0.5);
    final radius = min(size.width*0.5, size.height*0.5);
    canvas.drawCircle(center, radius, paint);

    //arco
    final arco = Paint()
      ..strokeWidth = 10
      ..color = Colors.pink
      ..style = PaintingStyle.stroke;
    
    //parte que va llenando el arco
    double arcAngle = 2*pi*(porcentaje/100);
    canvas.drawArc(
      Rect.fromCircle(center: center,radius: radius), -pi/2 , arcAngle, false, arco);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}