import 'package:flutter/material.dart';

import 'package:disigns_animates/src/labs/slideshow_page.dart';
import 'package:disigns_animates/src/pages/animacion_page.dart';
import 'package:disigns_animates/src/pages/emergency_page.dart';
import 'package:disigns_animates/src/pages/graficas_circulares_pages.dart';
import 'package:disigns_animates/src/pages/header_page.dart';
import 'package:disigns_animates/src/pages/pinteresst_page.dart';
import 'package:disigns_animates/src/pages/sliver_list_page.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

final pageRoutes = <_Route>[
  _Route(FontAwesomeIcons.slideshare, 'Slideshow', SlideShowPage()),
  _Route(FontAwesomeIcons.ambulance, 'Emergency', EmergencyPage()),
  _Route(FontAwesomeIcons.heading, 'Encabezados', HeadersPage()),
  _Route(FontAwesomeIcons.peopleCarry, 'Cuadro animado', CuadradoAnimado()),
  _Route(FontAwesomeIcons.circleNotch, 'Circulo progreso', GraficasCircularesPage()),
  _Route(FontAwesomeIcons.pinterest, 'Pinteres', PinterestPage()),
  _Route(FontAwesomeIcons.mobile, 'Sliver', SliverListPage()),





];

class _Route {
  final IconData icon;
  final String titulo;
  final Widget page;

  _Route(this.icon, this.titulo, this.page);
  
}