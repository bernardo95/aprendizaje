import 'package:flutter/material.dart';

class SliderModel with ChangeNotifier {
  double _currentPage = 0;
  Color _colorPrimario = Colors.grey;
  Color _colorSecundario = Colors.blue;
  double _bulletPrimario = 12.0;
  double _bulletSecundario = 12.0;

  double get currentPage => this._currentPage;

  set currentPage(double value) {
    this._currentPage = value;
    notifyListeners();
  }

  Color get colorPrimario => this._colorPrimario;

  set colorPrimario(Color value) {
    this._colorPrimario = value;
    // notifyListeners();
  }

  Color get colorSecundario => this._colorSecundario;

  set colorSecundario(Color value) {
    this._colorSecundario = value;
    // notifyListeners();
  }

  double get bulletPrimario => this._bulletPrimario;

  set bulletPrimario(double value) {
    this._bulletPrimario = value;
    // notifyListeners();
  }

  double get bulletSecundario => this._bulletSecundario;
  
  set bulletSecundario(double value) {
    this._bulletSecundario = value;
    // notifyListeners();
  }
}
