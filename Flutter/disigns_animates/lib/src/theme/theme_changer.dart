import 'package:flutter/material.dart';

class ThemeChanger with ChangeNotifier{
  bool _darkTheme = false;
  bool _customTheme = false;
  ThemeData _currentTheme;

  ThemeChanger(int theme){
    switch (theme) {
      case 1:
        this._darkTheme = false;
        this._customTheme = false;
        this._currentTheme = ThemeData.light();
        break;
      case 2:
        this._darkTheme = true;
        this._customTheme = false;
        this._currentTheme = ThemeData.dark();
        break;
      case 3:
        this._darkTheme = false;
        this._customTheme = true;
        this._currentTheme = ThemeData.light();
        break;
      default:
        this._darkTheme = false;
        this._customTheme = false;
        this._currentTheme = ThemeData.light();
        break;
    }
  }

  bool get darkTheme =>this._darkTheme;
  bool get customTheme =>this._customTheme;
  ThemeData get currentTheme => this._currentTheme;

  set darkTheme(bool value){
    this._customTheme = false;
    this._darkTheme = value;
    if (value) {
      this._currentTheme = ThemeData.dark();
    }else{
      this._currentTheme = ThemeData.light();
    }
    notifyListeners();
  }

  set customTheme(bool value){
     this._darkTheme = false;
     this._customTheme = value;
     if (value) {
      this._currentTheme = ThemeData.light();
    }else{
      this._currentTheme = ThemeData.light();
    }
     notifyListeners();
  }

  
}