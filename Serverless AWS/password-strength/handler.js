// 'use strict';
const verifyPasswordLenght = require('./constraints/verifyPasswordLength');
const verifyPasswordStrength = require('./constraints/verifyPasswordStrength');

module.exports.password = async event => {
    try {
        const { password } = event.pathParameters;
        await verifyPasswordLenght(password);
        const { score } = (await verifyPasswordStrength(password)).score;
        return {
            statusCode: 200,
            body: JSON.stringify({
                message: `Password score: ${score}`
            })
        }
    } catch (error) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: `Error: ${error.message}`,
                score: error.score
            })
        }
    }
};