var imagenes=[];
imagenes["cauchin"]="vaca.png";
imagenes["pokacho"]="pollo.png";
imagenes["tocinauro"]="cerdo.png";

var cauchin = new Pakiman("cauchin",100,30,"tierra");
var pokacho = new Pakiman("pokacho",80,50,"aire");
var tocinauro = new Pakiman("tocinauro",120,40,"tierra");

var coleccion= [];
coleccion.push(cauchin);
coleccion.push(pokacho);
coleccion.push(tocinauro);

for (var pakin of coleccion) {
  pakin.mostrar();
}
