var vp=document.getElementById('canvas');
var papel=vp.getContext("2d");
document.addEventListener("keydown",moverLobo);

var teclas = {
  UP: 38,
  DOWN: 40,
  LEFT: 37,
  RIGHT: 39
};

var fondo={
  url: "tile.png",
  cargaStatus: false
};

var vaca={
  url: "vaca.png",
  cargaStatus: false
};

var cerdo={
  url: "cerdo.png",
  cargaStatus: false
};

var pollo={
  url: "pollo.png",
  cargaStatus: false
};

var lobo={
  url: "lobo.png",
  cargaStatus:false
};

fondo.imagen = new Image();
fondo.imagen.src=fondo.url;
fondo.imagen.addEventListener("load",cargarFondo);

vaca.imagen = new Image();
vaca.imagen.src=vaca.url;
vaca.imagen.addEventListener("load",cargarVaca);

cerdo.imagen = new Image();
cerdo.imagen.src=cerdo.url;
cerdo.imagen.addEventListener("load",cargarCerdo);

pollo.imagen = new Image();
pollo.imagen.src=pollo.url;
pollo.imagen.addEventListener("load",cargarPollo);

lobo.imagen = new Image();
lobo.imagen.src = lobo.url;
lobo.imagen.addEventListener("load",cargarLobo);

function cargarFondo() {
 fondo.cargaStatus=true;
 dibujar();
}

function cargarVaca() {
 vaca.cargaStatus=true;
 dibujar();
}

function cargarCerdo() {
 cerdo.cargaStatus=true;
 dibujar();
}

function cargarPollo() {
 pollo.cargaStatus=true;
 dibujar();
}

function cargarLobo() {
  lobo.cargaStatus=true;
  dibujar();
}

function dibujar() {
  if (fondo.cargaStatus) {
      papel.drawImage(fondo.imagen,0,0);
  }
  if (vaca.cargaStatus) {
    for (var i = 0; i < 5; i++) {
      var x=aleatorio(0,5);
      var y= aleatorio(0,5);
      var x=x*80;
      var y=y*80;
    papel.drawImage(vaca.imagen,x,y);
    }
  }
  if (cerdo.cargaStatus) {
    for (var i = 0; i < 5; i++) {
      var x=aleatorio(0,5);
      var y= aleatorio(0,5);
      var x=x*80;
      var y=y*80;
      papel.drawImage(cerdo.imagen,x,y);
    }
  }
  if (pollo.cargaStatus) {
    for (var i = 0; i < 5; i++) {
      var x=aleatorio(0,5);
      var y= aleatorio(0,5);
      var x=x*80;
      var y=y*80;
    papel.drawImage(pollo.imagen,x,y);
    }
  }
  if(lobo.cargaStatus){
    papel.drawImage(lobo.imagen,250,250);
  }
}

var xLobo=250;
var yLobo=250;
var movimiento=50;
function moverLobo(tecla) {
  switch (tecla.keyCode) {
    case teclas.UP:

    papel.drawImage(lobo.imagen,xLobo,yLobo-movimiento);
    yLobo=yLobo-movimiento;
    break;
    case teclas.DOWN:
    papel.drawImage(lobo.imagen,xLobo,yLobo+movimiento);
    yLobo=yLobo+movimiento;
    break;
    case teclas.LEFT:
    papel.drawImage(lobo.imagen,xLobo-movimiento,yLobo);
    xLobo=xLobo-movimiento;
    break;
    case teclas.RIGHT:
    papel.drawImage(lobo.imagen,xLobo+movimiento,yLobo);
    xLobo=xLobo+movimiento;
      break;
    default:
  }
}
function aleatorio(min,max) {
  var resultado;
  resultado=Math.floor(Math.random()*(max-min+1))+min;
  return resultado;
}
