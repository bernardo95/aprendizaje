
var cuadrito=document.getElementById("canvas");
var papel=cuadrito.getContext("2d");
var x;
var y;
var estado=0;
var color="blue";
cuadrito.addEventListener("mousedown",presionarMouse);
cuadrito.addEventListener("mouseup",soltarMouse);
cuadrito.addEventListener("mousemove",dibujarMouse);

function dibujarMouse(evento) {
  if (estado==1) {
    dibujarLinea(color,x,y,evento.clientX,evento.clientY,papel);
  }
  x=evento.clientX;
  y=evento.clientY;
}

function dibujarLinea(color,xinicial,yinicial,xfinal,yfinal,lienzo) {
  lienzo.beginPath();
  lienzo.strokeStyle = color;
  lienzo.lineWidth=3;
  lienzo.moveTo(xinicial,yinicial);
  lienzo.lineTo(xfinal,yfinal);
  lienzo.stroke();
  lienzo.closePath();
}

function presionarMouse(evento) {
  estado=1;
  x=evento.clientX;
  y=evento.clientY;
  console.log(evento);
}

function soltarMouse(evento) {
  estado=0;
  x=evento.clientX;
  y=evento.clientY;
}
