document.addEventListener("keydown",dibujarFlechas);
var cuadrito=document.getElementById("canvas");
var papel=cuadrito.getContext("2d");
var teclas = {
  UP: 38,
  DOWN: 40,
  LEFT: 37,
  RIGHT: 39
};

function dibujarLinea(color,xinicial,yinicial,xfinal,yfinal,lienzo) {
  lienzo.beginPath();
  lienzo.strokeStyle = color;
  lienzo.lineWidth=3;
  lienzo.moveTo(xinicial,yinicial);
  lienzo.lineTo(xfinal,yfinal);
  lienzo.stroke();
}
var x=150;
var y=150;
var color="blue";
var movimiento=3;
function dibujarFlechas(tecla){
switch (tecla.keyCode) {
  case teclas.UP:
  dibujarLinea(color,x,y,x,y-movimiento,papel);
  y=y-movimiento;
  break;
  case teclas.DOWN:
  dibujarLinea(color,x,y,x,y+movimiento,papel);
  y=y+movimiento;
  break;
  case teclas.LEFT:
  dibujarLinea(color,x,y,x-movimiento,y,papel);
  x=x-movimiento;
  break;
  case teclas.RIGHT:
  dibujarLinea(color,x,y,x+movimiento,y,papel);
  x=x+movimiento;
    break;
  default:
}
}
