var texto = document.getElementById('texto_lineas');
var boton = document.getElementById('boton_linea');
boton.addEventListener("click",dibujoClick);
var d = document.getElementById("dibujito")
var lienzo = d.getContext("2d");
var ancho=d.width;

function dibujarLinea(color,xinicial,yinicial,xfinal,yfinal) {
  lienzo.beginPath();
  lienzo.strokeStyle = color;
  lienzo.moveTo(xinicial,yinicial);
  lienzo.lineTo(xfinal,yfinal);
  lienzo.stroke();
  lienzo.closePath();
}

function dibujoClick() {
var numero = texto.value;
numero=parseInt(numero);
var espacio=(ancho/numero);
var y=0;
var x=espacio;
for (var i = 0; i < numero; i++) {
dibujarLinea("blue",y,0,ancho,x);
dibujarLinea("blue",0,y,x,ancho);
y=y+espacio;
x=x+espacio;
console.log(x);
}
}
