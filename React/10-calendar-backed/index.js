const express = require("express");
require("dotenv").config();
const cors = require("cors");
const { dbConnection } = require("./database/config");

const app = express();
const port = process.env.PORT || 4000;

//database
dbConnection();

//cors configuration
app.use(cors());

app.listen(port, () => console.log(`listening on http://localhost:${port}`));

//directorio publico
app.use(express.static("public"));

//body parse
app.use(express.json());

//routes
app.use("/api/auth", require("./routes/auth"));
