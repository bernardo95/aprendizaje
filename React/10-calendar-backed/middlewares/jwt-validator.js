const { response } = require("express");
const jwt = require("jsonwebtoken");

const validatorJWT = (req, res = response, next) => {
  //handle errors
  const token = req.header("x-token");
  if (!token) {
    return res.status(401).json({
      ok: false,
      errors: "Token doesn't provider",
    });
  }
  try {
    const payload = jwt.verify(token, process.env.SECRET_JWT_SEED);
    const { uid, name } = payload;
    req.uid = uid;
    req.name = name;
    next();
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error,
    });
  }
};

module.exports = {
  validatorJWT,
};
