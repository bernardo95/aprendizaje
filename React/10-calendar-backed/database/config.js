const mongoose = require("mongoose");

const DB_CNN = process.env.DB_CNN;

const dbConnection = async () => {
  try {
    await mongoose.connect(DB_CNN, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    console.log("DB Online");
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  dbConnection,
};
