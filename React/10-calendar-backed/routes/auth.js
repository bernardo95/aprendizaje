// Routes user /Auth
// host + /api/auth

const { Router } = require("express");
const router = Router();
const { check } = require("express-validator");
const { createUser, loginUser, renewToken } = require("../controllers/auth");
const { validatorField } = require("../middlewares/field-validators");
const { validatorJWT } = require("../middlewares/jwt-validator");

router.post(
  "/register",
  [
    check("name", "Name is required").notEmpty(),
    check("email", "Email is required").notEmpty(),
    check("email", "Email is not valid").isEmail(),
    check("password", "Passoword must be 6 characters").isLength({ min: 6 }),
    validatorField,
  ],
  createUser
);

router.post(
  "/",
  [
    check("email", "Email is required").notEmpty(),
    check("email", "Email is not valid").isEmail(),
    check("password", "Password is required").notEmpty(),
    validatorField,
  ],
  loginUser
);

router.get("/renew", [validatorJWT], renewToken);

module.exports = router;
