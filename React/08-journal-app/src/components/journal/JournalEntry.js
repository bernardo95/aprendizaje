import React from "react";
import moment from "moment";
import { useDispatch } from "react-redux";
import { activeNote } from "../../actions/notesActions";

export const JournalEntry = (props) => {
  // console.log(props);
  const dispatch = useDispatch();
  const { id, body } = props;
  const noteDate = moment(body.date);
  const handleActiveEntry = () => {
    const note = {
      title: body.title,
      body: body.body,
      date: body.date,
      url: body.url,
    };
    dispatch(activeNote(id, note));
  };
  return (
    <div onClick={handleActiveEntry} className="journal__entry pointer">
      {body.url && (
        <div
          className="journal__entry-picture"
          style={{
            backgroundSize: "cover",
            backgroundImage: `url(${body.url})`,
          }}
        ></div>
      )}

      <div className="journal__entry-body">
        <p className="journal__entry-title">{body.title}</p>
        <p className="journal__entry-content">{body.body}.</p>
      </div>

      <div className="journal__entry-date-box">
        <span>{noteDate.format("dddd")}</span>
        <h4>{noteDate.format("Do")}</h4>
      </div>
    </div>
  );
};
