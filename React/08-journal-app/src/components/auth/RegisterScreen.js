import React from "react";
import { Link } from "react-router-dom";
import { useForm } from "../../hooks/useForm";
import { useDispatch, useSelector } from "react-redux";
import validator from "validator";
import { removeErrorAction, setErrorAction } from "../../actions/uiActions";
import { startRegisterWithEmailPasswordName } from "../../actions/authActions";

export const RegisterScreen = () => {
  const dispatch = useDispatch();
  const { msgError } = useSelector((state) => state.ui);
  console.log(msgError);

  const [formValues, handleInputChange] = useForm({
    name: "Bernardo",
    email: "bernardo@gmail.com",
    password: "123456",
    passwordConfirm: "123456",
  });
  const { name, email, password, passwordConfirm } = formValues;

  const handleRegister = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      dispatch(startRegisterWithEmailPasswordName(email, password, name));
    }
  };

  const isFormValid = () => {
    if (name.trim().lenght === 0) {
      dispatch(setErrorAction("Name is required"));
      return false;
    } else if (!validator.isEmail(email)) {
      dispatch(setErrorAction("Email is not valid"));

      return false;
    } else if (password !== passwordConfirm || password.lenght < 5) {
      dispatch(
        setErrorAction("Password should be at last 6 characters and match")
      );
      return false;
    }
    dispatch(removeErrorAction());

    return true;
  };

  return (
    <>
      <h3 className="auth__title">Register</h3>

      <form onSubmit={handleRegister}>
        {msgError && <div className="auth__alert-error"> {msgError} </div>}
        <input
          type="text"
          placeholder="Name"
          name="name"
          className="auth__input"
          autoComplete="off"
          value={name}
          onChange={handleInputChange}
        />

        <input
          type="text"
          placeholder="Email"
          name="email"
          className="auth__input"
          autoComplete="off"
          value={email}
          onChange={handleInputChange}
        />

        <input
          type="password"
          placeholder="Password"
          name="password"
          className="auth__input"
          value={password}
          onChange={handleInputChange}
        />

        <input
          type="password"
          placeholder="Confirm password"
          name="passwordConfirm"
          className="auth__input"
          value={passwordConfirm}
          onChange={handleInputChange}
        />

        <button type="submit" className="btn btn-primary btn-block mb-5">
          Register
        </button>

        <Link to="/auth/login" className="link">
          Already registered?
        </Link>
      </form>
    </>
  );
};
