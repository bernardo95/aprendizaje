import firebase from "firebase";
import "firebase/firebase-firestore";
import "firebase/auth";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDG7SjMX9BlVMEQHYFpHPkfAGlO25DMsZ0",
  authDomain: "react-apps-78932.firebaseapp.com",
  databaseURL: "https://react-apps-78932.firebaseio.com",
  projectId: "react-apps-78932",
  storageBucket: "react-apps-78932.appspot.com",
  messagingSenderId: "187215920",
  appId: "1:187215920:web:2f65f475154de4ae1d3df5",
  measurementId: "G-DKV7S17T21",
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { db, googleAuthProvider, firebase };
