import Swal from "sweetalert2";
import { db } from "../firebase/firebase-config";
import { fileUpload } from "../helpers/fileUpload";
import { loadNotes } from "../helpers/loadNotes";
import { types } from "../types/types";

export const startNewNote = () => {
  return async (dispatch, getState) => {
    const { uid } = getState().auth;
    const newNote = {
      title: "",
      body: "",
      date: new Date().getTime(),
    };

    const doc = await db.collection(`${uid}/journal/notes`).add(newNote);
    dispatch(activeNote(doc.id, newNote));
  };
};

export const activeNote = (id, note) => ({
  type: types.notesActive,
  payload: {
    id,
    ...note,
  },
});

export const startLoadingNotes = (uid) => {
  return async (dispatch) => {
    const notes = await loadNotes(uid);
    dispatch(setNotes(notes));
  };
};

export const setNotes = (notes) => ({
  type: types.notesLoad,
  payload: notes,
});

export const startSaveNote = (note) => {
  return async (dispatch, getState) => {
    const { uid } = getState().auth;

    if (!note.url) {
      delete note.url;
    }

    const noteToFirestore = { ...note };

    delete noteToFirestore.id;

    try {
      await db.doc(`${uid}/journal/notes/${note.id}`).update(noteToFirestore);
      // console.log(noteToFirestore, "note");
      dispatch(refreshNote(note.id, noteToFirestore));
      // dispatch(startLoadingNotes(uid));
    } catch (error) {
      console.log(error);
    }
  };
};

export const refreshNote = (id, note) => ({
  type: types.notesUpdated,
  payload: {
    id,
    body: { ...note },
  },
});

export const startUploading = (file) => {
  return async (dispatch, getState) => {
    const { active: activeNote } = getState().notes;
    Swal.fire({
      title: "Uploading...",
      text: "Please wait...",
      allowOutsideClick: false,
      showConfirmButton: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
    // console.log(activeNote);
    try {
      const fileUrl = await fileUpload(file);
      activeNote.url = fileUrl;
      dispatch(startSaveNote(activeNote));
      Swal.close();
    } catch (error) {}
  };
};

export const startDeleteNote = (id) => {
  return async (distpach, getState) => {
    const { uid } = getState().auth;
    try {
      await db.doc(`${uid}/journal/notes/${id}`).delete();
      distpach(deleteNote(id));
    } catch (error) {}
  };
};

export const deleteNote = (id) => ({
  type: types.notesDelete,
  payload: id,
});

export const cleanNotes = () => ({
  type: types.notesLogoutCleaning,
  payload: {},
});
