import { types } from "../types/types";
import { firebase, googleAuthProvider } from "../firebase/firebase-config";
import { finishLoading, startLoading } from "./uiActions";
import { cleanNotes } from "./notesActions";
// import Swal from "sweetalert2";

export const startLoginEmailPassword = (email, password) => {
  return async (dispatch) => {
    dispatch(startLoading());
    await firebase.auth().signInWithEmailAndPassword(email, password);
    dispatch(login(email, password));
    dispatch(finishLoading());
  };
};

export const startRegisterWithEmailPasswordName = (email, password, name) => {
  return async (dispatch) => {
    dispatch(startLoading());
    const userCredentials = await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password);
    await userCredentials.user.updateProfile({
      displayName: name,
    });
    dispatch(finishLoading());
    console.log(userCredentials);
  };
};

export const login = (uid, displayName) => ({
  type: types.login,
  payload: {
    uid,
    displayName,
  },
});

export const startLogout = () => {
  return async (dispatch) => {
    await firebase.auth().signOut();
    dispatch(logout());
    dispatch(cleanNotes());
  };
};

export const logout = () => ({
  type: types.logout,
});

export const googleLogin = () => {
  return async (dispatch) => {
    const userCredentials = await firebase
      .auth()
      .signInWithPopup(googleAuthProvider);
    console.log(userCredentials);
    const { user } = userCredentials;
    dispatch(login(user.uid, user.displayName));
  };
};
