export const fileUpload = async (file) => {
  const cloudUrl = "https://api.cloudinary.com/v1_1/djdmjxgxh/upload";

  const formData = new FormData();
  formData.append("upload_preset", "react-journal");
  formData.append("file", file);

  try {
    const httpResponse = await fetch(cloudUrl, {
      method: "post",
      body: formData,
    });
    if (httpResponse.ok) {
      const cloudResp = await httpResponse.json();
      return cloudResp.secure_url;
    } else {
      throw httpResponse.json();
    }
  } catch (error) {
    throw error;
  }
};
