import React, { useState } from "react";
import { MultipleCustomHooks } from "../03-examples/MultipleCustomHooks";
import "../02-useEffect/useEfect.css";

export const ReactExampleRef = () => {
  const [show, setShow] = useState(false);
  return (
    <div>
      <h1>React Example Ref</h1>
      <hr />
      {show && <MultipleCustomHooks />}
      <button className="btn btn-dark mt-3" onClick={() => setShow(!show)}>
        Show/Hide
      </button>
    </div>
  );
};
