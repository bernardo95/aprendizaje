import React, { useEffect, useState } from "react";
import { Message } from "./Message";
import "./useEfect.css";

export const SimpleForm = () => {
  const [formState, setForm] = useState({
    name: "",
    email: "",
  });
  const { name, email } = formState;
  const handleInput = ({ target }) => {
    setForm({
      ...formState,
      [target.name]: target.value,
    });
  };

  useEffect(() => {
    // console.log("hey");
  }, []);

  useEffect(() => {
    // console.log("formState cambio");
  }, [formState]);

  useEffect(() => {
    // console.log("email cambio");
  }, [email]);

  return (
    <>
      <h1>useEffect</h1>
      <hr />
      <div className="form-group">
        <input
          type="text"
          name="name"
          className="form-control"
          placeholder="tu nombre"
          autoComplete="off"
          value={name}
          onChange={handleInput}
        ></input>
      </div>
      <div className="form-group">
        <input
          type="text"
          name="email"
          className="form-control"
          placeholder="email@gmail.com"
          autoComplete="off"
          value={email}
          onChange={handleInput}
        ></input>
      </div>

      {name && <Message />}
    </>
  );
};
