import React, { memo } from "react";

export const ShowIncrement = memo(({ increment }) => {
  console.log("s");
  return (
    <button className="btn btn-danger" onClick={() => increment(5)}>
      Incrementar
    </button>
  );
});
