import React from "react";
import { useCounter } from "../../hooks/useCounter";
import { useFetch } from "../../hooks/useFetch";
import "../02-useEffect/useEfect.css";

export const MultipleCustomHooks = () => {
  const { counter, increment } = useCounter(1);
  const state = useFetch(
    `https://www.breakingbadapi.com/api/quotes/${counter}`
  );
  const { loading, data } = state;
  const { author, quote } = !!data && data[0];
  return (
    <div>
      <h1>BreakingBad</h1>
      <hr />

      {loading ? (
        <div className="alert alert-info text-capitalize">Loading...</div>
      ) : (
        <div className="blockquote text-right">
          <p className="mb-0">{quote}</p>
          <footer className="blockquote-footer">{author}</footer>
        </div>
      )}
      <button className="btn btn-primary" onClick={increment}>
        Siguiente
      </button>
    </div>
  );
};
