import React from "react";
import "./styles.css";

export const TodoListItem = ({ todo, i, handleDelete, handleToggle }) => {
  return (
    <li className="list-group-item" key={todo.id}>
      <p
        onClick={() => handleToggle(todo)}
        className={`text-center ${todo.done && "complete"}`}
      >
        {i + 1}. {todo.desc}
      </p>
      <button onClick={() => handleDelete(todo)} className="btn btn-danger">
        Borrar
      </button>
    </li>
  );
};
