import React, { useEffect, useReducer } from "react";
import { useForm } from "../../hooks/useForm";
import "./styles.css";
import { TodoList } from "./TodoList";
import { todoReducer } from "./todoReducer";

// const initialState = [
//     {
//       id: new Date().getTime(),
//       desc: "Aprender React",
//       done: false,
//     },
//   ];

const init = () => {
  let todos = JSON.parse(localStorage.getItem("todos")) || [];
  return todos;
};

export const TodoApp = () => {
  const [todos, dispatch] = useReducer(todoReducer, [], init);

  const [{ descripcion }, handelInputChange, reset] = useForm({
    descripcion: "",
  });

  const handleAddTodo = (newTodo) => {
    dispatch({
      type: "add",
      payload: newTodo,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (descripcion.trim().length <= 1) return;
    const newTodo = {
      id: new Date().getTime(),
      desc: descripcion,
      done: false,
    };
    handleAddTodo(newTodo);
    reset();
  };

  const handleDelete = (todo) => {
    const action = {
      type: "delete",
      payload: todo,
    };
    dispatch(action);
  };

  const handleToggle = (todo) => {
    dispatch({
      type: "toggle",
      payload: todo,
    });
  };

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  return (
    <div>
      <h1>TodoApp ({todos.length})</h1>
      <hr />
      <div className="row">
        <div className="col-7">
          <TodoList
            todos={todos}
            handleDelete={handleDelete}
            handleToggle={handleToggle}
          />
        </div>
        <div className="col-5">
          <h4>Agregar TODO</h4>
          <hr />
          <form onSubmit={handleSubmit}>
            <input
              onChange={handelInputChange}
              type="text"
              name="descripcion"
              value={descripcion}
              className="form-control"
              placeholder="Aprender.."
              autoComplete="off"
            />
            <button
              type="submit"
              className="btn btn-outline-primary mt-1 btn-block"
            >
              Agregar
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};
