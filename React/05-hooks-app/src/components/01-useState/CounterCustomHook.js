import React from "react";
import { useCounter } from "../../hooks/useCounter";
import "./counter.css";

export const CounterCustomHook = () => {
  const { state, increment, decrement, reset } = useCounter();
  const factor = 2;
  return (
    <div>
      <h1>Counter Custom Hook: {state}</h1>
      <hr />
      <button className="btn btn-success" onClick={() => increment(factor)}>
        +{factor}
      </button>
      <button className="btn btn-danger" onClick={() => decrement(factor)}>
        -{factor}
      </button>
      <button className="btn btn-info" onClick={reset}>
        reset
      </button>
    </div>
  );
};
