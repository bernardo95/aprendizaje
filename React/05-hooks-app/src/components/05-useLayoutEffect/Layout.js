import React, { useLayoutEffect, useRef } from "react";
import { useCounter } from "../../hooks/useCounter";
import { useFetch } from "../../hooks/useFetch";
import "./layout.css";

export const Layout = () => {
  const { counter, increment } = useCounter(1);
  const state = useFetch(
    `https://www.breakingbadapi.com/api/quotes/${counter}`
  );
  const { data } = state;
  const { quote } = !!data && data[0];
  const pTagRef = useRef();
  useLayoutEffect(() => {
    console.log(pTagRef.current.getBoundingClientRect());
  }, [quote]);
  return (
    <div>
      <h1>Layout Effect</h1>
      <hr />

      <div className="blockquote text-right">
        <p ref={pTagRef} className="mb-0">
          {quote}
        </p>
      </div>

      <button className="btn btn-primary" onClick={increment}>
        Siguiente
      </button>
    </div>
  );
};
