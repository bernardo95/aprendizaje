import { useEffect, useRef, useState } from "react";

export const useFetch = (url) => {
  const isMounted = useRef(true);
  const [state, setState] = useState({
    data: null,
    loading: true,
    error: null,
  });

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    setState({
      ...state,
      loading: true,
    });
    fetch(url)
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        if (isMounted.current) {
          setState({
            loading: false,
            error: null,
            data: data,
          });
        } else {
          console.log("no existe componente");
        }
      });
  }, [url]);
  return state;
};
