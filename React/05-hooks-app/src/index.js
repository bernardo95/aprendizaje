import React from "react";
import ReactDOM from "react-dom";
// import { CallbackHook } from "./components/06-memos/CallbackHook";
// import { TodoApp } from "./components/07-useReducer/TodoApp";
import { MainApp } from "./components/08-useContext/MainApp";
// import { MemoHook } from "./components/06-memos/MemoHook";
// import { FormCustomHook } from "./components/02-useEffect/FormCustomHook";
// import { MultipleCustomHooks } from "./components/03-examples/MultipleCustomHooks";
// import { FocusScreen } from "./components/04-useRef/FocusScreen";
// import { ReactExampleRef } from "./components/04-useRef/ReactExampleRef";
// import { Layout } from "./components/05-useLayoutEffect/Layout";
// import { Memorize } from "./components/06-memos/Memorize";
// import { CounterApp } from "./components/01-useState/CounterApp";
// import { CounterCustomHook } from "./components/01-useState/CounterCustomHook";
// import { SimpleForm } from "./components/02-useEffect/SimpleForm";

ReactDOM.render(<MainApp />, document.getElementById("root"));
