import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { uiOpenModal } from "../../actions/uiAction.js";
import {
  evenSetActive,
  eventClearActiveNote,
} from "../../actions/calendarAction.js";

import { CalendarModal } from "./CalendarModal.jsx";
import { CalendarEvent } from "./CalendarEvent.jsx";
import { Navbar } from "../ui/Navbar.jsx";
import { AddNewFab } from "../ui/AddNewFab.jsx";
import { DeleteEventFab } from "../ui/DeleteEventFab.jsx";

import { messages } from "../../helpers/calendar-messages-es";

import { Calendar, momentLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
import "moment/locale/es";
moment.locale("es");

const localizer = momentLocalizer(moment);

const getLastView = () => {
  return localStorage.getItem("lastView") || "month";
};

export const CalendarScreen = () => {
  const { events, activeEvent } = useSelector((state) => state.calendar);
  const dispatch = useDispatch();

  const [lastView, setLastView] = useState(getLastView());

  const eventStyleGetter = (event, start, end, isSelected) => {
    // console.log(event, start, end, isSelected);
    const style = {
      backgroundColor: "#367cf7",
      borderRadius: 0.8,
      display: "block",
      color: "whites",
    };
    return style;
  };

  const onDobleClick = (e) => {
    dispatch(uiOpenModal());
  };

  const onSelectEvent = (e) => {
    dispatch(evenSetActive(e));
  };

  const onViewChange = (e) => {
    localStorage.setItem("lastView", e);
    setLastView(e);
  };

  const onSelectSlot = (e) => {
    dispatch(eventClearActiveNote());
  };

  return (
    <div className="calendar-screen">
      <Navbar />
      <Calendar
        localizer={localizer}
        events={events}
        startAccessor="start"
        endAccessor="end"
        messages={messages}
        eventPropGetter={eventStyleGetter}
        components={{ event: CalendarEvent }}
        onDoubleClickEvent={onDobleClick}
        onSelectEvent={onSelectEvent}
        onSelectSlot={onSelectSlot}
        selectable={true}
        onView={onViewChange}
        view={lastView}
      />
      <AddNewFab />
      {activeEvent && <DeleteEventFab />}
      <CalendarModal />
    </div>
  );
};
