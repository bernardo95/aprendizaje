import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { LoginScreen } from "../components/auth/LoginScreen.jsx";
import { CalendarScreen } from "../components/calendar/CalendarScreen.jsx";

export const AppRouter = () => {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/login" component={LoginScreen}></Route>
          <Route exact path="/" component={CalendarScreen}></Route>
          <Redirect to="/" />
        </Switch>
      </div>
    </Router>
  );
};
