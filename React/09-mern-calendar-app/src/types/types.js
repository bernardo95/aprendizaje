export const types = {
  uiOpenModal: "[ui] Open Modal",
  uiCloseModal: "[ui] Close Modal",
  eventSetActive: "[event] Set active",
  eventAddNew: "[event] add New",
  eventClearActiveNote: "[event] Clear Active Note",
  eventUpdated: "[event] Event uUdated",
  eventDeleted: "[event] Event Deleted",
};
