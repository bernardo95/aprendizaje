import React from "react";
import ReactDOM from "react-dom";
import { CalendarApp } from "./CalendarApp.jsx";
import "./styles.css";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(<CalendarApp />, document.getElementById("root"));

reportWebVitals();
