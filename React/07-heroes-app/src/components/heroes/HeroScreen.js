import React, { useMemo } from "react";
import { Redirect, useParams } from "react-router-dom";
import { getHeroeById } from "../../selectors/getHeroeById";

export const HeroScreen = ({ history }) => {
  const { heroeId } = useParams();
  // const heroe = getHeroeById(heroeId);
  const heroe = useMemo(() => getHeroeById(heroeId), [heroeId]);

  if (!heroe) return <Redirect to="/" />;
  const hanldeReturn = () => {
    if (history.length <= 2) {
      history.push("/");
    } else {
      history.goBack();
    }
  };
  const {
    id,
    superhero,
    publisher,
    alter_ego,
    first_appearance,
    characters,
  } = heroe;
  return (
    <div className="row mt-5">
      <div className="col-4">
        <img
          src={`../assets/heroes/${id}.jpg`}
          className="img-thumbnail animate__animated animate__fadeIn"
          alt={superhero}
        />
      </div>
      <div className="col-8">
        <h3>{superhero}</h3>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            <strong>Alter Ago: {alter_ego}</strong>
          </li>
          <li className="list-group-item">
            <strong>Publisher: {publisher}</strong>
          </li>
          <li className="list-group-item">
            <strong>First appearance: {first_appearance}</strong>
          </li>
        </ul>
        <h5>Character</h5>
        <p>{characters}</p>
        <button className="btn btn-outline-danger" onClick={hanldeReturn}>
          Return
        </button>
      </div>
    </div>
  );
};
