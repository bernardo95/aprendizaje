import React, { useState } from "react";
import PropTypes from "prop-types";
const CounterApp = ({ value }) => {
  const [counter, setCounter] = useState(value);
  //   setNombre("h");
  const handleAdd = () => {
    // setCounter(counter + 1);
    setCounter((c) => c + 1);
  };
  const handleLess = () => {
    // setCounter(counter + 1);
    setCounter((c) => c - 1);
  };

  const handleReset = () => {
    // setCounter(counter + 1);
    setCounter((c) => (c = value));
  };

  return (
    <>
      <h1>CounterApp</h1>
      <h2>{counter}</h2>
      <button onClick={handleLess}>-1</button>
      <button onClick={handleReset}>Reset</button>
      <button onClick={handleAdd}>+1</button>
    </>
  );
};

CounterApp.propTypes = {
  value: PropTypes.number.isRequired,
};
export default CounterApp;
