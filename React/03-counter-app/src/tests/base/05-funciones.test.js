import "@testing-library/jest-dom";
const {
  getUser,
  getUsuarioActivo,
} = require("../../base-pruebas/05-funciones");

describe("Test 05-funciones", () => {
  test("should be equial", () => {
    const userTest = {
      uid: "ABC123",
      username: "El_Papi1502",
    };
    const user = getUser();
    expect(user).toEqual(userTest);
  });
  test("should be equial", () => {
    const userTest = {
      uid: "ABC567",
      username: "El_Papi1502",
    };
    const user = getUsuarioActivo("El_Papi1502");
    expect(user).toEqual(userTest);
  });
});
