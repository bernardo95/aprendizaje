import "@testing-library/jest-dom";
const { getSaludo } = require("../../base-pruebas/02-template-string");

describe("Test 02-template-string.test.js", () => {
  test("getSaludo should return 'Hola Bernardo'", () => {
    const nombre = "Bernardo";
    const saludo = getSaludo(nombre);

    expect(saludo).toBe("Hola " + nombre);
  });
});
