import "@testing-library/jest-dom";
import { retornaArreglo } from "../../base-pruebas/07-deses-arr";

describe("Test 07-deses-arr.test", () => {
  test("should return string, number", () => {
    const [letras, numeros] = retornaArreglo();
    expect(letras).toEqual("ABC");
    expect(numeros).toEqual(123);
  });
});
