describe("test file demo.test.js", () => {
  test("should be equal strings", () => {
    //1. initialitation
    const message = "Hello world";
    //2. estimulate
    const message2 = `Hello world`;

    //3. look up behavior
    expect(message).toBe(message2);
  });
});
